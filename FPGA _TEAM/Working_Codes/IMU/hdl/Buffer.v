// Buffer.v

module Buffer (pclk, reset, address, rd_address, read_en, write_en, data_in, data_out);
parameter   data_width      =16;
parameter   address_width   =4;
parameter   mem_elements    =17;

input           pclk;
input           reset;
input           read_en;
input           write_en;
input   [4:0]   address;
input   [8:0]   rd_address;
input   [15:0]  data_in;

output  [15:0]  data_out;
wire    [15:0]  data_out;

reg     [data_width-1:0]  mem[mem_elements:0];
integer         i;

assign data_out = read_en? mem[rd_address]:16'bz;

always @ (posedge pclk or posedge reset)
  if (reset)
    begin
        for (i=0;i<18; i=i+1)
        mem[i]       <= 0;
    end
  else
    begin
      if (write_en == 1)
        mem[address] <= data_in;
      else
        mem[address] <= mem[address];
  end
endmodule
        