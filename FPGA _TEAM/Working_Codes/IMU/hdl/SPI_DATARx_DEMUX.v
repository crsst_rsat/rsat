// SPI_DATARx_DEMUX.v

module SPI_DATARx_DEMUX (SPI_PRDATA, data_cntrl, demux_en, buffer_data, MP_data);

input           data_cntrl;
input           demux_en;
input   [15:0]  SPI_PRDATA;

output  [15:0]  buffer_data;
output  [15:0]  MP_data;
wire     [15:0]  buffer_data;
wire     [15:0]  MP_data;

assign MP_data = ((data_cntrl== 1) | (demux_en==1))?SPI_PRDATA:MP_data;
assign buffer_data = ((data_cntrl== 0) | (demux_en==1))?SPI_PRDATA:buffer_data;

//always @ (data_cntrl or demux_en or SPI_PRDATA)
//  begin
//    if (demux_en)
//        if (data_cntrl)
//            MP_data         <= SPI_PRDATA;
//        else
//            buffer_data     <= SPI_PRDATA;
//  end
//
endmodule