// Data_or.v

module data_or (MP_st_re, MP_buf_re, diag_en, status_out, Sensor_data_out, IMU_data, MP_data);

input   [15:0] status_out;
input   [15:0] Sensor_data_out, IMU_data;
input          MP_st_re, MP_buf_re, diag_en;

output  [15:0] MP_data;
reg     [15:0] MP_data;

//assign  MP_data = status_out || Sensor_data_out || IMU_data;

always @ *
  if (MP_st_re)
    MP_data     <= status_out;
  else if (MP_buf_re)
    MP_data     <= Sensor_data_out;
  else if (diag_en)
    MP_data     <= IMU_data;
  else
    MP_data     <= 16'bz;

endmodule
