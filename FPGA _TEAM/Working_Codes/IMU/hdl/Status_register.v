// Status_register.v

module Status_register (MP_re, MP_we, MP_data_in, reset, clk, data_rdy, data_NA, status_dataout, MP_BUFFER_re,
                        MP_IMU_config_en, MP_IMU_diagnosis_en, MP_IMU_sensor_re, config_en_rst, FPGA_we,
                        diagnosis_en_rst,data_NA_en, data_rdy_en, data_NA_clr);

input               MP_re;
input               MP_we;
input               FPGA_we;
input               reset,clk;
input   [15:0]      MP_data_in;

reg     [15:0]      status;

input               data_rdy;
input               config_en_rst;
input               diagnosis_en_rst;
input               data_NA; // fault;
input               data_NA_en, data_NA_clr; // fault_en;
input               data_rdy_en;

output              MP_BUFFER_re;
output              MP_IMU_config_en;
output              MP_IMU_diagnosis_en;
output              MP_IMU_sensor_re;

output  [15:0]      status_dataout;

wire    [15:0]      status_dataout;
wire    [15:0]      status_masked;
wire                fault_status, data_NA_status, data_rdy_statusv;

assign  data_NA_status   = data_NA_en? data_NA : data_NA_clr? 0: status[4];
assign  data_rdy_status  = data_rdy_en?data_rdy : status[0];

//assign status_masked        = MP_data_in | status;
assign status_dataout       = MP_re? status: 16'bz; 
assign MP_BUFFER_re         = status[2];
assign MP_IMU_config_en     = status[3];
assign MP_IMU_diagnosis_en  = status[4];
assign MP_IMU_sensor_re     = status[5];


always @ (posedge clk or posedge reset)
begin
    if (reset == 1)
      begin
        status      <= 0;
      end
    else if ((MP_we == 1) && (MP_re != 1))
        status      <= MP_data_in;
    else if ((FPGA_we == 1) && (MP_re != 1))
      begin
        status[3]   <= config_en_rst? 0:status[3];
        status[4]   <= diagnosis_en_rst? 0:status[4];
      end
    else
      begin
//        fault_status     <= fault_en? fault : status[7];
        status      <= {status[15:7], data_NA_status, status[5:2], data_NA, data_rdy_status};
      end 
end
//assign  MP_re  = status[0];

endmodule