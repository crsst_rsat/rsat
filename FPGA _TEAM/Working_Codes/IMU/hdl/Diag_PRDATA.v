// Diag_PRDATA.v

module Diag_PRDATA (clk, reset, demux_cntrl, PRdata_in, Diag_data);

input           clk, reset, demux_cntrl;
input   [15:0]  PRdata_in;

output  [15:0]  Diag_data;
reg     [15:0]  Diag_data;

always @ (posedge clk or posedge reset)
 begin
    if (reset)
        Diag_data   <= 0;
    else if (demux_cntrl)
        Diag_data   <=  PRdata_in;
    else
        Diag_data   <=  Diag_data;
 end

 endmodule