// Receiver_SPI_buffer.v
`timescale 1ns/1ps
module Receiver_SPI_buffer;

reg             PCLK, msCLK, clk_1us;
reg             PRESETN;
reg             SPISDI=1'b0;
reg             IMU_datardy;         

reg  [15:0]     MP_data_in1;
reg  [8:0]      MP_addr;
reg             MP_re;
reg             MP_we;
reg             IMU_CS_n;

wire [7:0]      SPISS;
wire            SPISCLKO;
wire            SPISDO;
reg             start, MP_data_in2_0, stop, diag_start;

wire [15:0]     MP_data_in, MP_data_in2;

reg  [15:0]     buf_temp;
reg  [15:0]     IMU_addr_reg;
integer i;
integer j;
integer k,m; 


IMU_Top SPI_CORE_RX_0(
.PCLK(PCLK),
.RESET(PRESETN),
.IMU_datardy(IMU_datardy),
.MP_data_in(MP_data_in),              //16 bits
.MP_re(MP_re),
.MP_we(MP_we),
.IMU_CS_n(IMU_CS_n),
.MP_addr(MP_addr),                 //8 bts
.SPISDI(SPISDI),
.SPISS(SPISS),
.SPISCLKO(SPISCLKO),
.SPISDO(SPISDO),
.clk_1us(clk_1us)
);


initial 
 begin
        j           =16;
        k           =100;
        PCLK        =1'b0;
        msCLK       = 0;
        clk_1us     = 0;
        IMU_CS_n    = 1;
        IMU_datardy = 1;
        MP_data_in1  =16'b0000000000100000;
        MP_addr     =8'b11111111;
        PRESETN     =1'b1;
        IMU_addr_reg=0;
        MP_re       =0;
        MP_we       =0;
        start       =0;
        stop        =0;
        diag_start  = 0;

#1000   PRESETN     =1'b0;
        MP_we       =1;
#1000   MP_we       =0;
end

always @ (posedge msCLK)
begin
    if (start)
      begin
        MP_addr         =8'b11111111;
#50    MP_re           =1;
#150     MP_data_in2_0   = MP_data_in2[0];
#500    MP_re           =0;
      end
    else
      begin
        MP_addr     =MP_addr;
        MP_re       =0;
      end
end

always @ (posedge msCLK)
begin
  if (MP_data_in2_0)
   begin
     MP_addr     = 8'b11111111;
     MP_data_in1 = 16'b0000000000100100;
     stop        = 1;
#50  MP_we       = 1;
#500 MP_we       = 0;
#50000  MP_addr     = 0;
     for (m=0; m<21; m=m+1)
      begin
        MP_addr     = m;
        MP_re       = 1;
  #500  MP_re       = 0;
      end
     MP_addr     = 8'b11110000;
     MP_data_in1 = 16'b1010101010101010;
#50  MP_we       = 1;
#500 MP_we       = 0;
#500 MP_addr     = 8'b11111111;
     MP_data_in1 = 16'b0000000000010000;
#50  MP_we       = 1;
#500 MP_we       = 0;
#5000MP_addr     = 8'b11111010;
#50  MP_re       = 1;
#500 MP_re       = 0;


   end
    
end
    

always @ (negedge SPISCLKO)
begin
    j               =j-1;
    buf_temp        =k;
    SPISDI          =buf_temp[j];
    IMU_addr_reg[j] =SPISDO;
    if(j==0)
      begin
        k           =k+1;
        j           =16;
        start       =stop?0:1;
      end
end

assign MP_data_in  = MP_we? MP_data_in1:16'bz;
assign MP_data_in2 = MP_re? MP_data_in:MP_data_in2;

always 
#25 PCLK=~PCLK;

always 
#2500 msCLK=~msCLK;

always 
#50 clk_1us = ~clk_1us;

endmodule


