// GPS_buffer.v
module IMU_buffer (data, address, r_en, w_en, data_out);

input   [3:0]   address;
input           r_en, w_en;

input   [15:0]   data; 
output  [15:0]   data_out;
reg     [15:0]   mem [15:0];

assign  data_out = (r_en && !w_en)? {mem[address+3], mem[address+2], mem[address+1], mem[address]}:data_out;

always @ (w_en, address)
  begin
    if (w_en)
        mem[address] <= data; 
    else
        mem[address] <= mem[address];
  end
endmodule