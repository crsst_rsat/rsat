// SPI_DATA_MUX.v

module SPI_DATA_MUX (MP_data, FPGA_data, data_sel, SPI_data_out);

input  [15:0]    MP_data;
input  [15:0]    FPGA_data;
input           data_sel;

output [15:0]   SPI_data_out;

assign SPI_data_out = data_sel? MP_data : FPGA_data;

endmodule