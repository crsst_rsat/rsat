// config_reg.v

module config_reg (reset, config_data_in, we, config_data_out);

input  [15:0]   config_data_in;
input           we;
input           reset;

output [15:0]   config_data_out;
reg    [15:0]   config_data_out;
 
//assign  config_data_out = we?config_data_in:config_data_out; 

 always @ *
    if (reset)
       config_data_out      <= 16'b0000_0000_0000_0000;
    else if (we)
       config_data_out      <=  config_data_in;
    else
       config_data_out      <=  config_data_out;
           
endmodule 