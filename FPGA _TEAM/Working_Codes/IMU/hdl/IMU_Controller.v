// IMU_Controller.v

module IMU_SPI_Controller (PCLK, clk_1us, reset, PREADY, PWRITE, PENABLE, PSEL,
                       PWDATA, PADDR, SPIRXAVAIL, SPITXRFM, SPIOEN, SPIMODE, PRESETN,
                       IMU_datardy, buffer_we, buffer_waddr, mux_data_sel, demux_data_sel, demux_en,
                       MP_config_en, MP_diagnosis_en, MP_sensor_re, MP_buffer_re, 
                       FPGA_status_we, config_en_rst, data_rdy, data_rdy_en, data_NA,data_NA_clr,
                       diagnosis_en_rst, data_NA_en);

input        IMU_datardy;

input        PCLK, clk_1us;
input        reset;

input        PREADY;

input        MP_config_en;
input        MP_diagnosis_en;
input        MP_sensor_re;
input        MP_buffer_re;

output       data_rdy;
output       data_rdy_en;
output       data_NA;
output       FPGA_status_we;
output       config_en_rst;
output       diagnosis_en_rst;
output       mux_data_sel;
output       demux_data_sel;
output       demux_en;

output  reg  PRESETN;
output  reg  PWRITE;
output  reg  PENABLE;
output  reg  PSEL;
output  reg [15:0]  PWDATA;
output  reg [6:0]   PADDR;

output       buffer_we;
output [4:0] buffer_waddr;

reg          buffer_we;
reg    [4:0] buffer_waddr;

reg          data_rdy;
reg          data_rdy_en;
reg          data_NA;
reg          FPGA_status_we;
reg          config_en_rst;
reg          diagnosis_en_rst;
reg          mux_data_sel;
reg          demux_data_sel;
reg          demux_en;

input        SPIRXAVAIL;
input        SPITXRFM;
input        SPIOEN;
input        SPIMODE;

//output  reg  SPISSI;
//output  reg  SPISDI;

reg   [5:0]  state;
reg   [5:0]  count;

wire  [5:0]  count_next;
wire  [7:0] SPIdata_OUT;

reg   [7:0] sensor_reg;
reg   [4:0]  sensor_addr;
reg          rom_en;

output reg   data_NA_en;
output reg          data_NA_clr;
reg          cnt_start, cnt_reset, stuck_rst;
reg   [15:0] time_out_counter;


assign  count_next = count + 1;
assign  SPIdata_OUT = rom_en? sensor_reg:8'bz;

always @ (rom_en or sensor_addr)
  begin
    case (sensor_addr)
      0 : sensor_reg = 8'h10;        //X_Gyro_Low
      1 : sensor_reg = 8'h12;        //X_Gyro_OUT
      2 : sensor_reg = 8'h14;        //Y_Gyro_Low
      3 : sensor_reg = 8'h16;        //Y_Gyro_OUT
      4 : sensor_reg = 8'h18;        //Z_Gyro_Low
      5 : sensor_reg = 8'h1A;        //Z_Gyro_OUT
      6 : sensor_reg = 8'h1C;        //X_Accl_Low
      7 : sensor_reg = 8'h1E;        //X_Accl_OUT
      8 : sensor_reg = 8'h20;        //Y_Accl_Low
      9 : sensor_reg = 8'h22;        //Y_Accl_OUT
      10: sensor_reg = 8'h24;        //Z_Accl_Low
      11: sensor_reg = 8'h26;        //Z_Accl_OUT
      12: sensor_reg = 8'h28;        //X_Magn_OUT
      13: sensor_reg = 8'h2A;        //Y_Magn_OUT
      14: sensor_reg = 8'h2C;        //Z_Magn_OUT
      15: sensor_reg = 8'h0E;        //Temp_OUT
      16: sensor_reg = 8'h0A;        //Diag_OUT
      17: sensor_reg = 8'h0A;        //Diag_OUT
      18: sensor_reg = 8'h0A;        //Dummy addess
      default: sensor_reg = 8'h10;   //X_Gyro_Low

    endcase
  end


//assign prv_state   =  state;

always @ (posedge PCLK or posedge reset)
  begin
    if (reset)
     begin
        data_NA             <= 0;
        data_NA_en          <= 0;
        stuck_rst           <= 0;
     end
    else if (time_out_counter==40000)
     begin
        data_NA_en          <= 1;
        data_NA             <= 1;
        stuck_rst           <= 1;
     end
    else
     begin
        data_NA             <= 0;
        data_NA_en          <= 0;
        stuck_rst           <= 0;
     end
   end
        
always @ (posedge clk_1us or posedge cnt_reset)
    if (cnt_reset)
        time_out_counter     <= 0;
    else if (cnt_start)
        time_out_counter     <= time_out_counter + 1;
    else
        time_out_counter     <= time_out_counter;


//reg SPICLKI;
always @ (posedge PCLK or posedge reset)
begin
  if (reset == 1)
    begin
       PWDATA           <= 16'b0000000000000000;
       PADDR            <= 7'b0000000;
       PSEL             <= 1'b0;
       PWRITE           <= 1'b0;
       PENABLE          <= 1'b0;
       state            <= 0;
       count            <= 0;
       data_rdy         <= 0;
       PRESETN          <= 0;
       buffer_we        <= 0;
       buffer_waddr     <= 0;
       rom_en           <= 0;
       sensor_addr      <= 0;
       mux_data_sel     <= 0;
       demux_data_sel   <= 0;
       demux_en         <= 0; 
       config_en_rst    <= 0;
       diagnosis_en_rst <= 0;
       cnt_reset        <= 1;
       cnt_start        <= 0;
       data_rdy_en      <= 0;
       data_NA_clr      <= 0;
    end
  else if (stuck_rst)
    begin
       PWDATA           <= 16'b0000000000000000;
       PADDR            <= 7'b0000000;
       PSEL             <= 1'b0;
       PWRITE           <= 1'b0;
       PENABLE          <= 1'b0;
       state            <= 0;
       count            <= 0;
       data_rdy         <= 0;
       PRESETN          <= 0;
       buffer_we        <= 0;
       buffer_waddr     <= 0;
       rom_en           <= 0;
       sensor_addr      <= 0;
       mux_data_sel     <= 0;
       demux_data_sel   <= 0;
       demux_en         <= 0; 
       config_en_rst    <= 0;
       diagnosis_en_rst <= 0;
       cnt_reset        <= 1;
       cnt_start        <= 0;
       data_rdy_en      <= 0;
       data_NA_clr      <= 0;
    end       
  else
    case (state)    
    0: begin
          PRESETN           <= 1;
          PWDATA            <= 16'b0000000000000000;
          PADDR             <= 7'b0000000;
          PSEL              <= 1'b0;
          PWRITE            <= 1'b0;
          PENABLE           <= 1'b0;
          data_rdy          <= 0;
          state             <= 1;
          count             <= 0;
          mux_data_sel      <= 0;
          demux_data_sel    <= 0;
          config_en_rst     <= 0;
          diagnosis_en_rst  <= 0;
          FPGA_status_we    <= 0;
          demux_en          <= 0;
          cnt_reset         <= 1;
          cnt_start         <= 0;
          data_rdy_en       <= 0;
          data_NA_clr       <= 1;

       end
    1: begin
         if (count == 5)
          begin
             PADDR          <= 7'b0000000;
             PSEL           <=1'b1;
             PWRITE         <=1'b1;
             PWDATA         <=16'b0000000000000011;
             state          <= 2;
             count          <= 0;
          end
         else
          begin
             count          <= count_next;
             state          <= 1;
          end
       end

    2: begin
             PENABLE        <= 1'b1;
             state          <= 3;
             count          <= 0;
       end

    3: begin
             PSEL           <= 1'b0;
             PWRITE         <= 1'b0;
             PENABLE        <= 1'b0;
             state          <= 4;
             count          <= 0;
       end
      
    4: begin
         if (count == 5)
          begin
             PADDR          <= 7'b0100100;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b1;
             PWDATA         <= 16'b0000000000000001;
             state          <= 5;
             count          <= 0;
          end
         else
          begin
             count          <= count_next;
             state          <= 4;
          end
       end

    5: begin
             PENABLE        <= 1'b1;
             state          <= 6;
             count          <= 0;
       end

    6: begin
             PSEL           <= 1'b0;
             PWRITE         <= 1'b0;
             PENABLE        <= 1'b0;
             state          <= 34;
             count          <= 0;
       end

   34: begin
        if (MP_buffer_re)
             state          <= 34;
        else
             state          <= 7;
       end

    7: begin
        if(MP_sensor_re)
          begin
             demux_data_sel <= 0;
             state          <= 8;
          end
        else if(MP_config_en)
          begin
             demux_data_sel <= 0;
             state          <= 16;
          end
        else if(MP_diagnosis_en)
          begin
             demux_data_sel <= 1;
             state          <= 22;
          end
        else
          begin
             demux_data_sel <= 0;
             state          <= 7;
          end
       end

    8: begin
             cnt_reset      <= 0;
             cnt_start      <= 1;
             data_rdy       <= 0;
        if (IMU_datardy)
          begin
            data_NA_clr     <= 1;
             state          <= 31;
          end
        else
             state          <= 8;
       end

   31: begin
             rom_en         <= 1;
             cnt_reset      <= 0;
             cnt_start      <= 1;
             data_NA_clr    <= 1;
             state          <= 9;
       end
    
    9: begin
             data_NA_clr    <= 0;
             rom_en         <= 1;
             cnt_reset      <= 0;
             cnt_start      <= 1;
        if (SPITXRFM)
          begin
             PADDR          <= 7'b0001100;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b1;
             PWDATA         <= {SPIdata_OUT,8'b00};
             state          <= 10;
          end
        else 
             state          <= 9;
        end

    10: begin
             cnt_reset      <= 1;
             cnt_start      <= 0;
             PENABLE        <= 1'b1;
             state          <= 11;
        end

    11: begin
             PSEL           <= 1'b0;
             PWRITE         <= 1'b0;
             PENABLE        <= 1'b0;
             rom_en         <= 0;
             state          <= 12;
        end

    12: begin
             cnt_reset      <= 0;
             cnt_start      <= 1;
          if (SPIRXAVAIL)
            begin
             PADDR          <= 7'b0001000;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b0;
             state          <= 13;
             demux_en       <= 1;
            end
          else
             state          <= 12;
        end

    13: begin
             cnt_reset      <= 1;
             cnt_start      <= 0;
             PENABLE        <= 1'b1;
             state          <= 14;
             buffer_we      <= 1;
        end

    14: begin
             demux_en       <= 0;
             buffer_we      <= 0;             
             PSEL           <= 1'b0;
             PWRITE         <= 1'b0;
             PENABLE        <= 1'b0;
             buffer_waddr   <= buffer_waddr + 1;
             sensor_addr    <= sensor_addr + 1;
             state          <= 15;
        end

    15: begin
           if (buffer_waddr == 19)
            begin
             buffer_waddr   <= 0;
             data_rdy_en    <= 1;
             data_rdy       <= 1;
             state          <= 33;
            end
           else
             state          <= 31;
        end

    33: begin
             data_rdy_en    <= 1;
             data_rdy       <= 1;
             state          <= 0;
        end

//Configuration of IMU States 

    16: begin
           mux_data_sel     <= 1;
           demux_data_sel   <= 1;
           state            <= 17;
        end

    17: begin
             cnt_reset      <= 0;
             cnt_start      <= 1;
        if (SPITXRFM)
          begin
             PADDR          <= 7'b0001100;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b1;
             PWDATA         <= 16'bz;
             state          <= 18;
          end
        else 
             state          <= 17;
       end

    18: begin
             cnt_reset      <= 1;
             cnt_start      <= 0;
             PENABLE        <= 1'b1;
             state          <= 19;
       end

    19: begin
             PSEL              <= 1'b0;
             PWRITE            <= 1'b0;
             PENABLE           <= 1'b0;
             config_en_rst     <= 1;
             state             <= 20;
        end

    20: begin
             FPGA_status_we    <= 1;
             config_en_rst     <= 1;
             state             <= 21;
        end

    21: begin
             FPGA_status_we    <= 0;
             config_en_rst     <= 0;
             state             <= 7;
        end

//Diagnosis states for IMU
             
    22: begin
           mux_data_sel         <= 1;
           demux_data_sel       <= 1;
           count                <= 0;
           state                <= 23;
        end

    23: begin
             cnt_reset      <= 0;
             cnt_start      <= 1;
        if (SPITXRFM)
          begin
             PADDR          <= 7'b0001100;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b1;
             PWDATA         <= 16'bz;
             state          <= 24;
          end
        else 
             state          <= 23;
       end

    24: begin
             cnt_reset      <= 1;
             cnt_start      <= 0;
             PENABLE        <= 1'b1;
             state          <= 25;
       end

    25: begin
             PSEL           <= 1'b0;
             PWRITE         <= 1'b0;
             PENABLE        <= 1'b0;
             state          <= 26;
        end

    26: begin
             cnt_reset      <= 0;
             cnt_start      <= 1;
          if (SPIRXAVAIL)
            begin
             PADDR          <= 7'b0001000;
             PSEL           <= 1'b1;
             PWRITE         <= 1'b0;
             state          <= 27;
             demux_en       <= 1;
            end
          else
             state          <= 26;
        end

    27: begin
             cnt_reset      <= 1;
             cnt_start      <= 0;
             PENABLE        <= 1'b1;
             demux_en       <= 1;
             state          <= 28;
        end

    28: begin
             PSEL               <= 1'b0;
             PWRITE             <= 1'b0;
             PENABLE            <= 1'b0;
             demux_en           <= 0;
             count              <= count_next;
             state              <= 32;
        end

    32: begin
          if (count == 2)
            begin            
             diagnosis_en_rst   <= 1;
             count              <= 0;
             state              <= 29;
            end
          else
             state              <= 23;
        end

    29: begin
             FPGA_status_we    <= 1;
             diagnosis_en_rst  <= 1;
             state             <= 30;
        end

    30: begin
             FPGA_status_we    <= 0;
             diagnosis_en_rst  <= 0;
             state             <= 7;
        end
   

    endcase
end
endmodule