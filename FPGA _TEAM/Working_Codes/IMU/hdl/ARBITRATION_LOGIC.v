// Arbitration_logic.v

module arbitration_logic (clk, reset, MP_re, MP_we, IMU_CS_n, MP_addr, status_we, 
                          buffer_re, status_MP_re, config_MP_we, diag_MP_re);

input        MP_re;
input        MP_we;
input        IMU_CS_n;
input        clk, reset;

input [8:0]  MP_addr;

output       status_we;
output       buffer_re;
//output [4:0] buf_rd_addr;
output       status_MP_re;
output       config_MP_we;
output       diag_MP_re;
                
reg          status_we;
wire          buffer_re;
//reg    [4:0] buf_rd_addr;
wire          status_MP_re;
reg          config_MP_we;
wire          diag_MP_re;

reg    [3:0] count,state;
wire   [3:0] count_next;
  
assign count_next = count + 1;

assign  buffer_re    = IMU_CS_n? (((MP_addr >= 8'h00) & (MP_addr < 8'hf0))&(MP_re == 1)&(MP_we != 1))?1:0:0;  
assign  status_MP_re = IMU_CS_n? ((MP_re == 1) & (MP_addr == 8'hff)&(MP_we != 1))?1:0:0 ; 
assign  diag_MP_re   = IMU_CS_n? ((MP_re == 1) & (MP_addr == 8'hfa)&(MP_we != 1))?1:0:0;


always @ (posedge clk or posedge reset)
  begin
    if (reset == 1)
      begin
       status_we        <= 0;
       config_MP_we     <= 0;
       count            <= 0;
       state            <= 0;
      end
    else if (IMU_CS_n == 1)
        case (state)

        0: begin
                count        <= 0;
                status_we    <= 0;
                config_MP_we <= 0;
             if ((MP_we == 1) & (MP_addr == 8'hff))
                state        <= 1;
             else if ((MP_we == 1) & (MP_addr == 8'hf0))
                state        <= 3;
             else
                state        <= 0;
           end

        1: begin
                status_we    <= (MP_we == 1) & (MP_addr == 8'hff) & (count > 3) & (count < 7);
             if (count == 9)
               begin
                count        <= 0;
                state        <= 2;
               end
             else   
               begin
                count        <= count_next;
                state        <= 1;
               end
           end

        2: begin
             if (MP_we == 0)
                state        <= 0;
             else
                state        <= 2;
           end

        3: begin
                config_MP_we <= (MP_we == 1) & (MP_addr == 8'hf0) & (count > 3) & (count < 7)? 1 : 0;
             if (count == 9)
               begin
                count        <= 0;
                state        <= 4;
               end
             else   
               begin
                count        <= count_next;
                state        <= 3;
               end
           end

        4: begin
             if (MP_we == 0)
                state        <= 0;
             else
                state        <= 4;
           end
        default:   state             <= 0;
        endcase
        else
        begin
           status_we    <= 1'b0;
        end
end
endmodule

