// Status_reg.v

module Status_reg (data_ready, buf_read_en, config_en, diagnosis_en, Ren, Wen, status);

input           data_ready;
input           buf_read_en;
input           config_en;
input           diagnosis_en;
input           Ren;
input           Wen;

output [15:0]   status;

wire   [15:0]   status_data_temp;
wire   [15:0]   status_data;


assign status_temp = {12'b000000000000, diagnosis_en, config_en, buf_read_en, data_ready};
assign status_data = Wen? status_temp : status_data;
assign status      = Ren? status_data : status;

endmodule

