// DATA_BUFFER_14x16.v

module DATA_BUFFER_14x16 (IMU_data_in, buf_addr, Wen, Ren, IMU_data_out);

input   [15:0] IMU_data_in;
input   [3:0]  buf_addr;
input          Wen;
input          Ren;

output  [15:0] IMU_data_out;

reg     [15:0] data_buffer [13:0];

always @ *
  begin
    if (Wen)
        data_buffer[buf_addr]  <= IMU_data_in;
    else if (Ren)
        IMU_data_out           <= data_buffer[buf_addr];
    else
        IMU_data_out           <= IMU_data_out;
  end

endmodule
