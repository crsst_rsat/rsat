// Adr_ROM.v

module Adr_ROM (clk, reset, addr, imu_adr, ren);

input        clk, reset, ren;
input  [3:0] addr;

output [15:0]imu_adr;
reg    [15:0]imu_adr;

always @ (posedge clk or posedge reset)
  begin
    if (reset)
        imu_adr <= 0;
    else if (ren)
        case(addr)
        0: imu_adr  <= ;
        1: imu_adr  <= ;
        2 imu_adr   <= ;
        3: imu_adr  <= ;
        4: imu_adr  <= ;
        5: imu_adr  <= ;
        6: imu_adr  <= ;
        7: imu_adr  <= ;
        8: imu_adr  <= ;
        9: imu_adr  <= ;
       10: imu_adr  <= ;
       11: imu_adr  <= ;
       12: imu_adr  <= ;
       13: imu_adr  <= ;
       14: imu_adr  <= ;
       15: imu_adr  <= ;
       16: imu_adr  <= ;
       17: imu_adr  <= ;
       18: imu_adr  <= ;
       19: imu_adr  <= ;
       default:
       endcase
  end
endmodule

             