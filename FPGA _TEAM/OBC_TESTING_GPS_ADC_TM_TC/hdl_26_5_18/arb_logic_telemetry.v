`timescale 1ns / 1ps

module arb_log_TM(
    input [15:0] status,
	 //output [0:1] SPC_sel,
	 output  [3:0] address_dual_mem,
    output reg SR_WR,
    //output SPC_RD,
	 
	 output dual_mem_WR,
	input clock,
   input RD,
	input CS,
	input master_reset,
   input WR,
   input [8:0] ADD,
	input [15:0] MP_datain,
    output [15:0] MP_dataout, //ask ma'am write from processorto dual mem and read from status to processor
	output [15:0] data_out,  //sending input 16 bits to output
	output reg WR_bit
    );
	reg [4:0]count;
   reg temp; 
	
	assign  MP_dataout= ((ADD==9'h0FF) & CS==1 & ~WR & RD)?status:16'bz;
	assign dual_mem_WR = master_reset?1'b0:((ADD[7:4]==4'hA & CS==1 & WR)?1'b1:1'b0);
	//assign dual_mem_WR=1'b1;
	assign address_dual_mem = (ADD[7:4]==4'hA & CS==1 & WR)?ADD[3:0]:4'hz;
	//assign address_dual_mem = (ADD[7:4]==4'hB & CS==1 & WR)?ADD[3:0]:4'hzz;
	assign data_out=(CS==1 & WR)?MP_datain:16'Hz;

//	
//always@(posedge master_reset)
// begin
//  if(master_reset)
//  SR_WR <= 1'b0;
//  else 
//  begin
//		SR_WR  <= (count >= 6 & count <10)? 1'b1:1'b0;
//		WR_bit <= (count >= 3 & count <10)? data_line[0]:WR_bit;
//  end
//end

always@(*)
begin //we shud check cs for writing
SR_WR  = (count >= 1 & count<3)? 1'b1:1'b0;
WR_bit = SR_WR? MP_datain[0]:WR_bit;
end

always@(posedge master_reset or posedge clock)
begin
	if(master_reset)
	begin
	    count <= 0;
		//temp  <= 1;
		//WR_bit<=0;
	end
	else
	begin
		if(WR && ADD==8'hFF && CS==1)
		 count<=(count<=6)?count+1:count;
		else
		count<=0;
	 end   
end 
endmodule