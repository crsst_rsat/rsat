module latch3_heater_cntrls (PO_Reset, MP_data_in, latch3_CS, MP_write,
                             Heater1_ON_OFF, Heater2_ON_OFF, Heater3_ON_OFF, 
                             Heater4_ON_OFF, Heater5_ON_OFF, Heater6_ON_OFF,
                             RW1_ON_OFF, RW2_ON_OFF, RW3_ON_OFF, RW4_ON_OFF,
                             ANT_DEPLOY, SA1_DEPLOY, SA2_DEPLOY, TM_DS_EN);


input       PO_Reset;
input       MP_write;
input [15:0]MP_data_in;
input       latch3_CS;

output      Heater1_ON_OFF;
output      Heater2_ON_OFF;
output      Heater3_ON_OFF;
output      Heater4_ON_OFF;
output      Heater5_ON_OFF;
output      Heater6_ON_OFF;
output      RW1_ON_OFF;
output      RW2_ON_OFF;
output      RW3_ON_OFF;
output      RW4_ON_OFF;
output      ANT_DEPLOY;
output      SA1_DEPLOY;
output      SA2_DEPLOY;
output      TM_DS_EN;

reg      Heater1_ON_OFF;
reg      Heater2_ON_OFF;
reg      Heater3_ON_OFF;
reg      Heater4_ON_OFF;
reg      Heater5_ON_OFF;
reg      Heater6_ON_OFF;
reg      RW1_ON_OFF;
reg      RW2_ON_OFF;
reg      RW3_ON_OFF;
reg      RW4_ON_OFF;
reg      ANT_DEPLOY;
reg      SA1_DEPLOY;
reg      SA2_DEPLOY;
reg      TM_DS_EN;

wire [15:0] data_temp;

assign data_temp = (latch3_CS & ~MP_write)? MP_data_in : data_temp;

always @ (*)
  if (PO_Reset)
    begin
      Heater1_ON_OFF       = 0;
      Heater2_ON_OFF       = 0;
      Heater3_ON_OFF       = 0;
      Heater4_ON_OFF       = 0;
      Heater5_ON_OFF       = 0;
      Heater6_ON_OFF       = 0;
      RW1_ON_OFF           = 0;
      RW2_ON_OFF           = 0;
      RW3_ON_OFF           = 0;
      RW4_ON_OFF           = 0;
      ANT_DEPLOY           = 0;
      SA1_DEPLOY           = 0;
      SA2_DEPLOY           = 0;
      TM_DS_EN             = 0;
    end
  else if (latch3_CS & ~MP_write)
    begin
      Heater1_ON_OFF       = data_temp[0];
      Heater2_ON_OFF       = data_temp[1];
      Heater3_ON_OFF       = data_temp[2];
      Heater4_ON_OFF       = data_temp[3];
      Heater5_ON_OFF       = data_temp[4];
      Heater6_ON_OFF       = data_temp[5];
      RW1_ON_OFF           = data_temp[6];
      RW2_ON_OFF           = data_temp[7];
      RW3_ON_OFF           = data_temp[8];
      RW4_ON_OFF           = data_temp[9];
      ANT_DEPLOY           = data_temp[10];
      SA1_DEPLOY           = data_temp[11];
      SA2_DEPLOY           = data_temp[12];
      TM_DS_EN             = data_temp[13];
    end
  else 
    begin
      Heater1_ON_OFF       = Heater1_ON_OFF;
      Heater2_ON_OFF       = Heater2_ON_OFF;
      Heater3_ON_OFF       = Heater3_ON_OFF;
      Heater4_ON_OFF       = Heater4_ON_OFF;
      Heater5_ON_OFF       = Heater5_ON_OFF;
      Heater6_ON_OFF       = Heater6_ON_OFF;
      RW1_ON_OFF           = RW1_ON_OFF;
      RW2_ON_OFF           = RW2_ON_OFF;
      RW3_ON_OFF           = RW3_ON_OFF;
      RW4_ON_OFF           = RW4_ON_OFF;
      ANT_DEPLOY           = ANT_DEPLOY;
      SA1_DEPLOY           = SA1_DEPLOY;
      SA2_DEPLOY           = SA2_DEPLOY;
      TM_DS_EN             = TM_DS_EN;
    end
    endmodule