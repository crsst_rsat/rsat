module latch2_on_off_reset ( PO_Reset, MP_data_in, latch2_CS, MP_write,
                      IMU1_ON_OFF, IMU1_Reset, IMU2_ON_OFF, IMU2_Reset,
                      GPS1_ON_OFF, GPS1_Reset, GPS2_ON_OFF, GPS2_Reset,
                      MTR_ON_OFF, Payload_ON_OFF, X_Tx_ON_OFF, RF_Tx_ON_OFF, 
                      SPARE1_ON_OFF, SPARE2_ON_OFF);


input       PO_Reset;
input       MP_write;
input [15:0]MP_data_in;
input       latch2_CS;

output      IMU1_ON_OFF;
output      IMU1_Reset;
output      IMU2_ON_OFF;
output      IMU2_Reset;
output      GPS1_ON_OFF;
output      GPS1_Reset;
output      GPS2_ON_OFF;
output      GPS2_Reset;
output      MTR_ON_OFF; 
output      Payload_ON_OFF;
output      X_Tx_ON_OFF;
output      RF_Tx_ON_OFF;
output      SPARE1_ON_OFF;
output      SPARE2_ON_OFF;

reg      IMU1_ON_OFF;
reg      IMU1_Reset;
reg      IMU2_ON_OFF;
reg      IMU2_Reset;
reg      GPS1_ON_OFF;
reg      GPS1_Reset;
reg      GPS2_ON_OFF;
reg      GPS2_Reset;
reg      MTR_ON_OFF; 
reg      Payload_ON_OFF;
reg      X_Tx_ON_OFF;
reg      RF_Tx_ON_OFF;
reg      SPARE1_ON_OFF;
reg      SPARE2_ON_OFF;

wire [15:0] data_temp;

assign data_temp = (latch2_CS & ~MP_write)? MP_data_in : data_temp;

always @ (*)
  if (PO_Reset)
    begin
      IMU1_ON_OFF       = 0;
      IMU1_Reset        = 1;
      IMU2_ON_OFF       = 0;
      IMU2_Reset        = 1;
      GPS1_ON_OFF       = 0;
      GPS1_Reset        = 1;
      GPS2_ON_OFF       = 0;
      GPS2_Reset        = 1;
      MTR_ON_OFF        = 0;
      Payload_ON_OFF    = 0;
      X_Tx_ON_OFF       = 0;
      RF_Tx_ON_OFF      = 0;
      SPARE1_ON_OFF     = 1;
      SPARE2_ON_OFF     = 1;
    end
  else if (latch2_CS & ~MP_write)
    begin
      IMU1_ON_OFF       = data_temp[0];
      IMU1_Reset        = data_temp[1];
      IMU2_ON_OFF       = data_temp[2];
      IMU2_Reset        = data_temp[3];
      GPS1_ON_OFF       = data_temp[4];
      GPS1_Reset        = data_temp[5];
      GPS2_ON_OFF       = data_temp[6];
      GPS2_Reset        = data_temp[7];
      MTR_ON_OFF        = data_temp[8];
      Payload_ON_OFF    = data_temp[9];
      X_Tx_ON_OFF       = data_temp[10];
      RF_Tx_ON_OFF      = data_temp[11];
      SPARE1_ON_OFF     = data_temp[12];
      SPARE2_ON_OFF     = data_temp[13];
    end
  else 
    begin
      IMU1_ON_OFF       = IMU1_ON_OFF;
      IMU1_Reset        = IMU1_Reset;
      IMU2_ON_OFF       = IMU2_ON_OFF;
      IMU2_Reset        = IMU2_Reset;
      GPS1_ON_OFF       = GPS1_ON_OFF;
      GPS1_Reset        = GPS1_Reset;
      GPS2_ON_OFF       = GPS2_ON_OFF;
      GPS2_Reset        = GPS2_Reset;
      MTR_ON_OFF        = MTR_ON_OFF;
      Payload_ON_OFF    = Payload_ON_OFF;
      X_Tx_ON_OFF       = X_Tx_ON_OFF;
      RF_Tx_ON_OFF      = RF_Tx_ON_OFF;
      SPARE1_ON_OFF     = SPARE1_ON_OFF;
      SPARE2_ON_OFF     = SPARE2_ON_OFF;
    end
    endmodule