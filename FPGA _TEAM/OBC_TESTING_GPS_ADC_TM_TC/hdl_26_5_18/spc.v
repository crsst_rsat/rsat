module SPC(clock3125,clock40,master_reset,write_sig,read_sig,rand_data_out,busy,data_16,sel,zero_status);
input clock3125,clock40,master_reset,write_sig,read_sig,rand_data_out;
output reg busy;
output reg [0:15] data_16;
input [0:1] sel;
reg [0:1]  present_state;
reg [0:55] store;
reg [0:1]  count;
reg [0:5]  count_in;
reg [0:15] mem [0:3];
reg temp;
input zero_status;

wire rand_data_out_wire;

assign rand_data_out_wire=rand_data_out;

always @(*)
    data_16  <= (read_sig == 1'b0)?mem[sel]:16'hzzzz;

//sequential realisations
always @(negedge clock3125 or posedge master_reset) begin
	if(master_reset) begin
		store<=56'h00000000000000;
		present_state <= 2'b00;
	    end
	
	else 
	begin
	case(present_state)
	2'b00:
            begin
			store<=56'h00000000000000;
			if(~write_sig)
				present_state<=2'b01;
			else	
				present_state<=2'b00;
		    end
		  
	2'b01:
          begin

		  if(count_in<= 55)
		  begin
            	store  <= {store,rand_data_out_wire};
				present_state <= 2'b01;
		  end
			
		  else
		  begin
				store  <= {store,rand_data_out_wire};
				present_state <= 2'b10;
		  end 
          end		  
	2'b10: 
	    
	    begin
        //{mem[3],mem[2],mem[1],mem[0]}<={store,8'h00};
		if(read_sig ==1'b1)
		    begin
			store  		  <= store;
    		present_state <= 2'b10;
		    end
	    else if(write_sig==1'b0)
		    begin
			store  		  <= store;
			present_state <= 2'b01;
		    end
		end
	  
	endcase
    end
		
end
//
//always@ (negedge read_sig or posedge master_reset)begin
//    if(master_reset)
//        busy <= 1'b1;
//    else begin
//        if(sel== 3)
//            busy <= 1'b1;
//        else
//            busy <= 1'b0;
//        end
//    end

always @ (posedge read_sig or posedge master_reset) begin
    if(master_reset)
        temp <= 0;
    else if(read_sig & sel==3 & busy == 0)
            temp <= 1;
    else if(busy == 1)
        temp <= 0;
    else temp <= temp;
   end



always @ (present_state or master_reset or temp)
begin
    if (master_reset)
        busy<=1'b1;
    else
    case(present_state)
	2'b00:busy <= 1'b1;
	2'b01:busy <= 1'b1;
	2'b10:
          if (sel == 2'b11 & temp)
              busy <= 1'b1;
          else
              begin
                {mem[3],mem[2],mem[1],mem[0]}<={store,8'h00};
                busy <=  1'b0;
              end
     default: busy <= 1'bz;
    endcase
end
//always @ (*)
//     begin
//     {mem[3],mem[2],mem[1],mem[0]} = (present_state== 2'b10)?{store,8'h00}:64'h0;
//     end

always @(negedge clock3125 or posedge master_reset)
begin
	
	if(master_reset) 
		count_in <= 6'b000000;
	else if(count_in==56 & write_sig) 
		count_in <= 6'b000000;
	else if(present_state == 2'b01 & write_sig) 
		count_in <= count_in + 1;
    else 
        count_in <= count_in;
	
end
endmodule
	  