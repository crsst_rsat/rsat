// Adrs_decoder.v

module adrs_decoder (reset, MP_ios_n, MP_addr, adc_cs, gps1_cs, gps2_cs, imu1_cs, imu2_cs, heater_cs,
                     torquer_cs, RW_cs, telemetry_cs, telecommand_cs, minor_cyc_cs, payload_cs,
                     op_latch1_cs, op_latch2_cs, op_latch3_cs, op_latch4_cs, op_latch5_cs,
                     ip_latch1_cs, ip_latch2_cs, ip_latch3_cs, ip_latch4_cs,op_latch6_cs);

input        MP_ios_n;
input        reset;
input [6:0] MP_addr;

output       adc_cs;
output       gps1_cs;
output       gps2_cs;
output       imu1_cs;
output       imu2_cs;
output       heater_cs;
output       torquer_cs;
output       RW_cs;
output       telemetry_cs;
output       telecommand_cs;
output       minor_cyc_cs;
output       payload_cs;
output       op_latch1_cs;
output       op_latch2_cs;
output       op_latch3_cs;
output       op_latch4_cs;
output       op_latch5_cs;
output       ip_latch1_cs;
output       ip_latch2_cs;
output       ip_latch3_cs, ip_latch4_cs, op_latch6_cs;

reg          adc_cs;
reg          gps1_cs;
reg          gps2_cs;
reg          imu1_cs;
reg          imu2_cs;
reg          heater_cs;
reg          torquer_cs;
reg          RW_cs;
reg          telemetry_cs;
reg          telecommand_cs;
reg          minor_cyc_cs;
reg          payload_cs;
reg          op_latch1_cs;
reg          op_latch2_cs;
reg          op_latch3_cs;
reg          op_latch4_cs;
reg          op_latch5_cs;
reg          ip_latch1_cs;
reg          ip_latch2_cs;
reg          ip_latch3_cs, ip_latch4_cs, op_latch6_cs;

wire   [6:0] MP_cs_add; 

assign MP_cs_add      = MP_addr; 

always @ (MP_ios_n, MP_addr, reset, MP_cs_add)
begin
    if(reset)
      begin
        adc_cs          <= 0;
        gps1_cs         <= 0;
        gps2_cs         <= 0;
        imu1_cs         <= 0;
        imu2_cs         <= 0;
        heater_cs       <= 0;
        torquer_cs      <= 0;
        RW_cs           <= 0;
        telemetry_cs    <= 0;
        telecommand_cs  <= 0;
        minor_cyc_cs    <= 0;
        payload_cs      <= 0;
        op_latch1_cs    <= 0;
        op_latch2_cs    <= 0;
        op_latch3_cs    <= 0;
        op_latch4_cs    <= 0;
        op_latch5_cs    <= 0;   
        ip_latch1_cs    <= 0;
        ip_latch2_cs    <= 0;
        ip_latch3_cs    <= 0;
        ip_latch4_cs    <= 0;
        op_latch6_cs    <= 0;
      end
    else if(MP_ios_n==0)
        case (MP_cs_add)
            0: begin
                adc_cs          <= 1;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;  
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            1: begin
                adc_cs          <= 0;
                gps1_cs         <= 1;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0; 
                op_latch6_cs    <= 0;
               end

            2: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 1;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            3: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 1;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            4: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 1;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            5: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 1;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            6: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 1;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            7: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 1;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            8: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 1;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            9: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 1;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                 ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            10:begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 1;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            11: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 1;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            12: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 1;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            13: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 1;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            14: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 1;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                 ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            15: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 1;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                 ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            16: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 1;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            17: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 1;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
                end

            18: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 1;
                ip_latch3_cs    <= 0;
                 ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            19: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 1;
                 ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end

            20: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 1;
                op_latch6_cs    <= 0;
               end

21: begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0;    
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 1;
               end

      default:begin
                adc_cs          <= 0;
                gps1_cs         <= 0;
                gps2_cs         <= 0;
                imu1_cs         <= 0;
                imu2_cs         <= 0;
                heater_cs       <= 0;
                torquer_cs      <= 0;
                RW_cs           <= 0;
                telemetry_cs    <= 0;
                telecommand_cs  <= 0;
                minor_cyc_cs    <= 0;
                payload_cs      <= 0;
                op_latch1_cs    <= 0;
                op_latch2_cs    <= 0;
                op_latch3_cs    <= 0;
                op_latch4_cs    <= 0;
                op_latch5_cs    <= 0; 
                ip_latch1_cs    <= 0;
                ip_latch2_cs    <= 0;
                ip_latch3_cs    <= 0;
                ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
               end
              endcase

      else
       begin
        adc_cs          <= 0;
        gps1_cs         <= 0;
        gps2_cs         <= 0;
        imu1_cs         <= 0;
        imu2_cs         <= 0;
        heater_cs       <= 0;
        torquer_cs      <= 0;
        RW_cs           <= 0;
        telemetry_cs    <= 0;
        telecommand_cs  <= 0;
        minor_cyc_cs    <= 0;
        payload_cs      <= 0;
        op_latch1_cs    <= 0;
        op_latch2_cs    <= 0;
        op_latch3_cs    <= 0;
        op_latch4_cs    <= 0;
        op_latch5_cs    <= 0; 
        ip_latch1_cs    <= 0;
        ip_latch2_cs    <= 0;
        ip_latch3_cs    <= 0;
        ip_latch4_cs    <= 0;
                op_latch6_cs    <= 0;
      end

end
endmodule






    