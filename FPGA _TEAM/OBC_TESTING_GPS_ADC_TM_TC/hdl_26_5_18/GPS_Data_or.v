// Data_or.v

module GPS_data_sel (status_out, GPSdata_out, MP_data, MP_st_re, MP_data_re);

input   [15:0] status_out;
input   [15:0] GPSdata_out;
input          MP_st_re;
input          MP_data_re;

output  reg [15:0] MP_data;

//assign  MP_data = MP_st_re ? status_out:MP_data_re? GPSdata_out:16'bz;

always @ *
  if (MP_st_re)
    MP_data     <= status_out;
  else if (MP_data_re)
    MP_data     <= GPSdata_out;
  else
    MP_data     <= 16'bz;
endmodule