///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: clk_200us.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module clk_200us( clk, reset, clk_200, MP_Reset);
input       clk, reset;
output  reg clk_200;
output      MP_Reset;

reg [11:0] count;

assign MP_Reset = ~ reset;
always @ (posedge clk or posedge reset)
if (reset)
  begin
    count       <= 0;
    clk_200     <= 0;
  end
 else if (count == 2499)
  begin
    count       <= 0;
    clk_200     <= ~clk_200;
  end
else
  begin
    count       <= count + 1;
    clk_200     <= clk_200;
  end

    


//<statements>

endmodule

