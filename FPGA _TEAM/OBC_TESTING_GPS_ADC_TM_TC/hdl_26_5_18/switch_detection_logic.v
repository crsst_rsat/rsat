// switch_detection_logic.v

module switch_detection_logic (clk, reset, MP_P1, MP_P2, MP_N1, MP_N2, 
                                delay_done1, delay_done2, delay1, delay2, store1, store2);
input       clk;
input       reset;
input       MP_P1;
input       MP_P2;
input       MP_N1;
input       MP_N2;
input       delay_done2, delay_done1;

output      delay1, delay2;
output      store1, store2;
//reg         delay1, delay2;

reg         MP_P1_prv, MP_P2_prv, MP_N1_prv, MP_N2_prv;

wire        P1_switch, N1_switch, P2_switch, N2_switch, store1, store2;

assign P1_switch = ((MP_P1_prv == 0) & (MP_P1 == 1)) | ((MP_P1_prv == 1) & (MP_P1 == 0));
assign N1_switch = ((MP_N1_prv == 0) & (MP_N1 == 1)) | ((MP_N1_prv == 1) & (MP_N1 == 0));
assign P2_switch = ((MP_P2_prv == 0) & (MP_P2 == 1)) | ((MP_P2_prv == 1) & (MP_P2 == 0));
assign N2_switch = ((MP_N2_prv == 0) & (MP_N2 == 1)) | ((MP_N2_prv == 1) & (MP_N2 == 0));

assign delay1     = (P1_switch & N1_switch)?1: delay_done1? 0:delay1;
assign delay2     = (P2_switch & N2_switch)?1: delay_done2? 0:delay2;
assign store1     = (P1_switch & N1_switch)? 1: 0;
assign store2     = (P2_switch & N2_switch)? 1: 0;


always @ (posedge clk or posedge reset)
begin
    if (reset)
      begin
         MP_P1_prv      <= 0;
         MP_P2_prv      <= 0;
         MP_N1_prv      <= 0;
         MP_N2_prv      <= 0;
      end
    else
      begin
         MP_P1_prv  <= MP_P1;
         MP_P2_prv  <= MP_P2;
         MP_N1_prv  <= MP_N1;
         MP_N2_prv  <= MP_N2;
      end
end

endmodule
