// GPS_controller.v // State 20 occupied
module GPS_controller (clk,clk_2us,reset,vpp,rxrdy, Txrdy, pdata,UART_RE,presetn,fault_clr, data_NA_clr, config_NMEA_msg,
                       buf_address,buf_we, buf_data, data_NA,fault,fault_en, data_NA_en, datardy, MP_status_re, MP_GPS_config_en,
                       framing_err_in, parity_err_in, overflow_err_in,framing_err, config_bytes, config_addr_FPGA,
                       parity_err, overflow_err,PPS_overflow, clk_100us, UART_WEN, Status_we, config_en_rst,config_type);

input       clk, clk_100us, clk_2us;
input       reset;
input       vpp;
input       rxrdy;
input       Txrdy;
input       MP_GPS_config_en;
input       MP_status_re;
input       framing_err_in;
input       parity_err_in;
input       overflow_err_in;
input [3:0] config_bytes;
input [2:0] config_NMEA_msg;

input [7:0] pdata;
input [2:0] config_type;
output      UART_RE;
output      presetn;
output      framing_err;
output      parity_err;
output      overflow_err;
output      fault_clr, data_NA_clr;
output[7:0] buf_address;
output      buf_we;
output[15:0]buf_data;
output      data_NA,fault;
output      datardy;
output      UART_WEN;
output      Status_we;
output      config_en_rst;
output      PPS_overflow;
output      fault_en, data_NA_en;
output [3:0]config_addr_FPGA;

reg    [3:0]config_addr_FPGA;
reg         PPS_overflow;
reg         data_NA, fault;
reg         datardy;
reg         UART_RE;
reg         presetn;
reg         framing_err;
reg         parity_err;
reg         overflow_err;
reg         UART_WEN;
reg         Status_we;
reg         config_en_rst;

reg   [7:0] buf_address, buffer_pps;
reg         buf_we;
reg   [15:0]buf_data;

reg   [4:0] state;
wire  [4:0] prv_state;
reg   [10:0]count;  
reg   [7:0] GPS_data_MSB, GPS_data_LSB;
reg   [7:0] data_count; 

reg         mark_stop;     
reg         mark_start, cnt_start, cnt_reset;
reg   [13:0] PPS_mark;  
reg   [21:0] time_out_counter;     

reg         VPP_pvs;
wire        VPP_posedge, VPP_negedge;
   
wire        SR_fault, SR_data_NA;
reg         fault_en, data_NA_en;
reg         fault_clr, data_NA_clr, stuck_clr;

reg [8:0]   buffer_size;
reg [8:0]   config_msg_size;


assign VPP_posedge  = (VPP_pvs == 0) & (vpp == 1);
assign VPP_negedge  = (VPP_pvs == 1) & (vpp == 0);

always @ (posedge clk)// or posedge reset)
begin
    if (reset)
      buffer_size     <= 0;
    else 
      buffer_size     <= config_msg_size;
end

always @ *
if (reset)
  begin
        config_msg_size  <= 206;
        buffer_pps       <= 103;
     end
else if(MP_GPS_config_en) begin
   if(config_type == 1)
    begin
        config_msg_size  <= 206;
        buffer_pps       <= 103;
     end

   else if(config_type == 5)begin
   
   case (config_NMEA_msg)
  0: begin
        config_msg_size  <= 244;
        buffer_pps       <= 122;
     end
  1:  begin
        config_msg_size  <= 286;
        buffer_pps       <= 143;
     end
  2:  begin
        config_msg_size  <= 232;
        buffer_pps       <= 117;
     end
  3:  begin
        config_msg_size  <= 285;
        buffer_pps       <= 143;
     end
  4:  begin
        config_msg_size  <= 252;
        buffer_pps       <= 127;
     end
  5:  begin
        config_msg_size  <= 277;
        buffer_pps       <= 139;
     end
 // 5: config_msg_size  <= 241;
  6:  begin
        config_msg_size  <= 196;
        buffer_pps       <= 99;
     end
  7:  begin
        config_msg_size  <= 206;
        buffer_pps       <= 103;
     end
 default:  begin
        config_msg_size  <= 206;
        buffer_pps       <= 103;
     end
  endcase
  end//elseif
  else 
  config_msg_size<=config_msg_size;

 end//elseif
else 
config_msg_size<=config_msg_size;


always @ (posedge clk or posedge reset)
  begin
    if (reset)
       VPP_pvs      <= 0;
    else
       VPP_pvs      <= vpp;
  end        

//assign SR_fault    =  fault_en? fault : fault_clr? 0 : SR_fault;
//assign SR_data_NA  =  data_NA_en? data_NA : data_NA_clr? 0 : SR_data_NA;

assign prv_state   =  state;

always @ (posedge clk)// or posedge reset)
  begin
    if (reset)
     begin
        stuck_clr           <= 0;
        data_NA             <= 0;
        fault               <= 0;
        fault_en            <= 0;
        data_NA_en          <= 0;
     end
    else if ((prv_state == state)&(time_out_counter==22'd1000000))
     begin
        stuck_clr           <= 1;
        case (state)
        2: begin
            data_NA_en      <= 1;
            data_NA         <= 1;
           end
        4: begin
            data_NA_en      <= 1;
            data_NA         <= 1;
           end
        6: begin
            fault           <= 1;
            fault_en        <= 1;
           end
        9: begin
            fault           <= 1;
            fault_en        <= 1;
           end
        18: begin
            fault           <= 1;
            fault_en        <= 1;
           end
        
  default: begin
            data_NA_en      <= 0;
            data_NA         <= 0;
            fault           <= 0;
            fault_en        <= 0;
           end

        endcase
     end
    else if ((prv_state == state)&(time_out_counter<1000000))
     begin
        fault_en            <= 0;
        data_NA_en          <= 0;
        stuck_clr           <= 0;
        fault               <= 0;
        data_NA             <= 0;
     end
    else
     begin
        fault               <= 0;
        data_NA             <= 0;
        data_NA_en          <= 0;
        fault_en            <= 0;
        stuck_clr           <= 0;
     end
   end
        
always @ (posedge clk_2us or posedge cnt_reset)
    if (cnt_reset)
        time_out_counter     <= 0;
    else if (cnt_start)
        time_out_counter     <= time_out_counter + 1;
    else
        time_out_counter     <= time_out_counter;

always @ (posedge clk_100us or posedge reset)
    if (reset)
        PPS_mark     <= 0;
    else if (mark_start)
        PPS_mark     <= PPS_mark + 1;
    else if (mark_stop)
        PPS_mark     <= 0;
    else
        PPS_mark     <= PPS_mark;
     
always @ (posedge clk ) //or posedge reset)
    if (reset)
      PPS_overflow      <= 0;
    else if (PPS_mark == 14'd16383)
      PPS_overflow      <= 1;

always @ (posedge clk)// or posedge reset)
  begin
    if (reset)
      begin
        buf_we          <= 0;
        buf_address     <= 0;
        buf_data        <= 0;
        GPS_data_MSB    <= 0;
        GPS_data_LSB    <= 0;
        UART_RE         <= 1;
        presetn         <= 0;
        state           <= 0;
        count           <= 0;
        datardy         <= 0;
        framing_err     <= 0;
        parity_err      <= 0;
        overflow_err    <= 0;
        data_count      <= 0;
        mark_start      <= 0;
        mark_stop       <= 0;
        UART_WEN        <= 1;
        Status_we       <= 0;
        config_en_rst   <= 0;
        cnt_reset       <= 1;
        cnt_start       <= 0;
        fault_clr       <= 0;
        data_NA_clr     <= 0;
        config_addr_FPGA<= 0;
      end
    else if (stuck_clr)
      begin
        buf_we          <= 0;
        buf_address     <= 0;
        buf_data        <= 0;
        GPS_data_MSB    <= 0;
        GPS_data_LSB    <= 0;
        UART_RE         <= 1;
        presetn         <= 0;
        state           <= 0;
        count           <= 0;
        datardy         <= 0;
        framing_err     <= 0;
        parity_err      <= 0;
        overflow_err    <= 0;
        data_count      <= 0;
        mark_start      <= 0;
        mark_stop       <= 0;
        UART_WEN        <= 1;
        Status_we       <= 0;
        config_en_rst   <= 0;
        cnt_reset       <= 1;
        cnt_start       <= 0;
        fault_clr       <= 0;
        data_NA_clr     <= 0;
      end        
    else
      case (state)
      0: begin
                buf_we          <= 0;
                buf_address     <= 0;
                buf_data        <= 0;
                GPS_data_MSB    <= 0;
                GPS_data_LSB    <= 0;
                UART_RE         <= 1;
                count           <= 0;
                data_count      <= 0;
                mark_stop       <= 0;
                UART_WEN        <= 1;
                Status_we       <= 0;
                config_en_rst   <= 0;
                state           <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0;
                fault_clr       <= 0;
                data_NA_clr     <= 0;
                config_addr_FPGA<= 0;
         end 

      1: begin
                presetn         <= 1;        //uart reset???
                UART_RE         <= 1;
                count           <= 0;
                state           <= 2;
          end
               
      2: begin
            cnt_reset           <= 0;
            cnt_start           <= 1;     
            if (MP_status_re)                //If MP is reading buffer, dont write into memory
             begin
                buf_address     <= buffer_pps;
                state           <= 15;
             end
            else if (MP_GPS_config_en)          //jump to GPS configuration state 18
             begin
     //           buf_address     <= 0;
                state           <= 18;
             end
            else if (VPP_posedge)              // else wait for PPS pulse
              begin
                buf_address     <= 0;
                mark_stop       <= 1;
                mark_start      <= 0;
                state           <= 3;
                data_NA_clr     <= 1;
                data_count      <= 0;
              end
            else
                state           <= 2;
         end

      3: begin
                datardy         <= 0;
                framing_err     <= 0;
                parity_err      <= 0;
                overflow_err    <= 0;
                data_NA_clr     <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0;
                mark_stop       <= 1;
                mark_start      <= 0;
                state           <= 4;
         end
                
      4: begin
                data_NA_clr     <= 0;
                cnt_reset       <= 0;
                cnt_start       <= 1;
                mark_stop       <= 0;
                mark_start      <= 1;
                presetn         <= 0;
            if (VPP_negedge)
              begin
                presetn         <= 0;
                data_NA_clr     <= 1;
                state           <= 5;
              end
            else
                state           <= 4;
         end

     5: begin
                presetn         <= 1;
                data_NA_clr     <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0;
                state           <= 6;
         end

      6: begin
                presetn         <= 1;
                data_NA_clr     <= 0;
                cnt_reset       <= 0;
                cnt_start       <= 1;
            if (rxrdy == 1)
              begin
                fault_clr       <= 1;
                UART_RE         <= 1'b0;
                state           <= 23;
                data_count      <= data_count + 1;
              end
            else
                state           <= 6;
            end

          23: begin
             if(count==3)
             begin
              UART_RE         <= 1'b1;
              count           <=0;
              state           <=7;
             end
             else begin
             count          <=count+1;
             UART_RE        <= 1'b0;
             state          <=23;
             end
          end//23

      7: begin
                fault_clr       <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0; 
                UART_RE         <= 1'b1;
                count           <= 0;
                state           <= 8;
             end

      8: begin
                fault_clr       <= 0;
                UART_RE         <= 1'b1;
                count           <= 0;
              //  state           <= 9;
                framing_err     <= framing_err | framing_err_in;
                parity_err      <= parity_err | parity_err_in;
                overflow_err    <= overflow_err | overflow_err_in;
                data_count      <= data_count;   
                GPS_data_LSB    <= pdata;
                if (data_count == config_msg_size )  //189)
                  begin
                    GPS_data_MSB <= 8'b00000000;                    
                    state       <= 12;
                  end
                else
                    state       <= 9;
             end

      9: begin 
                UART_RE         <= 1'b1;
                cnt_reset       <= 0;
                cnt_start       <= 1;
            if (rxrdy == 1)
              begin
                fault_clr       <= 1;
                UART_RE         <= 1'b0;
                data_count      <= data_count+1;
                state           <= 10;
              end
            else
                state           <= 9;
          end

      10: begin
                fault_clr       <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0; 
                UART_RE         <= 1'b0;
                state           <= 11;
          end
      11: begin
                fault_clr       <= 0;
                UART_RE         <= 1'b1;
                count           <= 0;
                state           <= 12;
                GPS_data_MSB    <= pdata;
             end

      12: begin
            //if (data_count == config_msg_size)  //189)
                //buf_data        <= {8'b00000000,GPS_data_LSB};
            //else
                buf_data        <= {GPS_data_MSB,GPS_data_LSB};
                state           <= 13;
          end
            
     13 : begin
                mark_stop       <= 0;
                buf_we          <= 1;
                state           <= 14;
          end
         
     14 : begin
                buf_we          <= 0;
            if (data_count < config_msg_size ) //189)
              begin
                buf_address     <= buf_address + 1;
                state           <= 6;
              end
            else 
              begin
                //buf_we        <= 0;
                buf_address     <= buf_address + 1;
                datardy         <= 1;
                mark_stop       <= 0;               
                state           <= 15;
              //  Status_we       <= 1;
              end
          end
      
      15: begin
            if (MP_status_re == 1)
             begin
                buf_data        <= PPS_mark;
                state           <= 16;
             end
            else  
                state           <= 2;
          end
      
      16: begin
                buf_we          <= 1;
                state           <= 17;
          end

      17:  begin
                buf_we          <= 0;
             if (MP_status_re == 0)
                state           <= 0;
             else
                state           <= 17;
           end

      18: begin
                cnt_reset       <= 0;
                cnt_start       <= 1;
                config_addr_FPGA<= config_addr_FPGA;
            if (Txrdy)
              begin
                fault_clr       <= 1;
                UART_WEN        <= 0;
                state           <= 19;
              end
            else
              begin
                UART_WEN        <= 1;
                state           <= 18;
              end
           end

       19: begin
                fault_clr       <= 1;
                cnt_reset       <= 1;
                cnt_start       <= 0;
                UART_WEN        <= 0;
                state           <= 20;
                Status_we       <= 0;
           end
       20: begin
                fault_clr       <= 0;
                UART_WEN        <= 1;
                if (config_addr_FPGA == config_bytes - 1)
                  begin
                    config_en_rst   <= 1;
                    config_addr_FPGA<= 0;
                    state           <= 21;
                  end
                else
                  begin
                    config_addr_FPGA<= config_addr_FPGA + 1;
                    state           <= 18;
                  end
           end
                 
       21: begin
                Status_we       <= 1;
                config_en_rst   <= 1;
                state           <= 22;
           end

       22: begin
                state           <= 0;
                config_en_rst   <= 1;
           end          

    default: state        <= 0;
    endcase
    end


endmodule

        