///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: latch5_others2.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module latch5_others2    ( PO_Reset, MP_data_in, latch5_CS, MP_write,
                           SEL_0, SEL_1, SEL_2, SENSOR1_ON_OFF,
                           SENSOR2_ON_OFF, SENSOR3_ON_OFF, SENSOR4_ON_OFF, SENSOR5_ON_OFF, 
                           SENSOR6_ON_OFF, SPI_DIN_1, SPI_DIN_2, SPI_NCS_1,
                           SPI_NCS_2, SPI_SCLK_1, SPI_SCLK_2, ANT_DEPLOY_SDA);


input       PO_Reset;
input       MP_write;
input       latch5_CS;
input [15:0]MP_data_in;


output reg  SEL_0, SEL_1, SEL_2, SENSOR1_ON_OFF, SENSOR2_ON_OFF, SENSOR3_ON_OFF, SENSOR4_ON_OFF, 
            SENSOR5_ON_OFF, SENSOR6_ON_OFF, SPI_DIN_1, SPI_DIN_2, SPI_NCS_1, SPI_NCS_2, 
            SPI_SCLK_1, SPI_SCLK_2, ANT_DEPLOY_SDA;

wire [15:0] data_temp;

assign data_temp = (latch5_CS & ~MP_write)? MP_data_in : data_temp;

always @ (PO_Reset or latch5_CS)
  if (PO_Reset)
    begin
        SEL_0           <= 0;
        SEL_1           <= 0; 
        SEL_2           <= 0;
        SENSOR1_ON_OFF  <= 0;
        SENSOR2_ON_OFF  <= 0;
        SENSOR3_ON_OFF  <= 0;
        SENSOR4_ON_OFF  <= 0;
        SENSOR5_ON_OFF  <= 0;
        SENSOR6_ON_OFF  <= 0;
        SPI_DIN_1       <= 0;
        SPI_DIN_2       <= 0;
        SPI_NCS_1       <= 1;
        SPI_NCS_2       <= 1; 
        SPI_SCLK_1      <= 0;
        SPI_SCLK_2      <= 0;
        ANT_DEPLOY_SDA  <= 0;
    end
  else if (latch5_CS & ~MP_write)
      begin
        SEL_0           <= data_temp[0];
        SEL_1           <= data_temp[1];
        SEL_2           <= data_temp[2];
        SENSOR1_ON_OFF  <= data_temp[3];
        SENSOR2_ON_OFF  <= data_temp[4];
        SENSOR3_ON_OFF  <= data_temp[5];
        SENSOR4_ON_OFF  <= data_temp[6];
        SENSOR5_ON_OFF  <= data_temp[7];
        SENSOR6_ON_OFF  <= data_temp[8];
        SPI_DIN_1       <= data_temp[9];
        SPI_DIN_2       <= data_temp[10];
        SPI_NCS_1       <= data_temp[11];
        SPI_NCS_2       <= data_temp[12];
        SPI_SCLK_1      <= data_temp[13];
        SPI_SCLK_2      <= data_temp[14];
        ANT_DEPLOY_SDA  <= data_temp[15];
    end
  else 
    begin
        SEL_0           <= SEL_0;
        SEL_1           <= SEL_1;
        SEL_2           <= SEL_2;
        SENSOR1_ON_OFF  <= SENSOR1_ON_OFF;
        SENSOR2_ON_OFF  <= SENSOR2_ON_OFF;
        SENSOR3_ON_OFF  <= SENSOR3_ON_OFF;
        SENSOR4_ON_OFF  <= SENSOR4_ON_OFF;
        SENSOR5_ON_OFF  <= SENSOR5_ON_OFF;
        SENSOR6_ON_OFF  <= SENSOR6_ON_OFF;
        SPI_DIN_1       <= SPI_DIN_1 ;
        SPI_DIN_2       <= SPI_DIN_2;
        SPI_NCS_1       <= SPI_NCS_1;
        SPI_NCS_2       <= SPI_NCS_2;
        SPI_SCLK_1      <= SPI_SCLK_1;
        SPI_SCLK_2      <= SPI_SCLK_2;
        ANT_DEPLOY_SDA  <= ANT_DEPLOY_SDA;

    end
 endmodule                          
