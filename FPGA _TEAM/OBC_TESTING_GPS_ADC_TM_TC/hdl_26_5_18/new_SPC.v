module SPC (input       clock3125,
            input       master_reset,
            input       SPC_data_in,
            input       write_sig,
            input       read_sig,
            input [1:0] sel,
            input       zero_status,
            output reg [15:0] SPC_data_out,
            output reg  busy);
	
	parameter S0 = 2'b00;
	parameter S1 = 2'b01;
	parameter S2 = 2'b10;
	parameter S3 = 2'b11;
	
	reg [15:0] mem [0:3];// array of vectors
	reg [55:0] store;
	reg [1:0]  present_state;
	reg [5:0]  count_in;
    reg [2:0]  count_out;

	always@(*)
		SPC_data_out <= (read_sig)? mem[sel]:16'hz;
	
	always@(posedge clock3125 or posedge master_reset)
	//determining the states
		if(master_reset)
			present_state <= 2'b00;
		else
			case(present_state)
				S0: begin 
					present_state <= (write_sig==0)? S0: S1;
					store         <= 56'h0;
					end
				S1: begin
					present_state <= (count_in==56)? S2: S1;
					store         <= {store,SPC_data_in};
					end
				S2: begin
					present_state <= (count_out==4)? S0: S2;
					store         <= store;
					end
				default: present_state <= S0;
				endcase
	
	always @(negedge clock3125 or posedge master_reset) begin
	//counting the number of input bits coming in
		if(master_reset) 
			count_in <= 0;
		else if(present_state == S1) 
			count_in <= (count_in==56)?0:count_in + 1;
		else 
			count_in <= count_in;
		end
		
	always@(negedge read_sig or posedge master_reset) begin
	//counting the number of words going out
		if(master_reset)
			count_out <= 0;
        else
            count_out <=(count_out < 4)?(count_out+1):
                        (~zero_status) ? 3'd4        : 3'd0;
		end
	
	always@(present_state or master_reset) begin
		if(master_reset)
			busy <= 0;
		else 
			case(present_state)
				S0: busy <= 1'b1;
				S1: busy <= 1'b1;
				S2: busy <= (count_out == 4)?1'b1:1'b0;
				default: busy <= 1'b1;
				endcase
	    end

	always@(present_state or master_reset)
		if(master_reset)
			{mem[3],mem[2],mem[1],mem[0]} <= 64'b0;
		else
			case(present_state)
				S0: {mem[3],mem[2],mem[1],mem[0]} <= 64'b0;
				S1: {mem[3],mem[2],mem[1],mem[0]} <= 64'b0;
				S2: {mem[3],mem[2],mem[1],mem[0]} <= {store,8'd0};
				default: {mem[3],mem[2],mem[1],mem[0]} <= 64'b0;
				endcase

endmodule