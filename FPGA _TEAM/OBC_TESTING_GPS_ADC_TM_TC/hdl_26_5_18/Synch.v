///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: Synch.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE1500> <Package::208 PQFP>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module Synch( clk, clk_1us, reset, m2_address, m1_address, Buf_addr, MP_re, stconv_n, ADC_RD_n, ADC_CS_n, EOC_n, buf_write_n);

parameter       m1_addr_width = 4;

input           clk;                    //freq = 25MHz, 40ns
input           clk_1us;
input           EOC_n, reset;
input           MP_re;

output          stconv_n, ADC_RD_n, ADC_CS_n;
output          buf_write_n;
output  [5:0]   Buf_addr;

output  [m1_addr_width  :0] m2_address;    //32 channel mux
output  [m1_addr_width-1:0] m1_address;    //16 channel mux

reg     [m1_addr_width:0]   m2_address;
reg     [m1_addr_width-1:0] m1_address;

reg             stconv_n, ADC_RD_n, ADC_CS_n;
reg             buf_write_n;
reg     [5:0]   Buf_addr;

reg     [9:0]   count;
reg     [4:0]   state;
reg             timeout;
reg     [1:0]   timeout_cnt;
reg             cnt_start;
reg             cnt_reset;

always @ (posedge clk_1us) // or posedge cnt_reset)
  begin
    if (cnt_reset)
     begin
        timeout             <= 0;
        timeout_cnt         <= 0;
     end
    else if (cnt_start)
     begin
        timeout_cnt         <= timeout_cnt + 1;
        timeout             <= (timeout_cnt== 3)?1:0;
     end
    else
     begin
        timeout             <= 0;
        timeout_cnt         <= 0;
     end
  end        

always @ (posedge clk) // or posedge reset)
begin
    if (reset)
      begin
        m1_address      <= 0;
        m2_address      <= 0;
        count           <= 0;
        stconv_n        <= 1;
        ADC_RD_n        <= 1;
        ADC_CS_n        <= 1;
        Buf_addr        <= 0;
        cnt_start       <= 0;
        cnt_reset       <= 1;
        state           <= 0;
      end
    else
        case (state)
        0:  begin
                m1_address          <= 0;
                m2_address          <= 0;
                count               <= 0;
                stconv_n            <= 1;
                ADC_RD_n            <= 1;
                ADC_CS_n            <= 1;
                buf_write_n         <= 1;
                Buf_addr            <= 0;
                cnt_reset           <= 0;
                if (MP_re == 0)
                    state           <= 1;
                else
                    state           <= 0;
            end

         1: begin
                if (count == 1000)
                  begin
                    count           <= 0;
                    state           <= 2;
                  end
                else
                  begin
                    count           <= count + 1;
                    state           <= 1;
                  end
            end

         2: begin
                stconv_n            <= 0;   
                if (count == 2)                 //Generate stconv_n pulse of t1 = 120ns
                  begin                         //t1 = 45 ns according to datasheet                  
                    stconv_n        <= 1;
                    count           <= 0;
                    state           <= 3;
                  end
                else
                  begin
                    state           <= 2;
                    count           <= count + 1;
                  end
            end
        
         3: begin
                    cnt_reset       <= 0;
                    cnt_start       <= 1;
                if ((EOC_n == 0) | timeout)    //wait for EOC (conversion time is 1.47us)
                 begin
                    cnt_reset       <= 1;
                    cnt_start       <= 0;
                    state           <= 4;
                 end
                else
                    state           <= 3;
            end

         4: begin
                    cnt_reset       <= 0;
                    ADC_CS_n        <= 0;
                    ADC_RD_n        <= 0;
                    state           <= 5;
            end

         5: begin
                    buf_write_n     <= 0;
                if (count == 3 )
                  begin
                    count           <= 0;
                    buf_write_n     <= 1;
                    ADC_CS_n        <= 1;
                    ADC_RD_n        <= 1;
                    state           <= 6;
                  end
                else
                  begin
                    count           <= count +1;
                    state           <= 5;
                  end
             end

          6: begin
                state               <= 7;
             end

          7: begin
                if (count == 5)
                  begin
                    count           <= 0;
                    state           <= 8;
                  end
                else
                  begin
                    count           <= count + 1;
                    state           <= 7;
                  end
             end

          8: begin
                if (m2_address == 30)
                  begin
                    m2_address      <= 31;
		    Buf_addr        <= Buf_addr+1;
                    count           <= 0;
                    state           <= 9;
                  end
                else
                  begin
                    m2_address      <= m2_address + 1;
                    Buf_addr        <= Buf_addr+1;
                    state           <= 1;
                  end
             end

          9: begin
                if (count == 1000)
                  begin
                    count           <= 0;
                    state           <= 10;
                  end
                else
                  begin
                    count           <= count + 1;
                    state           <= 9;
                  end
            end

         10: begin
                stconv_n            <= 0;
                if (count == 3)                 //t1 = 60ns
                  begin
                    stconv_n        <= 1;
                    count           <= 0;
                    state           <= 11;
                  end
                else
                  begin
                    state           <= 10;
                    count           <= count + 1;
                  end
            end
        
         11: begin
                    cnt_reset       <= 0;
                    cnt_start       <= 1;
                if ((EOC_n == 0) | timeout)    //wait for EOC (conversion time is 1.47us)
                 begin
                    cnt_reset       <= 1;
                    cnt_start       <= 0;
                    state           <= 12;
                 end
                else
                    state           <= 11;
            end

         12: begin
                    ADC_CS_n        <= 0;
                    ADC_RD_n        <= 0;
                    state           <= 13;
             end

         13: begin
                    cnt_reset       <= 0;
                    buf_write_n     <= 0;
                if (count == 3 )
                  begin
                    count           <= 0;
                    buf_write_n     <= 1;
                    ADC_CS_n        <= 1;
                    ADC_RD_n        <= 1;
                    state           <= 14;
                  end
                else
                  begin
                    count           <= count + 1;
                    state           <= 13;
                  end
             end

          14: begin
                state               <= 15;
             end

         15: begin
                if (count == 5)
                  begin
                    count           <= 0;
                    state           <= 16;
                  end
                else
                  begin
                    count           <= count + 1;
                    state           <= 15;
                  end
             end

          16: begin
                if (m1_address == 7)
                    state           <= 0;
                else
                  begin
                    m1_address      <= m1_address + 1;
                    Buf_addr        <= Buf_addr+1;
                    state           <= 9;
                  end
             end


          default: state            <= 0;
          endcase
end

endmodule

