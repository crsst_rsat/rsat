// config_reg.v

module GPS_config_reg (config_data_in,config_type, config_addr_MP, reset,we, config_data_out, config_addr_FPGA);

input  [8:0]    config_addr_MP;
input  [15:0]   config_data_in;
input  [3:0]    config_addr_FPGA;
input           reset,we;
output [2:0]    config_type;
output [7:0]    config_data_out;
reg    [7:0]    config_data_out;
reg    [2:0]    config_type;
reg    [7:0]    mem [0:12];
integer         i;

wire   [3:0]    mem_addr;

assign mem_addr = config_addr_MP[3:0];
//assign  config_data_out = mem[config_addr_FPGA]; 
 always @ *
    if (reset)
     begin
        config_data_out      <= 8'b0;
        config_type          <= 3'b0;
        for (i=0; i<=11; i=i+1)
        mem[i]  <= 0;
     end
    else if (we)
      begin
       mem[mem_addr]   <=  config_data_in[15:8];
       mem[mem_addr+1] <=  config_data_in[7:0];
      end

    else begin
       config_data_out       <=  mem[config_addr_FPGA];
       case    ({mem[5], mem[6]})
       16'hB000  : config_type <= 1;
       16'hB001  : config_type <= 2;
       16'hC0CF  : config_type <= 3;
       16'hFF01  : config_type <= 4;
       16'hEACF  : config_type <= 5;
       default:    config_type <= 0;
       endcase
     end
endmodule 