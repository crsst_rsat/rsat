 module mem_telemetry(
		//input clock
		input clock_40M,
		//input data signals
		input [15:0] ip_data,
		//output data signals
		output [255:0] op_data,
		//input control signals
		input [3:0] address, // addresses one of the 16 16-bit words in the memory space
		input master_reset,
		input mem_RD,
		input mem_WR,		   //write signal given from arbitration logic to the memory.
		input SR_mem_WR,	   //write ready bit from the status register. This is similar to the last bit of the status register in the telecommand system.
		//output control signals
		output mem_full,
		output reg [4:0] counter //it counts the number of reg filled
		);
		
	parameter S0 = 4'b0001; //SR_mem_WR,mem_full,mem_RD,conv_enbl,conter4,conv_finish
	parameter S1 = 4'b0010;
	parameter S2 = 4'b0100;	
	parameter S3 = 4'b1000;	
 
	//declarations 
	reg [3:0]  state;          //holds the state
	reg [15:0] store [15:0];   //a 16-array of 16-bit vectors.
	reg [3:0]  count,count1;          //used to count till 10 clock cycles. Meant to be used when mem_WR is HIGH.
	reg [15:0] addr_full;      //If the nth bit is HIGH, it indicates that the nth address of the memory has been written into
   reg [1:0] state_temp;
	reg temp1,temp2;
	//architecture description
	
	assign op_data        ={store[15]  ,store[14]  ,store[13]  ,store[12] ,
									store[11]  ,store[10]  ,store[9]  ,store[8] ,
									store[7]  ,store[6]  ,store[5] ,store[4],
									store[3] ,store[2] ,store[1] ,store[0]};
									
	assign mem_full       = (master_reset)?1'b0:&(addr_full);

	always @ (negedge mem_RD,posedge master_reset,posedge temp2)
	begin
	if(master_reset) begin
		temp1<=1'b0;
		//state_temp<=2'b00;
		end
	else
  begin
	/*temp1<=1'b1;	
	if(temp2==1'b1)
	temp1<=1'b0;*/
	temp1<=temp2?1'b0:1'b1;
		end
	end
				
			
	
	
	always@(posedge clock_40M,posedge master_reset)
	begin
	
		if(master_reset) begin
			temp2<=1'b0;
			state_temp<=2'b00;
		end
	else
  begin
  case(state_temp)
  2'b00:
			if(temp1) begin
				temp2<=1'b1;
				state_temp<=2'b01;
			end
2'b01:begin temp2<=1'b0;
		state_temp<=2'b00;
		end
	endcase end end
	
	always@(negedge clock_40M, posedge master_reset)
	begin
		if(master_reset == 1'b1) begin
			{store[0],store[1],store[2],store[3],store[4],store[5],store[6],store[7],store[8],store[9],store[10],store[11],store[12],store[13],store[14],store[15]} <= 256'bz;
			state    <= S0;
			addr_full   <= 16'b0;
			count       <=4'b0;
			 count1<=4'b0;
			counter<=5'b0;
			end
		else begin
			case(state)
				S0: begin 
				      count1<=0;
						if(SR_mem_WR == 1'b1)   state <= S1;
						//else if(mem_RD == 1'b1) state <= S2;
				 		else							state <= S0;
						count <= 4'b0;
						counter<=5'b0;
						end
				S1:  //sr_mem_wr is continuously high and mem_wr is pulse whose width is 10 clock cycles and count = 4(which increments wrt to clock) it starts writing
					if(SR_mem_WR & mem_WR & ~mem_full ) begin
						store[address]     <= (count == 2)? ip_data: 16'bz; //is this way of addressing a vector in a vector-array right?
						addr_full[address] <= (count == 2)? 1'b1   : 1'b0 ;
						counter            <=  counter + 5'b00001;
						count 		       <= (count == 2)? 2      : count + 4'b0001;
						state		          <=  S1;
						end
					else if(SR_mem_WR & mem_WR & mem_full) begin
						count         	    <= 4'b0;
						state              <= S2;
						end
					/*else begin
						count              <= 4'b0;
						state              <= S1;
						end*/
				S2: 
						/*if(mem_RD)
							count1<=count1+1;
						else if(count1==150)
						begin
						count1<=0;*/
						if(temp2)
						state <= S3;
						//end
				S3:
					if(mem_full) begin
						addr_full  <= 16'b0;
						counter    <= 5'b0;
						state      <= S0;
						end
				endcase
			end
	end
endmodule