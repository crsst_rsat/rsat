///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: mux_input.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module mux_input( mp_read, mp_iosn, mp_addr, data1, data2, data3, data4, data_ADC, data_GPS, data_TC, data_TM,  mp_data_out);

input          mp_read, mp_iosn;
input  [15:0] data1, data2, data3, data4, data_ADC, data_GPS, data_TC, data_TM;
wire   [6:0]  sel;
input  [6:0]  mp_addr;
reg    [15:0] mp_data;
output [15:0] mp_data_out;

assign sel = mp_addr;
assign mp_data_out = (~mp_read & ~mp_iosn)? mp_data : 16'bz;
//<statements>
always @ (*)
case (sel)
    7'd0: mp_data = data_ADC;
    7'd1: mp_data = data_GPS;
    7'd8: mp_data = data_TM;
    7'd9: mp_data = data_TC;
    7'd17: mp_data = data1;
    7'd18: mp_data = data2;
    7'd19: mp_data = data3;
    7'd20: mp_data = data4;
default: mp_data = 16'bz;
endcase

endmodule

