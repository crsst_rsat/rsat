// clk_div_2us.v
module clk_div_2us (clk, reset, clk_2us);

input       clk;
input       reset;

output      clk_2us;
reg         clk_2us;

reg   [4:0] count;


always @ (posedge clk or posedge reset)
    if (reset)
      begin
        count       <= 0;
        clk_2us   <= 0;
      end
    else if (count  == 24)
      begin
        count       <= 0;
        clk_2us   <= !clk_2us;
      end
    else
      begin
        count     <= count + 1;
        clk_2us   <= clk_2us;
      end

endmodule
