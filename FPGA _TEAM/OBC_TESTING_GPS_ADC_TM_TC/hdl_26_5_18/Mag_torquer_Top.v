`timescale 1 ns/100 ps
// Version: 9.1 9.1.0.18


module Mag_torquer_Top(
       MP_P1,
       MP_P2,
       MP_N1,
       MP_N2,
       clk,
       clk_200us,
       reset,
       delayed_P1,
       delayed_P2,
       delayed_N1,
       delayed_N2
    );
input  MP_P1;
input  MP_P2;
input  MP_N1;
input  MP_N2;
input  clk;
input  clk_200us;
input  reset;
output delayed_P1;
output delayed_P2;
output delayed_N1;
output delayed_N2;

    wire delay_logic_0_delay_done1, delay_logic_0_delay_done2, 
        switch_detection_logic_0_delay1, 
        switch_detection_logic_0_delay2, 
        switch_detection_logic_0_store1, 
        switch_detection_logic_0_store2, GND_net, VCC_net;
    
    VCC VCC (.Y(VCC_net));
    GND GND (.Y(GND_net));
    switch_detection_logic switch_detection_logic_0 (.clk(clk), .reset(
        reset), .MP_P1(MP_P1), .MP_P2(MP_P2), .MP_N1(MP_N1), .MP_N2(
        MP_N2), .delay_done1(delay_logic_0_delay_done1), .delay_done2(
        delay_logic_0_delay_done2), .delay1(
        switch_detection_logic_0_delay1), .delay2(
        switch_detection_logic_0_delay2), .store1(
        switch_detection_logic_0_store1), .store2(
        switch_detection_logic_0_store2));
    delay_logic delay_logic_0 (.MP_P1(MP_P1), .MP_P2(MP_P2), .MP_N1(
        MP_N1), .MP_N2(MP_N2), .delay1(switch_detection_logic_0_delay1)
        , .delay2(switch_detection_logic_0_delay2), .clk(clk), 
        .clk_200us(clk_200us), .reset(reset), .store1(
        switch_detection_logic_0_store1), .store2(
        switch_detection_logic_0_store2), .MagTor_P1(delayed_P1), 
        .MagTor_P2(delayed_P2), .delayed_N1(delayed_N1), .delayed_N2(
        delayed_N2), .delay_done1(delay_logic_0_delay_done1), 
        .delay_done2(delay_logic_0_delay_done2));
    
endmodule
