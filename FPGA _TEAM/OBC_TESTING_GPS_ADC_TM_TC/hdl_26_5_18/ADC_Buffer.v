// Buffer.v

module ADC_Buffer (clk, reset, address, rd_address, read_en, write_en_n, data_in, data_out);

input           clk;
input           reset;
input           read_en;
input           write_en_n;
input   [5:0]   address;
input   [8:0]   rd_address;
input   [11:0]  data_in;

output  [15:0]  data_out;
reg     [15:0]  data_out;

reg     [11:0]  mem[47:0];

integer         i;

//assign data_out = (read_en)?{4'b0000, mem[rd_address]}:16'bz; 

always @ (posedge clk) //or posedge reset)
  begin
    if (reset)
       begin
        for (i=0; i<49; i=i+1)
          mem[i]     <= 0;
       end
    else if (write_en_n == 0)
        mem[address] <= data_in;
    else if (read_en)
        data_out     <= {4'b0000, mem[rd_address]};
    else
        data_out      <= 16'bz;
  end

endmodule
        