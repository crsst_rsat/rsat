module inpt_port3  (in_port3_cs, MP_data, MP_read,
                    LT_EP1_BRDY, LT_EP2_BRDY, LT_FP_READ, FP_WDOG
                    );

input       in_port3_cs;
input       MP_read; 
input       LT_EP1_BRDY, LT_EP2_BRDY, LT_FP_READ, FP_WDOG;

output[15:0]MP_data;
reg   [15:0]MP_data;

wire  [15:0] temp1;

assign temp1 = {LT_EP1_BRDY, LT_EP2_BRDY, LT_FP_READ, FP_WDOG, 12'd0};  
always @ (*)
  if (in_port3_cs)
    if (~MP_read)
      MP_data    = temp1;
    else 
      MP_data  = 16'bz;
  else
      MP_data  = 16'bz;
endmodule