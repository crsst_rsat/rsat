`timescale 1ns / 1ps

module receiver   (clock3125,clock40,master_reset,CS,data,randomizer_out,BCH_data,randomizer_data,
                   BCH_enbl,randomizer_enbl,AP,busy,MP_data, data_16,status,SPC_data,SPC_RD,SR_RD,SR_WR,MP_ADD,RD,WR);
input wire clock3125;
input wire clock40;
input wire master_reset;
input wire data;
input  [8:0]MP_ADD;
output      SR_RD,SR_WR,SPC_RD;
output      randomizer_out;
output      BCH_data;
output      randomizer_data;
output      BCH_enbl;
output      randomizer_enbl; 
output      AP;

output busy;
input [15:0] MP_data;
output [15:0] data_16;
output wire [15:0] SPC_data;
output wire [15:0] status;
input wire RD;//
input wire WR;//
input CS;
wire auth_enbl;
wire [0:6] parity;
wire filler_bit;
wire WR_bit;
wire SPC_enbl;
wire zero_status;
wire [0:1] sel;
wire [15:0] header_reg;
wire       header_rd;

assign SPC_enbl = BCH_enbl&(~(~busy & status[0]));

data_normalizer  uut1(clock3125,master_reset,data,BCH_data,BCH_enbl,filler_bit, header_reg);

BCH_enc          uut2(clock3125,master_reset,BCH_data,BCH_enbl,randomizer_data,randomizer_enbl,auth_enbl,parity);

derandomizer_FSM uut4(master_reset,clock3125,randomizer_data,randomizer_enbl,randomizer_out);

authentication   uut3(clock3125,master_reset,BCH_data,randomizer_data,auth_enbl,AP);

SPC              uut5SPC (.clock3125(clock3125),
	                      .master_reset(master_reset),
	                      .SPC_data_in(randomizer_out),
	                      .write_sig(SPC_enbl),
                          .read_sig(SPC_RD),
                          .sel(sel),
	                      .SPC_data_out(SPC_data),
	                      .busy(busy),
                          .zero_status(zero_status));

status_reg       uut6(clock40, SR_RD,SR_WR,AP,busy,parity,filler_bit,master_reset,status,zero_status, MP_data );

arb_log          uut7(SR_RD, sel, SR_WR, SPC_RD, clock40, RD, CS, master_reset, WR, MP_ADD, header_rd);

mux_op           uut8(SPC_data, status, SPC_RD, SR_RD, data_16, header_reg, header_rd);

//MP_test          uut9(clock,master_reset,data_16,ADD,RD,WR,uart_we,uart_reset,uart_data_in,1);
endmodule