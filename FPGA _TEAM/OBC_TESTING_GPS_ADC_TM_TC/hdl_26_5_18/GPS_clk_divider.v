// clk_divider.v

module GPS_clk_divider (clk, reset, clk_100us);

input       clk;
input       reset;

output      clk_100us;
reg         clk_100us;

reg   [4:0] count;


always @ (posedge clk)// or posedge reset)
    if (reset)
      begin
        count       <= 0;
        clk_100us   <= 0;
      end
    else if (count  == 24)
      begin
        count       <= 0;
        clk_100us   <= !clk_100us;
      end
    else
      begin
        count       <= count + 1;
        clk_100us   <= clk_100us;
      end

endmodule
