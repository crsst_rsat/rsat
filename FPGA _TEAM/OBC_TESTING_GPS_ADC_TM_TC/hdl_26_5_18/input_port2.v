module inpt_port2  (in_port2_cs, MP_data, MP_read, 
                    FP_RW1_Rx, FP_RW2_Rx, FP_RW3_Rx, FP_RW4_Rx,
                    FP_GPS1_Rx, FP_GPS2_Rx, FP_GPS1_PPS, FP_GPS2_PPS, 
                    SA1_deploy_ST, SA2_deploy_ST, RX_TC_CLK, FP_ADC_NEOC, 
                    RX_TC_DATA, TS_TC_CLK, TS_TC_DATA, TC_MUXSEL
                    );

input       in_port2_cs;
input       MP_read; 
input       FP_RW1_Rx, FP_RW2_Rx, FP_RW3_Rx, FP_RW4_Rx, FP_GPS1_Rx, FP_GPS2_Rx, FP_GPS1_PPS, FP_GPS2_PPS, 
            SA1_deploy_ST, SA2_deploy_ST, RX_TC_CLK, FP_ADC_NEOC, RX_TC_DATA, TS_TC_CLK, TS_TC_DATA, TC_MUXSEL;

output[15:0]MP_data;
reg   [15:0]MP_data;

wire  [15:0]temp1;

assign temp1 = {FP_RW1_Rx, FP_RW2_Rx, FP_RW3_Rx, FP_RW4_Rx, FP_GPS1_Rx, FP_GPS2_Rx, FP_GPS1_PPS, FP_GPS2_PPS, 
                SA1_deploy_ST, SA2_deploy_ST, RX_TC_CLK, FP_ADC_NEOC, RX_TC_DATA, TS_TC_CLK, TS_TC_DATA, TC_MUXSEL};  

always @ (*)
  if (in_port2_cs)
    if (~MP_read)
      MP_data    = temp1;
    else 
      MP_data  = 16'bz;
  else
      MP_data  = 16'bz;

endmodule