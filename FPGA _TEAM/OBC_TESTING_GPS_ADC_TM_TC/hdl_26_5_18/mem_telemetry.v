module dual_mem(
		//input clock
		input clock_10KHz,
		//input data signals
		input [15:0] ip_data,
		input master_reset,
		//output data signals
		output [255:0] op_data,
		input mem_RD,//when the shift register wants to read.
		input mem_WR,//processor is writing to dual_port memory
		//input control signals
		input [3:0] address, // addresses one of the 16 16-bit words in the memory space
		
		
				 //write signal given from arbitration logic to the memory.
		input SR_mem_WR,	 //write ready bit from the status register. This is similar to the last bit of the status register in the telecommand system.
		//output control signals
		output  full_flag  );//full flag shift register enable
		parameter S0=3'b001;
		parameter S1=3'b010;
		parameter S2=3'b100;
 
	//declarations
	reg [2:0]  state;          //holds the state
	reg [15:0] store [15:0];   //a 16-array of 16-bit vectors.
	reg [3:0]  count;          //used to count till 10 clock cycles. Meant to be used when mem_WR is HIGH.
	reg [15:0] addr_full;      //If the nth bit is HIGH, it indicates that the nth address of the memory has been written into

	//architecture description
	
	assign op_data        =(mem_RD)?{store[15],store[14],store[13],store[12],
									 store[11],store[10],store[9] ,store[8],
									 store[7] ,store[6], store[5] ,store[4],
									 store[3] ,store[2], store[1], store[0]}: 256'bz;
	assign full_flag      = &(addr_full);
	
	
	always@(negedge clock_10KHz, posedge master_reset)
	begin
		if(master_reset == 1'b1) begin
			//op_data <= 256'b0;
			{store[15],store[14],store[13],store[12],
									 store[11],store[10],store[9] ,store[8],
									 store[7] ,store[6], store[5] ,store[4],
									 store[3] ,store[2], store[1], store[0]}<=256'b0;
			state    <= S0    ;
			//full_flag <= 1'b0  ;
			end
		else begin
			case(state)
				S0: if(SR_mem_WR == 1'b1)   
				     state <= S1;
					else if(mem_RD == 1'b1)
     					state <= S2;
					else					
					  state <= S0;
				S1: 
					if(SR_mem_WR & ~full_flag)
					begin
						store[address]     <= (count == 4)?ip_data:16'bz; //is this way of addressing a vector in a vector-array right?
						addr_full[address] <= 1'b1;
						count 		       <= count + 1;
						state		          <= S1;
						end
					else if(SR_mem_WR & full_flag) begin
						count         	  <= 1'b0;
						state             <= S0;
						end
				S2:
					if(full_flag) begin
						addr_full	   <= 16'b0;
						//full_flag 	   <= 1'b0;
						state    	   <= S0;
						end
				endcase
			end
	end
endmodule