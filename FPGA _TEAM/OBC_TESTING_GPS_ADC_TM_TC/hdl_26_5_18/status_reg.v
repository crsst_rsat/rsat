`timescale 1us / 1ps
// status_reg.v
//SR<=AP & data_ready & Parity & b"00000" & fb ;

module status_reg (clock, rd, MP_we, AP, busy, parity, filler_bit, master_reset, status, zero_status, MP_data_in);//temp of normalizer connects to filller bit

input           clock;
input           rd;
input           MP_we;
input           master_reset;
input           AP;
input           busy;
input   [0:6 ]  parity;
input           filler_bit;
input   [15:0]  MP_data_in;

output          zero_status;
output reg[15:0]status;

assign zero_status = status[0];

always@(posedge clock or posedge master_reset) 
begin
    if (master_reset)
      begin
        status      <= 16'b0000_0000_0000_0000;
      end
    else if (MP_we == 1)
        status      <= MP_data_in;
   else
      begin
        status      <= {AP, ~busy, parity, filler_bit, 5'b00000, status[0]};
      end
end

endmodule