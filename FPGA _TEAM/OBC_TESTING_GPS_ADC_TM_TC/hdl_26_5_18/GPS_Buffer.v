// Buffer.v

module GPS_Buffer_data (clk, reset, address, rd_address, read_en, write_en, data_in, data_out);
parameter   data_width      =16;
parameter   address_width   =10;
parameter   mem_elements    =150;

input           read_en;
input           write_en;
input   [7:0]   address;
input	[8:0]	rd_address;
input   [15:0]  data_in;
input           clk;
input           reset;

output  [15:0]  data_out;
reg     [15:0]  data_out;

reg     [data_width-1:0]  mem[mem_elements:0];

integer         i;

//assign data_out = read_en? mem[rd_address]:data_out;

always @ (posedge clk)// or posedge reset)
  begin
    if (reset)
     begin
        data_out     <= 0;
        for (i=0; i<150; i=i+1)
        mem[i]       <= 0;
     end
    else if (write_en == 1)
        mem[address] <= data_in;
    else if (read_en)
        data_out     <= mem[rd_address];
    else
        data_out     <= 0;
  end
endmodule
        