module conv_enc(clock,master_reset,conv_data,conv_out1,conv_out2,conv_enable,encoder_regs,final_out,conv_finish);
input clock,master_reset,conv_enable,conv_data;
output reg conv_out1,conv_out2,final_out,conv_finish;
output reg [6:0] encoder_regs;
reg state;
reg [11:0] count;
reg temp,temp1;
always@(posedge master_reset or negedge clock)
	begin
	if(master_reset==1'b1)
		begin
		state<=1'b0;
		temp<=1'b0;
		//count<=12'b0;
		conv_out1<=1'b0;
		conv_out2<=1'b0;
		//final_out<=1'b0;
		conv_finish <= 1'b1;
		encoder_regs<=7'b0;
		end
	else    
		begin
		temp<=conv_data;
		case(state)
		1'b0:
			begin
			conv_out1<=1'b0;
			conv_out2<=1'b0;
			//final_out<=1'b0;
			encoder_regs<=7'b0;
			if(conv_enable)
				begin
				conv_finish<=1'b0;
				state<=1'b1;
				encoder_regs<=7'b0;
				end
			end
		1'b1:
			begin
			conv_out1<= encoder_regs[6]^encoder_regs[5]^encoder_regs[4]^encoder_regs[3]^encoder_regs[0];
			conv_out2<= encoder_regs[0]^encoder_regs[1]^encoder_regs[4]^encoder_regs[3]^encoder_regs[6];
			encoder_regs[6] <= temp;
			         
			encoder_regs[5] <= encoder_regs[6];
			         
			encoder_regs[4] <= encoder_regs[5];
			         
			encoder_regs[3] <= encoder_regs[4];
			         
			encoder_regs[2] <= encoder_regs[3];
			         
			encoder_regs[1] <= encoder_regs[2];
			         
			encoder_regs[0] <= encoder_regs[1];

			/*if(count==256*7)
			begin
				//state<=1'b0;
				conv_finish<=1'b1;
			end*/
		  if(count==2048)
		  begin
		      conv_finish<=1'b1;
            state<=1'b0;
				end
			end
		endcase
		end
	end
//initialise final_data_out
always@(clock,master_reset)
	begin
if(master_reset) begin
	final_out<=1'b0;
end

else begin
	if(clock)
		begin
		final_out<=conv_out1;
		temp1<=conv_out2;
		end
	else
		final_out<=temp1;
	end
end
always@(posedge clock)
	begin
	case(state)
	1'b1: count<=count+12'b0001;
	1'b0:count<=12'b0;
	endcase
	end
endmodule