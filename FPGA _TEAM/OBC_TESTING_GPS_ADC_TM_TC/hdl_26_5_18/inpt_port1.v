module inpt_port1  (in_port1_cs, MP_data, MP_read,
                    FP_MTR1_mon1, FP_MTR1_mon2, FP_MTR2_mon1, FP_MTR2_mon2, 
                    FP_MTR3_mon1, FP_MTR3_mon2, SPI_DOUT_1, SPI_DOUT_2, 
                    TS_SPI_DOUT_3, FP_PL_RXD, FP_PL_STATUS, FP_MUXSEL_IMU2,
                    DIO1_1, DIO1_2, TS_SPI_DIO1_3, VOBC_TM
                    );

input       in_port1_cs;
input       MP_read; 
input       FP_MTR1_mon1, FP_MTR1_mon2, FP_MTR2_mon1, FP_MTR2_mon2, FP_MTR3_mon1, FP_MTR3_mon2, SPI_DOUT_1, 
            SPI_DOUT_2, TS_SPI_DOUT_3, FP_PL_RXD, FP_PL_STATUS, FP_MUXSEL_IMU2, DIO1_1, DIO1_2, TS_SPI_DIO1_3, 
            VOBC_TM;

output[15:0]MP_data;
reg   [15:0]MP_data;

wire  [15:0]temp1;

assign temp1 =     {FP_MTR1_mon1, FP_MTR1_mon2, FP_MTR2_mon1, FP_MTR2_mon2, 
                    FP_MTR3_mon1, FP_MTR3_mon2, SPI_DOUT_1, SPI_DOUT_2, 
                    TS_SPI_DOUT_3, FP_PL_RXD, FP_PL_STATUS, FP_MUXSEL_IMU2,
                    DIO1_1, DIO1_2, TS_SPI_DIO1_3, VOBC_TM};  

always @ (*)
  if (in_port1_cs)
    if (~MP_read)
      MP_data    = temp1;
    else 
      MP_data  = 16'bz;
  else
      MP_data  = 16'bz;

endmodule