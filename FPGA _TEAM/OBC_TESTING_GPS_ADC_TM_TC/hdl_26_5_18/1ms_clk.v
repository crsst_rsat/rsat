///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: 1ms_clk.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module ms_clk( clk_2us, reset, clk_1ms );
input  clk_2us, reset;
output reg clk_1ms;

reg [8:0] count;

always @ (posedge clk_2us or posedge reset)
if (reset == 1)
    begin
    clk_1ms     <= 0;
    count       <= 0;
    end
else if (count == 9'd249)
    begin
    clk_1ms     <= ~clk_1ms;
    count       <= 0;
    end
else
    count       <= count + 1;
    

//<statements>

endmodule

