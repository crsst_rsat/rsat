`timescale 1ns / 1ps

module data_normalizer   (clock,master_reset,data,BCH_data,BCH_enbl,filler_bit, search_register);
input wire clock;
input wire master_reset;
input wire data;       //Input data
output reg BCH_data;                //Output data from this block
output reg BCH_enbl;                //Enables next block which is BCH Encoder 
output reg filler_bit;              //64th bit which is always zero

reg present_state;
output reg [0:15] search_register;         //16 bit register to check starting bits EB90
reg temp;
reg[0:7] count;

always@(*) 
    filler_bit <= temp;             //To display filler bit in status register.

always@(posedge clock or posedge master_reset) 
 begin
  if(master_reset==1'b1) 
  begin
   count          <=8'h00;
   search_register<=16'h0000;
   present_state  <=1'b0;
   BCH_data       <=1'b0;
   BCH_enbl       <=1'b0;
  end
  else
   begin
   case(present_state)
      1'b0: 
       begin
        if(search_register==16'hEB90)
          begin
            temp            <=1'b0;                         //filler bit is 0
            count           <=count+1;                     //count starts incrementing from zero when EB90 is detected
            search_register <=16'h0000;             //once EB90 is detected search register is made zero
            BCH_data        <=data;                 //once EB90 is detected then first input data is sent to output pin BCH_data
            BCH_enbl        <=1'b1;                 //Enables next block BCH decoder which is next block
            present_state   <=1'b1;                 //Goes to next state
          end
       else if(search_register == 16'h146F)         //If received bits are inverted 
          begin
            temp            <=1'b1;                 //filler bit will be one(as bits are inverted)
            count           <=count+1;              //count starts incrementing when start bits are detected  
            search_register <=16'h0000;    
            BCH_data        <=~data;                //First received bit after detecting 146F is inverted, so it is inverted and then sent to output pin BCH_data
            BCH_enbl        <=1'b1;                 //Enables next block BCH decoder which is next block
            present_state   <=1'b1;                 //Goes to next state
          end
       else                                         //If start bit is not detected
          begin          
            temp            <=1'b0; 
            count           <=8'h00;
            search_register <=search_register<<1;   //Shifting received data
			search_register[15]<=data;
            BCH_data        <=1'b0;                     //zero is sent to output BCH_data
            BCH_enbl        <=1'b0;                     //BCH decoder which is next block is disabled
            present_state   <=1'b0;                 //Stays in same state
          end
			end
      1'b1:
		  begin
          BCH_data          <=data^temp;
          //If EB90 is detected temp=0 and hence input data is sent to output.
          //If 146F is detected then input bits are inverted and temp=1, so input bits are inverted and sent to BCH_data
          count             <=count+1;
          if(count==63)     
             present_state  <=1'b0; //goes to state 0 
          end
    endcase
  end
end
endmodule