module derandomizer_FSM   ( mreset ,   
                            clk,           
                            rand_data ,  
                            rand_enbl ,    
                            rand_data_out 
                            );

input      mreset,clk,rand_data,rand_enbl;
output reg rand_data_out;

reg [0:1] current_state ;
reg [8:1] par;

reg [5:0] count;

always @(negedge clk or posedge mreset)
begin
	if(mreset)
	  begin
		count<= 6'b000000;
		par <= 8'b11111110;
		current_state<=2'b00;
		rand_data_out<=1'b0;
	  end
	else
	begin
		case (current_state)
		2'b00: begin
					count<= 6'b000000;
					rand_data_out<=1'b0;
                    par <= 8'hFE;
					if(rand_enbl == 1'b1)
					 begin
						current_state<=2'b01;
						rand_data_out<=~rand_data;
					 end
					else
						current_state<=2'b00;
			   end
			  
		2'b01: begin
					if(count<56) 
					begin
						par[1] <= par[1] ^ par[3] ^ par[5] ^ par[8];
						par[2] <= par[1];		
						par[3] <= par[2];
						par[4] <= par[3];
						par[5] <= par[4];
						par[6] <= par[5];
						par[7] <= par[6];
						par[8] <= par[7];
						rand_data_out <= rand_data^par[8];
						current_state <= 2'b01;
					end
					
					else
					begin
						rand_data_out  <= 1'b0;
						current_state     <= 2'b00;
					end
				 count <= count+1'b1;
		        end 	 
 endcase
end
end
endmodule
					
		

