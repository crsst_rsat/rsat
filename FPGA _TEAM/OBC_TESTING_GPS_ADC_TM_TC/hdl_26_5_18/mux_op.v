`timescale 1ns / 1ps

module mux_op(
	input  [15:0] SPC_data,
	input  [15:0] SR_data,
	input  SPC_RD,
	input  SR_RD,
	output [15:0] data_PR,
    input  [15:0] header_reg,
    input       header_rd
    );
	 
	assign data_PR = (SPC_RD)?SPC_data:(SR_RD?SR_data: header_rd? header_reg:(16'hzzzz));

endmodule
