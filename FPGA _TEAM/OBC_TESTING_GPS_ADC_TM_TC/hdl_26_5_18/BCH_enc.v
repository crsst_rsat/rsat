module BCH_enc(input clock, 
	input master_reset, 
	input BCH_data,   //input to BCH_decoder          
	input BCH_enbl,   //Enables this block
	output reg BCH_data_out, //input to derandomizer and output of this block
	output reg next_enbl,    //Enables derandomizer which is next block
	output reg auth_enbl,    //Enables 
    output reg [0:6] parity);

	reg [3:0] present_state;
	reg [0:6] encoder_regs;
	reg temp;
	reg enbl_lock;
	reg [5:0]count;
	
	always@(negedge clock or posedge master_reset)
	begin
		if(master_reset)
			temp <= 1'b0;
		else 
		begin
			if(present_state == 4'b0010 | present_state == 4'b0001)
				temp <= encoder_regs[6]^BCH_data;
			else
				temp <= 1'b0;
		end
	end
	
	always@(posedge clock or posedge master_reset)
	begin
		if(master_reset)
			begin
			BCH_data_out  <= 1'b0;
			present_state <= 4'b0001;
			encoder_regs  <= 7'b0000000;
			next_enbl     <= 1'b0;
			auth_enbl     <= 1'b0;
            parity        <= 7'b0000000;
            end
		else
			begin
			case(present_state)
				4'b0001:
					begin
					encoder_regs  <= 7'b0000000;
					if(enbl_lock)
						begin
						present_state <= 4'b0010;
						BCH_data_out <= BCH_data;
						next_enbl     <= 1'b1;
						
						end
					else 
						begin
						present_state <= 4'b0001;
						next_enbl     <= 1'b0;
						BCH_data_out <= 1'b0;
						end
					end
				4'b0010:
					begin
					if(count >= 0 & count < 56)
						begin
						BCH_data_out  <= BCH_data;
						next_enbl     <= 1'b1;
						present_state <= 4'b0010;
						end
					else
						begin
						BCH_data_out  <= BCH_data;
						next_enbl     <= 1'b1;
						present_state <= 4'b0100;
                        parity        <= encoder_regs;
                        
						end
					end
				4'b0100:
					begin
					if(count >=56 & count <63)
						begin                           
						    BCH_data_out  <= ~encoder_regs[6];
						    present_state <= 4'b0100;
						end
					else 
						begin
						    BCH_data_out  <= ~encoder_regs[6];
						    present_state <= 4'b1000;
						end
						next_enbl     <=1'b0;
						auth_enbl     <=1'b1;
					end
				4'b1000:	
					begin
						BCH_data_out  <= 1'b0;
						next_enbl     <= 1'b0;
						auth_enbl     <=1'b0;
						present_state <= 4'b0001;
					end
			endcase	
			
			encoder_regs[0] <= temp;
			encoder_regs[1] <= encoder_regs[0];
			encoder_regs[2] <= encoder_regs[1] ^ temp;
			encoder_regs[3] <= encoder_regs[2];
			encoder_regs[4] <= encoder_regs[3];
			encoder_regs[5] <= encoder_regs[4];
			encoder_regs[6] <= encoder_regs[5] ^ temp;
			end 
	end
	
	always@(BCH_enbl)
	enbl_lock <= BCH_enbl;
	
	always@(posedge master_reset or negedge clock)
	begin
	if(master_reset)
		count <= 0;
	else if(~clock)
		begin
		if(enbl_lock)
			count <= count + 1'b1;
		else
			count <= count;
		end
	else 
		count <= count;
	end
	
endmodule