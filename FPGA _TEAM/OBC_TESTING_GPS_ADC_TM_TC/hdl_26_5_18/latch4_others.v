///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: latch4_others.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module latch4_othersmodule ( PO_Reset, MP_data_in, latch4_CS, MP_write,
                           FP_RW1_TX, FP_RW2_TX, FP_RW3_TX, FP_RW4_TX ,
                           FP_PL_TXD, FP_PL_EN, MIN_RST,  MAJ_RST, 
                           GC_CLK, TS_SPI_SCLK, TS_SPI_DIN_3, TS_SPI_NCS, 
                           FP_NMI );


input       PO_Reset;
input       MP_write;
input       latch4_CS;
input [15:0]MP_data_in;

output      FP_RW1_TX;
output      FP_RW2_TX;
output      FP_RW3_TX;
output      FP_RW4_TX;
output      FP_PL_TXD;
output      FP_PL_EN;
output      MIN_RST;
output      MAJ_RST;
output      GC_CLK, TS_SPI_SCLK, TS_SPI_DIN_3, TS_SPI_NCS, 
            FP_NMI;


reg      FP_RW1_TX;
reg      FP_RW2_TX;
reg      FP_RW3_TX;
reg      FP_RW4_TX;
reg      FP_PL_TXD;
reg      FP_PL_EN;
reg      MIN_RST;
reg      MAJ_RST;
reg      GC_CLK, TS_SPI_SCLK, TS_SPI_DIN_3, TS_SPI_NCS, 
         FP_NMI;

wire [15:0] data_temp;

assign data_temp = (latch4_CS & ~MP_write)? MP_data_in : data_temp;

always @ (PO_Reset or latch4_CS)
  if (PO_Reset)
    begin
      FP_RW1_TX         <= 1;
      FP_RW2_TX         <= 1;
      FP_RW3_TX         <= 1;
      FP_RW4_TX         <= 1;
      FP_PL_TXD         <= 1;
      FP_PL_EN          <= 0;
      MIN_RST     <= 0;
      MAJ_RST       <= 0;
      GC_CLK            <= 0;
      TS_SPI_SCLK       <= 0;
      TS_SPI_DIN_3      <= 0;
      TS_SPI_NCS        <= 1;
      FP_NMI            <= 0;
    end
  else if (latch4_CS & ~MP_write)
    begin
      FP_RW1_TX         <= data_temp[0];
      FP_RW2_TX         <= data_temp[1];
      FP_RW3_TX         <= data_temp[2];
      FP_RW4_TX         <= data_temp[3];
      FP_PL_TXD         <= data_temp[4];
      FP_PL_EN          <= data_temp[5];
      MIN_RST     <= data_temp[6];
      MAJ_RST       <= data_temp[7];
      GC_CLK            <= data_temp[8];
      TS_SPI_SCLK       <= data_temp[9];
      TS_SPI_DIN_3      <= data_temp[10];
      TS_SPI_NCS        <= data_temp[11];
      FP_NMI            <= data_temp[12];
    end
  else 
    begin
      FP_RW1_TX         <= FP_RW1_TX;
      FP_RW2_TX         <= FP_RW2_TX;
      FP_RW3_TX         <= FP_RW3_TX;
      FP_RW4_TX         <= FP_RW4_TX;
      FP_PL_TXD         <= FP_PL_TXD;
      FP_PL_EN          <= FP_PL_EN;
      MIN_RST           <= MIN_RST;
      MAJ_RST           <= MAJ_RST;
      GC_CLK            <= GC_CLK;
      TS_SPI_SCLK       <= TS_SPI_SCLK;
      TS_SPI_DIN_3      <= TS_SPI_DIN_3;
      TS_SPI_NCS        <= TS_SPI_NCS;
      FP_NMI            <= FP_NMI;
    end
 endmodule                          
