///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: MP_data_buffer.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module MP_data_buffer ( MP_datain, MP_dataout);

input       [15:0] MP_datain;
output  reg [15:0] MP_dataout;

//<statements>
always @ (*)
 MP_dataout  <= MP_datain;
endmodule

