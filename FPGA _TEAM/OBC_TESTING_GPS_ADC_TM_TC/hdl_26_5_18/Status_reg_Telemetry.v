// status_reg.v
//SR<=AP & data_ready & Parity & b"00000" & fb ;
//status_reg uut2(shift_empty,SR_WR, mem_full,conv_enable, conv_finish,counter_mem, master_reset,status,WR_bit);
module status_reg_TM (shift_empty,WR, mem_full,conv_enable, conv_finish,counter_mem, master_reset,status,WR_bit);//temp of normalizer connects to filller bit
//SR_mem_WR,mem_full,mem_RD(shiftempty),conv_enbl,conter4,conv_finish(after 256 bytes)
//wr and wr_bit
input           shift_empty;
input           WR;
input           master_reset;
input           mem_full;
input           conv_enable;
input				 conv_finish; 
input   [4:0]   counter_mem;
input           WR_bit;

output reg[15:0]  status;

always @ (*)
begin
    if (master_reset)
        status      <= 0;
 
            //status      <= {shift_empty,mem_full,conv_enable, conv_finish,counter_mem,6'b0,WR_bit};//WR_bit not declared to be made input port
				
		else
		begin
         if (WR == 1)
			if(WR_bit)
				status      <=  {shift_empty,mem_full,conv_enable, conv_finish,counter_mem,6'b0,WR_bit};
			else
				status      <= 16'b0; 
		 else
		
        status      <=  {shift_empty,mem_full,conv_enable, conv_finish,counter_mem,6'b0,status[0]};

    end
end

endmodule