// clk_divider.v

module ADC_clk_divider (clk, reset, clk_1us);

input       clk;
input       reset;

output      clk_1us;
reg         clk_1us;

reg   [10:0] count;


always @ (posedge clk)// or posedge reset)
    if (reset)
      begin
        count       <= 0;
        clk_1us   <= 0;
      end
    else if (count  == 19)
      begin
        count       <= 0;
        clk_1us   <= !clk_1us;
      end
    else
      begin
        count       <= count + 1;
        clk_1us   <= clk_1us;
      end

endmodule
