module Min_cycle_TC (clk_1ms, POR, min_TC, min_TC_rst,clk_16ms);

input       clk_1ms;
input       POR;
input       min_TC_rst;

output      min_TC;
output      clk_16ms;

reg         min_TC;
reg         clk_16ms;
reg [3:0]   count;

always @ (posedge clk_1ms or posedge POR or posedge min_TC_rst)
    if (POR)
     begin
        clk_16ms    <= 1;
        count       <= 0;
     end
    else if (min_TC_rst)
     begin
        count       <= 0;
        clk_16ms    <= 1;
     end
    else if (count == 4'd7)
     begin
        clk_16ms    <= ~ clk_16ms;
        count       <= 0;
     end
    else
        count       <= count+1;
        
always @ (posedge clk_16ms or posedge POR or posedge min_TC_rst)
  begin
    if (POR)
     begin
        min_TC      <= 0;
     end
    else if (min_TC_rst)
     begin
        min_TC      <= 0;
     end
    else 
        min_TC      <= 1;
  end

endmodule