module inpt_port4  (in_port4_cs, MP_data, MP_read, 
                    FP_ADC_D, MUX1_OUT, MUX2_OUT, MUX3_OUT, MUX4_OUT, MIN_TC, MAJ_TC
                    );

input       in_port4_cs;
input       MP_read; 
input [9:0] FP_ADC_D; 
input       MUX1_OUT, MUX2_OUT, MUX3_OUT, MUX4_OUT, MIN_TC, MAJ_TC;

output[15:0]MP_data;
reg   [15:0]MP_data;

wire  [15:0]temp1, temp2;

assign temp1 = {MIN_TC, MAJ_TC, FP_ADC_D, MUX1_OUT, MUX2_OUT, MUX3_OUT, MUX4_OUT};  

always @ (*)
  if (in_port4_cs)
    if (~MP_read)
      MP_data    = temp1;
    else 
      MP_data  = 16'bz;
  else
      MP_data  = 16'b0;
    
endmodule