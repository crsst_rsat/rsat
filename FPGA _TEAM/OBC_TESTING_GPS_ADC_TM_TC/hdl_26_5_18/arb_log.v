`timescale 1ns / 1ps

module arb_log(
                output       SR_RD,
                output [1:0] SPC_sel,
                output reg   SR_WR,
                output       SPC_RD,
                input        clock,
                input        RD,
                input        CS,
                input        master_reset,
                input        WR,
                input [8:0]  ADD,
                output       header_rd
                );

	reg [2:0]   count;
    wire [2:0]  count_next;
    reg         temp;
	wire         header_rd;

	//assign SR_RD = (ADD==8'hFF & CS==1 & ~WR)?RD:1'b0;
	assign SPC_RD  = (CS==1)?((ADD>= 9'h000) & (ADD<= 9'h003) & (RD == 0) & (WR != 0))?1:0:0; 
	assign header_rd  = (CS==1)?((ADD== 9'h0F0) & (RD == 0) & (WR != 0))?1:0:0; 

	assign SPC_sel = SPC_RD? ADD[1:0]: 2'b00;

	assign SR_RD   = (CS==1)?((ADD == 9'h0FF) & (RD == 0) & (WR != 0))?1:0:0; 
    assign count_next = count + 1;
//we should check cs for writing
always@(posedge clock or posedge master_reset) 
  begin
	if(master_reset) 
     begin
       SR_WR        <= 0;
       count        <= 0;
      // state        <= 0;
     end
    else if (CS == 1)
     begin
        SR_WR       <= ((WR == 0) & (ADD == 8'hff))&(count >= 1) & (count <= 2)?1:0;
        count       <= (WR == 0)? count_next : 0;
     end
    else
     begin
       SR_WR        <= 0;
       count        <= 0;
     end 
end
endmodule