// Mag_torquer_TB.v

// Mag_torquer_TB.v

//////////////////////////////////////////////////////////////////////
// Created by Actel SmartDesign Sat Jan 06 15:52:08 2018
// Testbench Template
// This is a basic testbench that instantiates your design with basic 
// clock and reset pins connected.  If your design has special
// clock/reset or testbench driver requirements then you should 
// copy this file and modify it. 
//////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps

module Mag_torquer_TB;

parameter SYSCLK_PERIOD = 40; // 10MHz

reg         SYSCLK;
reg         NSYSRESET;
reg         MP_P1;
reg         MP_P2;
reg         MP_N1;
reg         MP_N2;

reg         clk_200us;

wire        delayed_P1;
wire        delayed_P2;
wire        delayed_N1;
wire        delayed_N2;

initial
begin
    SYSCLK      = 1'b0;
    NSYSRESET   = 1'b1;
    clk_200us   = 0;
end

//////////////////////////////////////////////////////////////////////
// Reset Pulse
//////////////////////////////////////////////////////////////////////
initial
begin
    #(SYSCLK_PERIOD * 10 )
        NSYSRESET = 1'b0;
end


//////////////////////////////////////////////////////////////////////
// 10MHz Clock Driver
//////////////////////////////////////////////////////////////////////
always @(SYSCLK)
    #(SYSCLK_PERIOD / 2.0) SYSCLK <= !SYSCLK;


always 
    #200    clk_200us   <= !clk_200us;
//////////////////////////////////////////////////////////////////////
// Instantiate Unit Under Test:  Mag_torquer_Top
//////////////////////////////////////////////////////////////////////
Mag_torquer_Top Mag_torquer_Top_0 (
    // Inputs
    .MP_P1(MP_P1),
    .MP_P2(MP_P2),
    .MP_N1(MP_N1),
    .MP_N2(MP_N2),
    .clk(SYSCLK),
    .clk_200us(SYSCLK),
    .reset(NSYSRESET),

    // Outputs
    .delayed_P1(delayed_P1),
    .delayed_P2(delayed_P2 ),
    .delayed_N1(delayed_N1 ),
    .delayed_N2(delayed_N2 )

    // Inouts

);

initial
  begin
    MP_P1   = 1;
    MP_N1   = 0;
    MP_P2   = 1;
    MP_N2   = 0;

#1000
    MP_P1   = 0;
    MP_N1   = 0;
    MP_P2   = 1;
    MP_N2   = 0;

#6000
    MP_P1   = 0;
    MP_N1   = 1;
    MP_P2   = 1;
    MP_N2   = 1;

#6000
    MP_P1   = 1;
    MP_N1   = 0;
    MP_P2   = 0;
    MP_N2   = 1;

#6000
    MP_P1   = 0;
    MP_N1   = 1;
    MP_P2   = 1;
    MP_N2   = 0;
   
#6000;
end
endmodule

