// Status_register.v

module GPS_Status_register (MP_bit5, MP_re, MP_we, MP_data_in, reset, clk, framing_err, overflow_err, config_bytes,
                        parity_err, datardy, data_NA,fault, PPS_overflow, fault_en, data_NA_en, config_NMEA_msg,
                        status_dataout, MP_status_re, MP_GPS_config_re, FPGA_we, config_en_rst, fault_clr, data_NA_clr);

input               MP_re;
input               MP_we;
input               reset;
input               clk;
input   [15:0]      MP_data_in;

reg     [15:0]      status;

input               framing_err;
input               overflow_err;
input               parity_err;
input               datardy;
input               data_NA, fault;
input               data_NA_en, fault_en;
input               data_NA_clr, fault_clr;
input               FPGA_we;
input               config_en_rst;
input               PPS_overflow;

output              MP_status_re, MP_bit5;
output              MP_GPS_config_re;
output  [3:0]       config_bytes;
output  [2:0]       config_NMEA_msg;

output  [15:0]      status_dataout;

wire     [15:0]     status_dataout;

wire                fault_status, data_NA_status;

assign status_dataout       = (MP_re == 1)? {status[15:9], PPS_overflow, fault_status, status[6:5],data_NA_status, datardy, overflow_err, parity_err, framing_err}: 16'b0000_0000_0000_0000; 
assign MP_status_re         = status[5];
assign MP_GPS_config_re     = status[6];
assign config_bytes         = status[12:9];
assign config_NMEA_msg      = status[15:13];
assign data_NA_status       = data_NA_en? data_NA : data_NA_clr? 0:status[4];
assign fault_status         = fault_en? fault : fault_clr?0:status[7];

assign MP_bit5 = MP_data_in[3];

always @ (posedge clk)// or posedge reset)
begin
    if (reset == 1)
      begin
        status   <= 16'b0000_0000_0000_0000;
      end
    else if ((MP_we == 1))
        status   <= MP_data_in;
    else if ((FPGA_we == 1)) // && (MP_re != 1))
      begin
        status[6]<= config_en_rst? 0:status[6];    
      end
    else
      begin
        status   <= {status[15:9], PPS_overflow, fault_status, status[6:5],data_NA_status, datardy, overflow_err, parity_err, framing_err};
      end
//assign  MP_re  = status[0];
end
endmodule