`timescale 1ns / 1ps

module authentication (clock, master_reset, bch_data, rand_data, rand_enable, ap);

 input wire clock,master_reset,bch_data,rand_data,rand_enable;
 output reg ap;

 reg [0:2]  count;
 reg        update_ap;
 reg        store_bch_data,valid;
 
 always @ (negedge clock) // input
 begin
	store_bch_data  <= bch_data;
 end
	
 always @ (negedge clock or posedge master_reset) // counting
 begin
	if (master_reset == 1)
      begin
		count       <=3'b000;
		update_ap   <=1'b0;
	  end
	else if (rand_enable == 1)
		count       <=count+1;
	else if(count == 7)
	  begin
	    count       <=3'b000;
		update_ap   <= 1'b1;
	  end
   else
	    count       <=count;
 end
 
 always @ (negedge clock or posedge master_reset)
 
 begin
	if (master_reset==1 )
	 begin
       
		valid   <=0;
		ap      <=0;
	 end
	else if (count>0 & count <=7)
		valid   <= valid & (store_bch_data ~^ rand_data);
	else if (update_ap==1)
		ap      <=valid;
	else
		valid   <=valid;

 end
	endmodule

