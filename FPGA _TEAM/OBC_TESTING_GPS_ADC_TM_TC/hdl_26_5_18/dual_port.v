module dual_port(
   input clock_40M ,
	input clock_10KHz ,
	input master_reset,
	input CS, //=1'b1;
	input [8:0] ADD,
	input RD,
	input WR,
	
	input [15:0] MP_datain,

	output [15:0] MP_dataout,
	
	output [15:0] ip_data,
	output [15:0] status,
	output main_out,
	output uncoded_out,
	output conv_out1,
	output conv_out2,
	output [6:0] encoder_regs,
	output conv_out
	//output mem_full
	); //to select status or dual mem
	
//	reg clock_10KHz = 0;
	//reg clock_20KHz = 0;
	//wire [15:0] ip_data;
	//wire [15:0] input_data ;
   //reg [0:511] data=	512'hDBC1550CAAAABBBBDBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAABBBBDBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAAAAAA;
	wire [255:0] op_data;
	wire [3:0] address;
	//reg master_reset=0;
	wire mem_RD;
	wire mem_WR;
	wire SR_mem_WR;
	wire mem_full;
	//integer i = 0;
	//wire conv_out;
	//main_out,uncoded_out;
	//wire conv_out1;
	//wire conv_out2; 
	//wire [0:6] encoder_regs;
	wire [4:0] counter;
	wire conv_finish;
	//wire CS=1'b1;
	//wire [15:0] status;
	//wire [7:0] ADD);
	wire mem_RD_new;
	
	mem_telemetry mem1(.clock_40M(clock_40M), 
						.ip_data(ip_data), 
						.op_data(op_data),
						.address(address),
						.master_reset(master_reset),
						.mem_RD(mem_RD),
						.mem_WR(mem_WR),
						.SR_mem_WR(status[0]),
						.mem_full(mem_full),
						.counter(counter)
						);
	shift_reg uut(clock_10KHz,op_data,mem_full,master_reset,conv_out,uncoded_out,mem_RD,mem_WR,conv_finish);
	conv_enc uut1(clock_10KHz,master_reset,conv_out,conv_out1,conv_out2,mem_RD,encoder_regs,main_out,conv_finish);
	status_reg_TM uut2(mem_RD,SR_WR, mem_full,mem_RD, conv_finish,counter, master_reset,status,WR_bit);
	
	 arb_log_TM uut3(status,address,SR_WR,mem_WR,clock_40M,RD,CS,master_reset,WR,ADD,_MP_datain,_MP_dataout,ip_data,WR_bit);
	 //MP_test uut4(clock_10KHz,master_reset, input_data,ADD,RD,WR, uart_WE, uart_reset, uart_data_in,1'b1,mem_full,final_in_data16);
	/*always #5 clock_10KHz = ~clock_10KHz;
	always #2.5 clock_20KHz= ~clock_20KHz;
   initial begin
	#1 master_reset<=0;
	#1 master_reset<=1;
	#1 master_reset<=0;
	end
	*/
	//reg [15:0] final_in_data16 =16'hDBC5;
	/*always@(posedge clock_10KHz) 
	begin		
		SR_mem_WR = 1;
		
		for(i = 0; i < 32; i=i+1) begin
			mem_WR = 1;
			ip_data=data[(i*16)+:16];
			#100
			mem_WR = 0;
			#10;
			//ip_data = ip_data + 16'b1;
			address = address + 8'b1;
			$display(i);
			end
			SR_mem_WR=0;*/
			
		

		
	
		//end
	
endmodule