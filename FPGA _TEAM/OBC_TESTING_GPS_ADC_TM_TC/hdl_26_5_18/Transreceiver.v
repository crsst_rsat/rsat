///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: Transreceiver.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module Transreceiver( MPdata_in, MP_data_OPlatch, MP_data_IPlatch, IOS_n, MP_OE_n, MP_write_n );

input [15:0]MP_data_IPlatch;
input       IOS_n, MP_OE_n, MP_write_n;

output reg [15:0] MP_data_OPlatch;

inout [15:0]MPdata_in;

assign  MPdata_in = (IOS_n == 0)?(MP_OE_n == 0)? MP_data_IPlatch : 16'bz:16'bz;

always @ (*)
if (IOS_n == 0)
    MP_data_OPlatch = (MP_write_n == 0)? MPdata_in : 16'bz;
else
    MP_data_OPlatch = 16'bz;
//<statements>

endmodule

