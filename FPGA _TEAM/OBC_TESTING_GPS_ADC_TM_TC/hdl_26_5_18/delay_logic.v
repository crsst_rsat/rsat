// delay_logic.v

module delay_logic (MP_P1, MP_P2, MP_N1, MP_N2, delay1, delay2, clk, clk_200us, reset, store1, store2, 
                    MagTor_P1, MagTor_P2, delayed_N1, delayed_N2, delay_done1, delay_done2);


input       clk, clk_200us;
input       reset;
input       MP_P1;
input       MP_P2;
input       MP_N1;
input       MP_N2;
input       delay1, delay2;
input       store1, store2;

output      MagTor_P1;
output      MagTor_P2;
output      delayed_N1;
output      delayed_N2;
output      delay_done1, delay_done2  ;

reg         delayed_P1;
reg         delayed_P2;
reg         delayed_N1;
reg         delayed_N2;
reg         delay_done1, delay_done2  ;

reg  [4:0]  count1, count2;
reg         cnt1_reset, cnt2_reset, cnt1_en, cnt2_en;

wire        stored_P1, stored_N1, stored_P2, stored_N2;

assign MagTor_P1    = ~delayed_P1;
assign MagTor_P2    = ~delayed_P2;
assign stored_P1 = store1? MP_P1 : stored_P1;
assign stored_N1 = store1? MP_N1 : stored_N1;
assign stored_P2 = store2? MP_P2 : stored_P2;
assign stored_N2 = store2? MP_N2 : stored_N2;

always @ (posedge clk_200us or posedge cnt1_reset)
 begin
    if (cnt1_reset)
        count1      <= 0;
    else if (cnt1_en)
        count1      <= count1 + 1;
    else
        count1      <= count1;
 end

always @ (posedge clk_200us or posedge cnt2_reset)
 begin
    if (cnt2_reset)
        count2      <= 0;
    else if (cnt2_en)
        count2      <= count2 + 1;
    else
        count2      <= count2;
 end


always @ (posedge clk or posedge reset)
  begin
    if (reset)
      begin
        cnt1_reset  <= 1;
        cnt1_en     <= 0;
        delayed_P1  <= 0;
        delayed_N1  <= 0;
        delay_done1 <= 0;
      end
    else if (delay1 & (count1 == 31))
      begin
        cnt1_reset  <= 1;
        cnt1_en     <= 0;
        delayed_P1  <= stored_P1;
        delayed_N1  <= stored_N1;
        delay_done1 <= 1;
      end
    else if (delay1 & (count1 < 31))
      begin
        cnt1_reset  <= 0;
        cnt1_en     <= 1;
        delayed_P1  <= 0; //delayed_P1;
        delayed_N1  <= 0; //delayed_N1;
        delay_done1 <= 0;
      end
    else 
      begin
        cnt1_reset  <= 0;
        cnt1_en     <= 0;
        delayed_P1  <= (MP_P1==MP_N1)?0:MP_P1;
        delayed_N1  <= (MP_P1==MP_N1)?0:MP_N1;
        delay_done1 <= 0;
      end
  end  

always @ (posedge clk or posedge reset)
  begin
    if (reset)
      begin
        cnt2_reset  <= 1;
        cnt2_en     <= 0;
        delayed_P2  <= 0;
        delayed_N2  <= 0;
        delay_done2 <= 0;
      end
    else if (delay2 & (count2 == 31))
      begin
        cnt2_reset  <= 1;
        cnt2_en     <= 0;
        delayed_P2  <= stored_P2;
        delayed_N2  <= stored_N2;
        delay_done2  <= 1;
      end
    else if (delay2 & (count2 < 31))
      begin
        cnt2_reset  <= 0;
        cnt2_en     <= 1;
        delayed_P2  <= 0; //delayed_P2;
        delayed_N2  <= 0; //delayed_N2;
        delay_done2  <= 0;
      end
    else 
      begin
        cnt2_reset  <= 0;
        cnt2_en     <= 0;
        delayed_P2  <= (MP_P2==MP_N2)?0:MP_P2;
        delayed_N2  <= (MP_P2==MP_N2)?0:MP_N2;
        delay_done2  <= 0;
      end
  end  
endmodule