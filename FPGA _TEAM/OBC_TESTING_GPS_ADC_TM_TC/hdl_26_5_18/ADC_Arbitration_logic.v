// Arbitration_logic.v

module ADC_arbitration_logic (clk, reset,MP_re, MP_we, ADC_CS_n, MP_addr, status_we, buffer_re);

input        MP_re;
input        MP_we;
input        ADC_CS_n;
input        reset;
input        clk;
   
input [8:0]  MP_addr;

output       status_we;
output       buffer_re;

reg          status_we;
wire         buffer_re;
reg    [4:0] count;
reg    [3:0] state;

wire   [4:0] count_next;

assign count_next   = count + 1;
assign buffer_re    = ADC_CS_n? ((MP_addr != 8'hff)&(MP_re == 0)&(MP_we != 0))?1:0:0;  

always @ (posedge clk )//or posedge reset)
  begin
    if (reset == 1)
      begin
       status_we        <= 0;
       count            <= 0;
       state            <= 0;
      end
    else if (ADC_CS_n == 1) begin
        status_we    <= ((MP_we == 0) & (MP_addr == 8'hff) & (count > 0) & (count < 3))?1:0;
        count        <= (MP_we == 0)? count_next : 0;
end
        //case (state)
//
        //0: begin
                //count        <= 0;
                //status_we    <= 0;
             //if ((MP_we == 1) & (MP_addr == 8'hff))
               //begin
                //state        <= 1;
               //end
           //else
                //state        <= 0;
           //end
//
        //1: begin
                //status_we    <= (MP_we == 1) & (MP_addr == 8'hff) & (count > 2) & (count < 5);
             //if (count == 6)
               //begin
                //count        <= 0;
                //state        <= 2;
               //end
             //else   
               //begin
                //count        <= count_next;
                //state        <= 1;
               //end
           //end
//
        //2: begin
             //if (MP_we == 0)
                //state        <= 0;
             //else
                //state        <= 2;
           //end
//
        //default:   state             <= 0;
        //endcase

        else
        begin
           status_we    <= 1'b0;
           count        <= (MP_we == 0)? count_next : 0;
        end
end
endmodule

