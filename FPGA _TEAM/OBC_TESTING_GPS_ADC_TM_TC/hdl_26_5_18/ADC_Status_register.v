// Status_register.v

module ADC_Status_register (MP_re, w_en, data_in, reset);

output reg      MP_re;
input           w_en;
input           reset;

input   [15:0]  data_in;

reg    [15:0]  status;

always @ (*)
begin
    if (reset == 1)
      begin
        MP_re       <= 0;
        status      <= 0;
      end
     else if (w_en == 1)
     begin
        status      <= data_in;
        MP_re       <= status[0];
     end
     else
        status      <= status;
end

//assign  MP_re  = status[0];

endmodule