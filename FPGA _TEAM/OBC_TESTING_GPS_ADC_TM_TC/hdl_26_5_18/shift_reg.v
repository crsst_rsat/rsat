module shift_reg(clock,input_data,enable,master_reset,conv_output,uncoded_out,empty_flag,mem_WR,conv_finish);
input clock,conv_finish;
input [255:0] input_data;
input enable;
input master_reset;
input mem_WR;
output reg conv_output;
output reg uncoded_out;
output reg empty_flag;

reg [255:0] store;
reg [7:0] count;
reg [2:0] counter;
reg state;

always@(posedge master_reset or posedge clock) begin
	if(master_reset==1'b1) begin
		count<=8'b0;
		counter<=3'b0;
      conv_output<=1'b0;
      uncoded_out<=1'b0;
      empty_flag<=1'b0;
      state<=1'b0;
      store<=256'b0;
		end
	else begin
		case(state)
			1'b0: begin
					count<=8'b0;
					uncoded_out<=1'b0;
					conv_output<=1'b0;
					if(enable & ~mem_WR & conv_finish) begin //empty flag = memm_RD , enabl = mem_full
						empty_flag<=1'b1;
						store<=input_data;
						state<=1'b1;
						end
					else
						state<=1'b0;
						end
			1'b1: begin
					if(count<254) begin
						empty_flag<=1'b0;
						store<=store>>1;
						conv_output<=store[0];
						uncoded_out<=store[0];
						count<=count+1;
						end
					else if(count==8'd254) 
				   begin
					  if(counter!==7)
						empty_flag<=1'b1;
	
						store<=store>>1;
						conv_output<=store[0];
						uncoded_out<=store[0];
						count<=count+1;
						end
					else if(count==8'd255) begin
						empty_flag<=1'b0;
						store<=input_data;
						conv_output<=store[0];
						uncoded_out<=store[0];
						count<=0;
						counter<=counter+3'b1;
						if(counter==7) 
		 			 begin
							state<=1'b0;
						counter<=0;
						end
						end
	
					end
 
			endcase
		end
	end          
endmodule