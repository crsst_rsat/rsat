///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: latch5_others2.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::ProASIC3E> <Die::A3PE3000> <Package::484 FBGA>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

//`timescale <time_units> / <precision>

module latch6_others3    ( PO_Reset, MP_data_in, latch6_CS, MP_write,
                           FP_AMUX16_A0, FP_AMUX16_A1, FP_AMUX16_A2, FP_AMUX16_A3,
                            FP_AMUX_A0, FP_AMUX_A1, FP_AMUX_A2, FP_AMUX_A3, 
                            FP_AMUX_A4 ); /*synthesis syn_noprune=1*/


input       PO_Reset;
input       MP_write;
input       latch6_CS;
input [15:0]MP_data_in;


output reg  FP_AMUX16_A0, FP_AMUX16_A1, FP_AMUX16_A2, FP_AMUX16_A3,
            FP_AMUX_A0, FP_AMUX_A1, FP_AMUX_A2, FP_AMUX_A3, FP_AMUX_A4;

wire [15:0] data_temp;

assign data_temp = (latch6_CS & ~MP_write)? MP_data_in : data_temp;

always @ (PO_Reset or latch6_CS)
  if (PO_Reset)
    begin
        FP_AMUX16_A0        <= 0;
        FP_AMUX16_A1        <= 0;
        FP_AMUX16_A2        <= 0;
        FP_AMUX16_A3        <= 0;
        FP_AMUX_A0          <= 0;
        FP_AMUX_A1          <= 0;
        FP_AMUX_A2          <= 0;
        FP_AMUX_A3          <= 0;
        FP_AMUX_A4          <= 0;
    end
  else if (latch6_CS & ~MP_write)
     begin
        FP_AMUX16_A0        <= data_temp[0];
        FP_AMUX16_A1        <= data_temp[1];
        FP_AMUX16_A2        <= data_temp[2];
        FP_AMUX16_A3        <= data_temp[3];
        FP_AMUX_A0          <= data_temp[4];
        FP_AMUX_A1          <= data_temp[5];
        FP_AMUX_A2          <= data_temp[6];
        FP_AMUX_A3          <= data_temp[7];
        FP_AMUX_A4          <= data_temp[8];
    end
  else 
    begin
        FP_AMUX16_A0        <= FP_AMUX16_A0;
        FP_AMUX16_A1        <= FP_AMUX16_A1;
        FP_AMUX16_A2        <= FP_AMUX16_A2;
        FP_AMUX16_A3        <= FP_AMUX16_A3;
        FP_AMUX_A0          <= FP_AMUX_A0;
        FP_AMUX_A1          <= FP_AMUX_A1;
        FP_AMUX_A2          <= FP_AMUX_A2;
        FP_AMUX_A3          <= FP_AMUX_A3;
        FP_AMUX_A4          <= FP_AMUX_A4;
    end
 endmodule                          
