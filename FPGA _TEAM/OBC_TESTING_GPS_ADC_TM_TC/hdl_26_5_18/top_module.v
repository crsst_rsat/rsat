//test bench
`timescale 1ns/1ps
module top_module;
   reg clock_40M=1'b0;
  reg clock_10KHz = 1'b0; 
	reg clock_20KHz=1'b0;
	reg master_reset=1'b0;
	reg CS;
	wire [7:0] ADD;
	wire RD;
	wire WR;
   wire        uart_reset;
   wire [7:0]  uart_data_in;
	
	wire [15:0] data_line;
	
	wire [15:0] ip_data;
	wire [15:0] status;
	wire main_out;
	wire uncoded_out;
	wire conv_out1;
	wire conv_out2;
	wire [6:0] encoder_regs;
	wire conv_out;
	//wire mem_full;
	wire [15:0] final_in_data16;
	assign final_in_data16=16'hA3DB;	
	
//reg [0:511] data=	512'hDBC1550CAAAABBBBDBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAABBBBDBC1550CAAAAAAAADBC1550CAAAAAAAADBC1550CAAAAAAAA;
	
MP_test MP(clock_40M,master_reset, data_line,ADD,RD,WR,uart_WE, uart_reset, uart_data_in,1'b1);
dual_port trans(clock_40M,clock_10KHz ,clock_20KHz,master_reset,1'b1,ADD,RD,WR,data_line,ip_data,status,main_out,uncoded_out,conv_out1,conv_out2, encoder_regs,conv_out);
					
			 
always #12.5 clock_40M =~clock_40M;				 
always #50000 clock_10KHz = ~clock_10KHz;
always #25000 clock_20KHz= ~clock_20KHz;
   initial begin
	CS<=1;
	#1 master_reset<=0;
	#1 master_reset<=1;
	#1 master_reset<=0;
	end
endmodule	