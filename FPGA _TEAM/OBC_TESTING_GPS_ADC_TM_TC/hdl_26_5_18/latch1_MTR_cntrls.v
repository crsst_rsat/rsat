module latch1_MTR_cntrls ( clk, PO_Reset, MP_data_in, latch1_CS, MP_write,
                           FP_CTRL_MTR1_P1, FP_CTRL_MTR1_P2, FP_CTRL_MTR1_N1, FP_CTRL_MTR1_N2,
                           FP_CTRL_MTR2_P1, FP_CTRL_MTR2_P2, FP_CTRL_MTR2_N1, FP_CTRL_MTR2_N2,
                           FP_CTRL_MTR3_P1, FP_CTRL_MTR3_P2, FP_CTRL_MTR3_N1, FP_CTRL_MTR3_N2);


input       PO_Reset, clk;
input       MP_write;
input       latch1_CS;
input [15:0]MP_data_in;


output      FP_CTRL_MTR1_P1;
output      FP_CTRL_MTR1_P2;
output      FP_CTRL_MTR1_N1;
output      FP_CTRL_MTR1_N2;
output      FP_CTRL_MTR2_P1;
output      FP_CTRL_MTR2_P2;
output      FP_CTRL_MTR2_N1;
output      FP_CTRL_MTR2_N2;
output      FP_CTRL_MTR3_P1; 
output      FP_CTRL_MTR3_P2;
output      FP_CTRL_MTR3_N1;
output      FP_CTRL_MTR3_N2;

reg      FP_CTRL_MTR1_P1;
reg      FP_CTRL_MTR1_P2;
reg      FP_CTRL_MTR1_N1;
reg      FP_CTRL_MTR1_N2;
reg      FP_CTRL_MTR2_P1;
reg      FP_CTRL_MTR2_P2;
reg      FP_CTRL_MTR2_N1;
reg      FP_CTRL_MTR2_N2;
reg      FP_CTRL_MTR3_P1; 
reg      FP_CTRL_MTR3_P2;
reg      FP_CTRL_MTR3_N1;
reg      FP_CTRL_MTR3_N2;

reg      MagT_we;
reg    [4:0] count;
wire   [4:0] count_next;
wire [15:0] data_temp;

assign data_temp = (latch1_CS & MagT_we)? MP_data_in : data_temp;
assign count_next   = count + 1;

always @ (posedge clk or posedge PO_Reset)
  begin
  if (PO_Reset == 1)
    begin
       MagT_we          <= 0;
       count            <= 0;
    end
  else if (latch1_CS == 1) 
    begin
       MagT_we    <= ((MP_write == 0) & (count > 0) & (count < 3))?1:0;
       count      <= (MP_write == 0)? count_next : 0;
    end
  else
    begin
       MagT_we    <= 1'b0;
       count      <= (MP_write == 0)? count_next : 0;
    end
end
always @ (PO_Reset or latch1_CS or MagT_we or data_temp)
  if (PO_Reset)
    begin
      FP_CTRL_MTR1_P1        = 0;
      FP_CTRL_MTR1_P2        = 0;
      FP_CTRL_MTR1_N1        = 0;
      FP_CTRL_MTR1_N2        = 0;
      FP_CTRL_MTR2_P1        = 0;
      FP_CTRL_MTR2_P2        = 0;
      FP_CTRL_MTR2_N1        = 0;
      FP_CTRL_MTR2_N2        = 0;
      FP_CTRL_MTR3_P1        = 0;
      FP_CTRL_MTR3_P2        = 0;
      FP_CTRL_MTR3_N1        = 0;
      FP_CTRL_MTR3_N2        = 0;
    end
  else if (latch1_CS & MagT_we)
      begin
      FP_CTRL_MTR1_P1        = data_temp[0];
      FP_CTRL_MTR1_P2        = data_temp[1];
      FP_CTRL_MTR1_N1        = data_temp[2];
      FP_CTRL_MTR1_N2        = data_temp[3];
      FP_CTRL_MTR2_P1        = data_temp[4];
      FP_CTRL_MTR2_P2        = data_temp[5];
      FP_CTRL_MTR2_N1        = data_temp[6];
      FP_CTRL_MTR2_N2        = data_temp[7];
      FP_CTRL_MTR3_P1        = data_temp[8];
      FP_CTRL_MTR3_P2        = data_temp[9];
      FP_CTRL_MTR3_N1        = data_temp[10];
      FP_CTRL_MTR3_N2        = data_temp[11];
    end
  else 
    begin
      FP_CTRL_MTR1_P1        = FP_CTRL_MTR1_P1;
      FP_CTRL_MTR1_P2        = FP_CTRL_MTR1_P2;
      FP_CTRL_MTR1_N1        = FP_CTRL_MTR1_N1;
      FP_CTRL_MTR1_N2        = FP_CTRL_MTR1_N2;
      FP_CTRL_MTR2_P1        = FP_CTRL_MTR2_P1;
      FP_CTRL_MTR2_P2        = FP_CTRL_MTR2_P2;
      FP_CTRL_MTR2_N1        = FP_CTRL_MTR2_N1;
      FP_CTRL_MTR2_N2        = FP_CTRL_MTR2_N2;
      FP_CTRL_MTR3_P1        = FP_CTRL_MTR3_P1;
      FP_CTRL_MTR3_P2        = FP_CTRL_MTR3_P2;
      FP_CTRL_MTR3_N1        = FP_CTRL_MTR3_N1;
      FP_CTRL_MTR3_N2        = FP_CTRL_MTR3_N2;
    end
 endmodule                          