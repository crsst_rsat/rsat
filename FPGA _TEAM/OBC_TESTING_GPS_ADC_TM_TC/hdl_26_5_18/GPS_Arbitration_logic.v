// arbitration_logic.v
// Arbitration_logic.v

module GPS_arbitration_logic (clk, reset,MP_re, MP_we, GPS_CS_n, MP_addr, status_we, 
                          buffer_re, status_MP_re, config_MP_we);

input        MP_re;
input        MP_we;
input        GPS_CS_n;
input        clk, reset;

input [8:0]  MP_addr;

output       status_we;
output       buffer_re;
output       status_MP_re;
output       config_MP_we;

reg          status_we;
wire         buffer_re;
wire         status_MP_re;
reg          config_MP_we;
reg    [3:0] count,state;
wire   [3:0] count_next;
  
assign count_next = count + 1;

assign  buffer_re    = GPS_CS_n? ((MP_addr < 8'hf0)&(MP_re == 0)&(MP_we != 0))?1:0:0; //((MP_addr != 8'hff) &  
assign  status_MP_re = GPS_CS_n? ((MP_re == 0) & (MP_addr == 8'hff)&(MP_we != 0))?1:0:0 ; 
assign count_next = count + 1;

always @ (posedge clk)// or posedge reset)
  begin
    if (reset == 1)
      begin
       status_we        <= 0;
       config_MP_we     <= 0;
       count            <= 0;
       state            <= 0;
      end
    else if (GPS_CS_n == 1)
        //case (state)

      /*  0: begin
                count        <= 0;
                status_we    <= 0;
                config_MP_we <= 0;
             if ((MP_we == 0) & (MP_addr == 8'hff))
                state        <= 1;
             else if ((MP_we == 0) & (MP_addr >= 8'hf0) & (MP_addr <= 8'hf8))
                state        <= 3;
             else 
                state        <= 0;
           end*/

       begin
                status_we    <= ((MP_we == 0) & (MP_addr == 8'hff))&(count >= 1) & (count <= 2)?1:0; 
                config_MP_we <= ((MP_we == 0) & (MP_addr >= 8'hf0) & (MP_addr <= 8'hfd)) & (count >= 1) & (count <= 2)?1:0;
                count        <= (MP_we == 0)? count_next : 0;
             //if (count == 6)
               //begin
                //count        <= 0;
                //state        <= 2;
               //end
             //else   
               //begin
                //count        <= count_next;
                //state        <= 1;
               //end
           //end
//
        //2: begin
             //if (MP_we == 0)
                //state        <= 0;
             //else
                //state        <= 2;
           //end
//
//
        //3: begin
               // config_MP_we <= ((MP_we == 0)& (MP_addr >= 8'hf0) & (MP_addr <= 8'hf8))?1:0; //& (count >= 2) & (count <= 4)? 1 : 0;
             //if (count == 6)
               //begin
                //count        <= 0;
                //state        <= 4;
               //end
             //else   
               //begin
                //count        <= count_next;
                //state        <= 3;
               //end
           //end
//
        //4: begin
             //if (MP_we == 0)
                //state        <= 0;
             //else
                //state        <= 4;
           //end
//
        //default:   state             <= 0;
        //endcase
//
        //else
        //begin
           //status_we    <= 1'b0;
        end
    else
      begin
       status_we        <= 0;
       config_MP_we     <= 0;
        count <= 0;
      end        
end
endmodule

