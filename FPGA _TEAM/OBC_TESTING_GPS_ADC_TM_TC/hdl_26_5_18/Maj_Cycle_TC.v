module  Maj_Cycle_TC (clk_16ms, POR, maj_TC, maj_TC_rst, FP_NMI);

input   clk_16ms;
input   POR;
input   maj_TC_rst;

output  maj_TC;
output  FP_NMI;

reg     maj_TC;
reg     FP_NMI;
reg     clk_128ms;

reg [3:0]   count;
reg [8:0]   count_int;

always @ (posedge clk_16ms or posedge POR or posedge maj_TC_rst)
    if (POR)
     begin
        clk_128ms    <= 1;
        count        <= 0;
     end
    else if (maj_TC_rst)
     begin
        count        <= 0;
        clk_128ms    <= 1;
     end
    else if (count == 4'd3)
     begin
        clk_128ms    <= ~ clk_128ms;
        count        <= 0;
     end
    else
        count       <= count+1;

always @ (posedge clk_16ms or posedge POR or posedge maj_TC_rst)
    if (POR)
     begin
        count_int   <= 0;
        FP_NMI      <= 0;
     end
    else if (maj_TC_rst)
     begin
        count_int   <= 0;
        FP_NMI      <= 0;
     end
    else if (count_int == 9'd400)
     begin
        count_int   <= 0;
        FP_NMI      <= 1;
     end
    else    
     begin
        FP_NMI      <= 0;
        count_int   <= count_int + 1;
     end

always @ (posedge clk_128ms or posedge POR or posedge maj_TC_rst)
  begin
    if (POR)
     begin
        maj_TC      <= 0;
     end
    else if (maj_TC_rst)
     begin
        maj_TC      <= 0;
     end
    else 
     begin
        maj_TC      <= 1;
     end
  end
endmodule