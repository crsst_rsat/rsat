//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Feb 26 11:22:46 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// OP_LATCHES
module OP_LATCHES(
    // Inputs
    MP_data_in,
    MP_write,
    PO_Reset,
    clk,
    clk_200us,
    latch1_CS,
    latch2_CS,
    latch3_CS,
    latch4_CS,
    latch5_CS,
    latch6_CS,
    // Outputs
    ANT_DEPLOY,
    ANT_DEPLOY_SDA,
    FP_MTR1_N1,
    FP_MTR1_N2,
    FP_MTR1_P1,
    FP_MTR1_P2,
    FP_MTR2_N1,
    FP_MTR2_N2,
    FP_MTR2_P1,
    FP_MTR2_P2,
    FP_MTR3_N1,
    FP_MTR3_N2,
    FP_MTR3_P1,
    FP_MTR3_P2,
    FP_NMI,
    FP_PL_EN,
    FP_PL_TXD,
    FP_RW1_TX,
    FP_RW2_TX,
    FP_RW3_TX,
    FP_RW4_TX,
    GC_CLK,
    GPS1_ON_OFF,
    GPS1_Reset,
    GPS2_ON_OFF,
    GPS2_Reset,
    Heater1_ON_OFF,
    Heater2_ON_OFF,
    Heater3_ON_OFF,
    Heater4_ON_OFF,
    Heater5_ON_OFF,
    Heater6_ON_OFF,
    IMU1_ON_OFF,
    IMU1_Reset,
    IMU2_ON_OFF,
    IMU2_Reset,
    MAJ_RST,
    MIN_RST,
    MTR_ON_OFF,
    Payload_ON_OFF,
    RF_Tx_ON_OFF,
    RW1_ON_OFF,
    RW2_ON_OFF,
    RW3_ON_OFF,
    RW4_ON_OFF,
    SA1_DEPLOY,
    SA2_DEPLOY,
    SEL_0,
    SEL_1,
    SEL_2,
    SENSOR1_ON_OFF,
    SENSOR2_ON_OFF,
    SENSOR3_ON_OFF,
    SENSOR4_ON_OFF,
    SENSOR5_ON_OFF,
    SENSOR6_ON_OFF,
    SPARE1_ON_OFF,
    SPARE2_ON_OFF,
    SPI_DIN_1,
    SPI_DIN_2,
    SPI_NCS_1,
    SPI_NCS_2,
    SPI_SCLK_1,
    SPI_SCLK_2,
    TM_DS_EN,
    TS_SPI_DIN_3,
    TS_SPI_NCS,
    TS_SPI_SCLK,
    X_Tx_ON_OFF
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input  [15:0] MP_data_in;
input         MP_write;
input         PO_Reset;
input         clk;
input         clk_200us;
input         latch1_CS;
input         latch2_CS;
input         latch3_CS;
input         latch4_CS;
input         latch5_CS;
input         latch6_CS;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        ANT_DEPLOY;
output        ANT_DEPLOY_SDA;
output        FP_MTR1_N1;
output        FP_MTR1_N2;
output        FP_MTR1_P1;
output        FP_MTR1_P2;
output        FP_MTR2_N1;
output        FP_MTR2_N2;
output        FP_MTR2_P1;
output        FP_MTR2_P2;
output        FP_MTR3_N1;
output        FP_MTR3_N2;
output        FP_MTR3_P1;
output        FP_MTR3_P2;
output        FP_NMI;
output        FP_PL_EN;
output        FP_PL_TXD;
output        FP_RW1_TX;
output        FP_RW2_TX;
output        FP_RW3_TX;
output        FP_RW4_TX;
output        GC_CLK;
output        GPS1_ON_OFF;
output        GPS1_Reset;
output        GPS2_ON_OFF;
output        GPS2_Reset;
output        Heater1_ON_OFF;
output        Heater2_ON_OFF;
output        Heater3_ON_OFF;
output        Heater4_ON_OFF;
output        Heater5_ON_OFF;
output        Heater6_ON_OFF;
output        IMU1_ON_OFF;
output        IMU1_Reset;
output        IMU2_ON_OFF;
output        IMU2_Reset;
output        MAJ_RST;
output        MIN_RST;
output        MTR_ON_OFF;
output        Payload_ON_OFF;
output        RF_Tx_ON_OFF;
output        RW1_ON_OFF;
output        RW2_ON_OFF;
output        RW3_ON_OFF;
output        RW4_ON_OFF;
output        SA1_DEPLOY;
output        SA2_DEPLOY;
output        SEL_0;
output        SEL_1;
output        SEL_2;
output        SENSOR1_ON_OFF;
output        SENSOR2_ON_OFF;
output        SENSOR3_ON_OFF;
output        SENSOR4_ON_OFF;
output        SENSOR5_ON_OFF;
output        SENSOR6_ON_OFF;
output        SPARE1_ON_OFF;
output        SPARE2_ON_OFF;
output        SPI_DIN_1;
output        SPI_DIN_2;
output        SPI_NCS_1;
output        SPI_NCS_2;
output        SPI_SCLK_1;
output        SPI_SCLK_2;
output        TM_DS_EN;
output        TS_SPI_DIN_3;
output        TS_SPI_NCS;
output        TS_SPI_SCLK;
output        X_Tx_ON_OFF;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          ANT_DEPLOY_net_0;
wire          ANT_DEPLOY_SDA_net_0;
wire          clk;
wire          clk_200us;
wire          FP_MTR1_N1_net_0;
wire          FP_MTR1_N2_net_0;
wire          FP_MTR1_P1_net_0;
wire          FP_MTR1_P2_net_0;
wire          FP_MTR2_N1_net_0;
wire          FP_MTR2_N2_net_0;
wire          FP_MTR2_P1_net_0;
wire          FP_MTR2_P2_net_0;
wire          FP_MTR3_N1_net_0;
wire          FP_MTR3_N2_net_0;
wire          FP_MTR3_P1_net_0;
wire          FP_MTR3_P2_net_0;
wire          FP_NMI_net_0;
wire          FP_PL_EN_net_0;
wire          FP_PL_TXD_net_0;
wire          FP_RW1_TX_net_0;
wire          FP_RW2_TX_net_0;
wire          FP_RW3_TX_net_0;
wire          FP_RW4_TX_net_0;
wire          GC_CLK_net_0;
wire          GPS1_ON_OFF_net_0;
wire          GPS1_Reset_net_0;
wire          GPS2_ON_OFF_net_0;
wire          GPS2_Reset_net_0;
wire          Heater1_ON_OFF_net_0;
wire          Heater2_ON_OFF_net_0;
wire          Heater3_ON_OFF_net_0;
wire          Heater4_ON_OFF_net_0;
wire          Heater5_ON_OFF_net_0;
wire          Heater6_ON_OFF_net_0;
wire          IMU1_ON_OFF_net_0;
wire          IMU1_Reset_net_0;
wire          IMU2_ON_OFF_net_0;
wire          IMU2_Reset_net_0;
wire          latch1_CS;
wire          latch2_CS;
wire          latch3_CS;
wire          latch4_CS;
wire          latch5_CS;
wire          latch6_CS;
wire          MAJ_RST_net_0;
wire          MIN_RST_net_0;
wire   [15:0] MP_data_in;
wire          MP_write;
wire          MTR_ON_OFF_net_0;
wire          Payload_ON_OFF_net_0;
wire          PO_Reset;
wire          RF_Tx_ON_OFF_net_0;
wire          RW1_ON_OFF_net_0;
wire          RW2_ON_OFF_net_0;
wire          RW3_ON_OFF_net_0;
wire          RW4_ON_OFF_net_0;
wire          SA1_DEPLOY_net_0;
wire          SA2_DEPLOY_net_0;
wire          SEL_0_net_0;
wire          SEL_1_net_0;
wire          SEL_2_net_0;
wire          SENSOR1_ON_OFF_net_0;
wire          SENSOR2_ON_OFF_net_0;
wire          SENSOR3_ON_OFF_net_0;
wire          SENSOR4_ON_OFF_net_0;
wire          SENSOR5_ON_OFF_net_0;
wire          SENSOR6_ON_OFF_net_0;
wire          SPARE1_ON_OFF_net_0;
wire          SPARE2_ON_OFF_net_0;
wire          SPI_DIN_1_net_0;
wire          SPI_DIN_2_net_0;
wire          SPI_NCS_1_net_0;
wire          SPI_NCS_2_net_0;
wire          SPI_SCLK_1_net_0;
wire          SPI_SCLK_2_net_0;
wire          TM_DS_EN_net_0;
wire          TS_SPI_DIN_3_net_0;
wire          TS_SPI_NCS_net_0;
wire          TS_SPI_SCLK_net_0;
wire          X_Tx_ON_OFF_net_0;
wire          ANT_DEPLOY_SDA_net_1;
wire          SPI_NCS_1_net_1;
wire          SENSOR1_ON_OFF_net_1;
wire          SPI_DIN_1_net_1;
wire          SEL_1_net_1;
wire          SPI_SCLK_1_net_1;
wire          SENSOR4_ON_OFF_net_1;
wire          SENSOR3_ON_OFF_net_1;
wire          SPI_NCS_2_net_1;
wire          SEL_0_net_1;
wire          SPI_DIN_2_net_1;
wire          SPI_SCLK_2_net_1;
wire          SENSOR6_ON_OFF_net_1;
wire          SENSOR5_ON_OFF_net_1;
wire          SEL_2_net_1;
wire          SENSOR2_ON_OFF_net_1;
wire          FP_PL_EN_net_1;
wire          FP_PL_TXD_net_1;
wire          FP_RW2_TX_net_1;
wire          FP_RW3_TX_net_1;
wire          GC_CLK_net_1;
wire          FP_RW1_TX_net_1;
wire          FP_NMI_net_1;
wire          FP_RW4_TX_net_1;
wire          TS_SPI_DIN_3_net_1;
wire          TS_SPI_NCS_net_1;
wire          TS_SPI_SCLK_net_1;
wire          Heater1_ON_OFF_net_1;
wire          Heater6_ON_OFF_net_1;
wire          SA1_DEPLOY_net_1;
wire          RW1_ON_OFF_net_1;
wire          RW2_ON_OFF_net_1;
wire          Heater2_ON_OFF_net_1;
wire          ANT_DEPLOY_net_1;
wire          Heater3_ON_OFF_net_1;
wire          Heater5_ON_OFF_net_1;
wire          SA2_DEPLOY_net_1;
wire          RW3_ON_OFF_net_1;
wire          Heater4_ON_OFF_net_1;
wire          RW4_ON_OFF_net_1;
wire          TM_DS_EN_net_1;
wire          GPS1_ON_OFF_net_1;
wire          GPS2_Reset_net_1;
wire          SPARE2_ON_OFF_net_1;
wire          IMU1_Reset_net_1;
wire          MTR_ON_OFF_net_1;
wire          IMU1_ON_OFF_net_1;
wire          SPARE1_ON_OFF_net_1;
wire          Payload_ON_OFF_net_1;
wire          GPS2_ON_OFF_net_1;
wire          IMU2_Reset_net_1;
wire          RF_Tx_ON_OFF_net_1;
wire          GPS1_Reset_net_1;
wire          X_Tx_ON_OFF_net_1;
wire          IMU2_ON_OFF_net_1;
wire          FP_MTR1_N1_net_1;
wire          FP_MTR1_P2_net_1;
wire          FP_MTR2_P1_net_1;
wire          FP_MTR1_P1_net_1;
wire          FP_MTR3_N1_net_1;
wire          FP_MTR2_N1_net_1;
wire          FP_MTR2_N2_net_1;
wire          FP_MTR3_N2_net_1;
wire          FP_MTR3_P2_net_1;
wire          FP_MTR2_P2_net_1;
wire          FP_MTR1_N2_net_1;
wire          FP_MTR3_P1_net_1;
wire          MAJ_RST_net_1;
wire          MIN_RST_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign ANT_DEPLOY_SDA_net_1 = ANT_DEPLOY_SDA_net_0;
assign ANT_DEPLOY_SDA       = ANT_DEPLOY_SDA_net_1;
assign SPI_NCS_1_net_1      = SPI_NCS_1_net_0;
assign SPI_NCS_1            = SPI_NCS_1_net_1;
assign SENSOR1_ON_OFF_net_1 = SENSOR1_ON_OFF_net_0;
assign SENSOR1_ON_OFF       = SENSOR1_ON_OFF_net_1;
assign SPI_DIN_1_net_1      = SPI_DIN_1_net_0;
assign SPI_DIN_1            = SPI_DIN_1_net_1;
assign SEL_1_net_1          = SEL_1_net_0;
assign SEL_1                = SEL_1_net_1;
assign SPI_SCLK_1_net_1     = SPI_SCLK_1_net_0;
assign SPI_SCLK_1           = SPI_SCLK_1_net_1;
assign SENSOR4_ON_OFF_net_1 = SENSOR4_ON_OFF_net_0;
assign SENSOR4_ON_OFF       = SENSOR4_ON_OFF_net_1;
assign SENSOR3_ON_OFF_net_1 = SENSOR3_ON_OFF_net_0;
assign SENSOR3_ON_OFF       = SENSOR3_ON_OFF_net_1;
assign SPI_NCS_2_net_1      = SPI_NCS_2_net_0;
assign SPI_NCS_2            = SPI_NCS_2_net_1;
assign SEL_0_net_1          = SEL_0_net_0;
assign SEL_0                = SEL_0_net_1;
assign SPI_DIN_2_net_1      = SPI_DIN_2_net_0;
assign SPI_DIN_2            = SPI_DIN_2_net_1;
assign SPI_SCLK_2_net_1     = SPI_SCLK_2_net_0;
assign SPI_SCLK_2           = SPI_SCLK_2_net_1;
assign SENSOR6_ON_OFF_net_1 = SENSOR6_ON_OFF_net_0;
assign SENSOR6_ON_OFF       = SENSOR6_ON_OFF_net_1;
assign SENSOR5_ON_OFF_net_1 = SENSOR5_ON_OFF_net_0;
assign SENSOR5_ON_OFF       = SENSOR5_ON_OFF_net_1;
assign SEL_2_net_1          = SEL_2_net_0;
assign SEL_2                = SEL_2_net_1;
assign SENSOR2_ON_OFF_net_1 = SENSOR2_ON_OFF_net_0;
assign SENSOR2_ON_OFF       = SENSOR2_ON_OFF_net_1;
assign FP_PL_EN_net_1       = FP_PL_EN_net_0;
assign FP_PL_EN             = FP_PL_EN_net_1;
assign FP_PL_TXD_net_1      = FP_PL_TXD_net_0;
assign FP_PL_TXD            = FP_PL_TXD_net_1;
assign FP_RW2_TX_net_1      = FP_RW2_TX_net_0;
assign FP_RW2_TX            = FP_RW2_TX_net_1;
assign FP_RW3_TX_net_1      = FP_RW3_TX_net_0;
assign FP_RW3_TX            = FP_RW3_TX_net_1;
assign GC_CLK_net_1         = GC_CLK_net_0;
assign GC_CLK               = GC_CLK_net_1;
assign FP_RW1_TX_net_1      = FP_RW1_TX_net_0;
assign FP_RW1_TX            = FP_RW1_TX_net_1;
assign FP_NMI_net_1         = FP_NMI_net_0;
assign FP_NMI               = FP_NMI_net_1;
assign FP_RW4_TX_net_1      = FP_RW4_TX_net_0;
assign FP_RW4_TX            = FP_RW4_TX_net_1;
assign TS_SPI_DIN_3_net_1   = TS_SPI_DIN_3_net_0;
assign TS_SPI_DIN_3         = TS_SPI_DIN_3_net_1;
assign TS_SPI_NCS_net_1     = TS_SPI_NCS_net_0;
assign TS_SPI_NCS           = TS_SPI_NCS_net_1;
assign TS_SPI_SCLK_net_1    = TS_SPI_SCLK_net_0;
assign TS_SPI_SCLK          = TS_SPI_SCLK_net_1;
assign Heater1_ON_OFF_net_1 = Heater1_ON_OFF_net_0;
assign Heater1_ON_OFF       = Heater1_ON_OFF_net_1;
assign Heater6_ON_OFF_net_1 = Heater6_ON_OFF_net_0;
assign Heater6_ON_OFF       = Heater6_ON_OFF_net_1;
assign SA1_DEPLOY_net_1     = SA1_DEPLOY_net_0;
assign SA1_DEPLOY           = SA1_DEPLOY_net_1;
assign RW1_ON_OFF_net_1     = RW1_ON_OFF_net_0;
assign RW1_ON_OFF           = RW1_ON_OFF_net_1;
assign RW2_ON_OFF_net_1     = RW2_ON_OFF_net_0;
assign RW2_ON_OFF           = RW2_ON_OFF_net_1;
assign Heater2_ON_OFF_net_1 = Heater2_ON_OFF_net_0;
assign Heater2_ON_OFF       = Heater2_ON_OFF_net_1;
assign ANT_DEPLOY_net_1     = ANT_DEPLOY_net_0;
assign ANT_DEPLOY           = ANT_DEPLOY_net_1;
assign Heater3_ON_OFF_net_1 = Heater3_ON_OFF_net_0;
assign Heater3_ON_OFF       = Heater3_ON_OFF_net_1;
assign Heater5_ON_OFF_net_1 = Heater5_ON_OFF_net_0;
assign Heater5_ON_OFF       = Heater5_ON_OFF_net_1;
assign SA2_DEPLOY_net_1     = SA2_DEPLOY_net_0;
assign SA2_DEPLOY           = SA2_DEPLOY_net_1;
assign RW3_ON_OFF_net_1     = RW3_ON_OFF_net_0;
assign RW3_ON_OFF           = RW3_ON_OFF_net_1;
assign Heater4_ON_OFF_net_1 = Heater4_ON_OFF_net_0;
assign Heater4_ON_OFF       = Heater4_ON_OFF_net_1;
assign RW4_ON_OFF_net_1     = RW4_ON_OFF_net_0;
assign RW4_ON_OFF           = RW4_ON_OFF_net_1;
assign TM_DS_EN_net_1       = TM_DS_EN_net_0;
assign TM_DS_EN             = TM_DS_EN_net_1;
assign GPS1_ON_OFF_net_1    = GPS1_ON_OFF_net_0;
assign GPS1_ON_OFF          = GPS1_ON_OFF_net_1;
assign GPS2_Reset_net_1     = GPS2_Reset_net_0;
assign GPS2_Reset           = GPS2_Reset_net_1;
assign SPARE2_ON_OFF_net_1  = SPARE2_ON_OFF_net_0;
assign SPARE2_ON_OFF        = SPARE2_ON_OFF_net_1;
assign IMU1_Reset_net_1     = IMU1_Reset_net_0;
assign IMU1_Reset           = IMU1_Reset_net_1;
assign MTR_ON_OFF_net_1     = MTR_ON_OFF_net_0;
assign MTR_ON_OFF           = MTR_ON_OFF_net_1;
assign IMU1_ON_OFF_net_1    = IMU1_ON_OFF_net_0;
assign IMU1_ON_OFF          = IMU1_ON_OFF_net_1;
assign SPARE1_ON_OFF_net_1  = SPARE1_ON_OFF_net_0;
assign SPARE1_ON_OFF        = SPARE1_ON_OFF_net_1;
assign Payload_ON_OFF_net_1 = Payload_ON_OFF_net_0;
assign Payload_ON_OFF       = Payload_ON_OFF_net_1;
assign GPS2_ON_OFF_net_1    = GPS2_ON_OFF_net_0;
assign GPS2_ON_OFF          = GPS2_ON_OFF_net_1;
assign IMU2_Reset_net_1     = IMU2_Reset_net_0;
assign IMU2_Reset           = IMU2_Reset_net_1;
assign RF_Tx_ON_OFF_net_1   = RF_Tx_ON_OFF_net_0;
assign RF_Tx_ON_OFF         = RF_Tx_ON_OFF_net_1;
assign GPS1_Reset_net_1     = GPS1_Reset_net_0;
assign GPS1_Reset           = GPS1_Reset_net_1;
assign X_Tx_ON_OFF_net_1    = X_Tx_ON_OFF_net_0;
assign X_Tx_ON_OFF          = X_Tx_ON_OFF_net_1;
assign IMU2_ON_OFF_net_1    = IMU2_ON_OFF_net_0;
assign IMU2_ON_OFF          = IMU2_ON_OFF_net_1;
assign FP_MTR1_N1_net_1     = FP_MTR1_N1_net_0;
assign FP_MTR1_N1           = FP_MTR1_N1_net_1;
assign FP_MTR1_P2_net_1     = FP_MTR1_P2_net_0;
assign FP_MTR1_P2           = FP_MTR1_P2_net_1;
assign FP_MTR2_P1_net_1     = FP_MTR2_P1_net_0;
assign FP_MTR2_P1           = FP_MTR2_P1_net_1;
assign FP_MTR1_P1_net_1     = FP_MTR1_P1_net_0;
assign FP_MTR1_P1           = FP_MTR1_P1_net_1;
assign FP_MTR3_N1_net_1     = FP_MTR3_N1_net_0;
assign FP_MTR3_N1           = FP_MTR3_N1_net_1;
assign FP_MTR2_N1_net_1     = FP_MTR2_N1_net_0;
assign FP_MTR2_N1           = FP_MTR2_N1_net_1;
assign FP_MTR2_N2_net_1     = FP_MTR2_N2_net_0;
assign FP_MTR2_N2           = FP_MTR2_N2_net_1;
assign FP_MTR3_N2_net_1     = FP_MTR3_N2_net_0;
assign FP_MTR3_N2           = FP_MTR3_N2_net_1;
assign FP_MTR3_P2_net_1     = FP_MTR3_P2_net_0;
assign FP_MTR3_P2           = FP_MTR3_P2_net_1;
assign FP_MTR2_P2_net_1     = FP_MTR2_P2_net_0;
assign FP_MTR2_P2           = FP_MTR2_P2_net_1;
assign FP_MTR1_N2_net_1     = FP_MTR1_N2_net_0;
assign FP_MTR1_N2           = FP_MTR1_N2_net_1;
assign FP_MTR3_P1_net_1     = FP_MTR3_P1_net_0;
assign FP_MTR3_P1           = FP_MTR3_P1_net_1;
assign MAJ_RST_net_1        = MAJ_RST_net_0;
assign MAJ_RST              = MAJ_RST_net_1;
assign MIN_RST_net_1        = MIN_RST_net_0;
assign MIN_RST              = MIN_RST_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------latch2_on_off_reset
latch2_on_off_reset latch2_on_off_reset_0(
        // Inputs
        .PO_Reset       ( PO_Reset ),
        .latch2_CS      ( latch2_CS ),
        .MP_write       ( MP_write ),
        .MP_data_in     ( MP_data_in ),
        // Outputs
        .IMU1_ON_OFF    ( IMU1_ON_OFF_net_0 ),
        .IMU1_Reset     ( IMU1_Reset_net_0 ),
        .IMU2_ON_OFF    ( IMU2_ON_OFF_net_0 ),
        .IMU2_Reset     ( IMU2_Reset_net_0 ),
        .GPS1_ON_OFF    ( GPS1_ON_OFF_net_0 ),
        .GPS1_Reset     ( GPS1_Reset_net_0 ),
        .GPS2_ON_OFF    ( GPS2_ON_OFF_net_0 ),
        .GPS2_Reset     ( GPS2_Reset_net_0 ),
        .MTR_ON_OFF     ( MTR_ON_OFF_net_0 ),
        .Payload_ON_OFF ( Payload_ON_OFF_net_0 ),
        .X_Tx_ON_OFF    ( X_Tx_ON_OFF_net_0 ),
        .RF_Tx_ON_OFF   ( RF_Tx_ON_OFF_net_0 ),
        .SPARE1_ON_OFF  ( SPARE1_ON_OFF_net_0 ),
        .SPARE2_ON_OFF  ( SPARE2_ON_OFF_net_0 ) 
        );

//--------latch3_heater_cntrls
latch3_heater_cntrls latch3_heater_cntrls_0(
        // Inputs
        .PO_Reset       ( PO_Reset ),
        .latch3_CS      ( latch3_CS ),
        .MP_write       ( MP_write ),
        .MP_data_in     ( MP_data_in ),
        // Outputs
        .Heater1_ON_OFF ( Heater1_ON_OFF_net_0 ),
        .Heater2_ON_OFF ( Heater2_ON_OFF_net_0 ),
        .Heater3_ON_OFF ( Heater3_ON_OFF_net_0 ),
        .Heater4_ON_OFF ( Heater4_ON_OFF_net_0 ),
        .Heater5_ON_OFF ( Heater5_ON_OFF_net_0 ),
        .Heater6_ON_OFF ( Heater6_ON_OFF_net_0 ),
        .RW1_ON_OFF     ( RW1_ON_OFF_net_0 ),
        .RW2_ON_OFF     ( RW2_ON_OFF_net_0 ),
        .RW3_ON_OFF     ( RW3_ON_OFF_net_0 ),
        .RW4_ON_OFF     ( RW4_ON_OFF_net_0 ),
        .ANT_DEPLOY     ( ANT_DEPLOY_net_0 ),
        .SA1_DEPLOY     ( SA1_DEPLOY_net_0 ),
        .SA2_DEPLOY     ( SA2_DEPLOY_net_0 ),
        .TM_DS_EN       ( TM_DS_EN_net_0 ) 
        );

//--------latch4_othersmodule
latch4_othersmodule latch4_othersmodule_0(
        // Inputs
        .PO_Reset     ( PO_Reset ),
        .MP_data_in   ( MP_data_in ),
        .latch4_CS    ( latch4_CS ),
        .MP_write     ( MP_write ),
        // Outputs
        .FP_RW1_TX    ( FP_RW1_TX_net_0 ),
        .FP_RW2_TX    ( FP_RW2_TX_net_0 ),
        .FP_RW3_TX    ( FP_RW3_TX_net_0 ),
        .FP_RW4_TX    ( FP_RW4_TX_net_0 ),
        .FP_PL_TXD    ( FP_PL_TXD_net_0 ),
        .FP_PL_EN     ( FP_PL_EN_net_0 ),
        .MIN_RST      ( MIN_RST_net_0 ),
        .MAJ_RST      ( MAJ_RST_net_0 ),
        .GC_CLK       ( GC_CLK_net_0 ),
        .TS_SPI_SCLK  ( TS_SPI_SCLK_net_0 ),
        .TS_SPI_DIN_3 ( TS_SPI_DIN_3_net_0 ),
        .TS_SPI_NCS   ( TS_SPI_NCS_net_0 ),
        .FP_NMI       ( FP_NMI_net_0 ) 
        );

//--------latch5_others2
latch5_others2 latch5_others2_0(
        // Inputs
        .PO_Reset       ( PO_Reset ),
        .latch5_CS      ( latch5_CS ),
        .MP_write       ( MP_write ),
        .MP_data_in     ( MP_data_in ),
        // Outputs
        .SEL_0          ( SEL_0_net_0 ),
        .SEL_1          ( SEL_1_net_0 ),
        .SEL_2          ( SEL_2_net_0 ),
        .SENSOR1_ON_OFF ( SENSOR1_ON_OFF_net_0 ),
        .SENSOR2_ON_OFF ( SENSOR2_ON_OFF_net_0 ),
        .SENSOR3_ON_OFF ( SENSOR3_ON_OFF_net_0 ),
        .SENSOR4_ON_OFF ( SENSOR4_ON_OFF_net_0 ),
        .SENSOR5_ON_OFF ( SENSOR5_ON_OFF_net_0 ),
        .SENSOR6_ON_OFF ( SENSOR6_ON_OFF_net_0 ),
        .SPI_DIN_1      ( SPI_DIN_1_net_0 ),
        .SPI_DIN_2      ( SPI_DIN_2_net_0 ),
        .SPI_NCS_1      ( SPI_NCS_1_net_0 ),
        .SPI_NCS_2      ( SPI_NCS_2_net_0 ),
        .SPI_SCLK_1     ( SPI_SCLK_1_net_0 ),
        .SPI_SCLK_2     ( SPI_SCLK_2_net_0 ),
        .ANT_DEPLOY_SDA ( ANT_DEPLOY_SDA_net_0 ) 
        );

//--------MAG_TORQUER_LOGIC
MAG_TORQUER_LOGIC MAG_TORQUER_LOGIC_0(
        // Inputs
        .reset      ( PO_Reset ),
        .clk_200us  ( clk_200us ),
        .clk        ( clk ),
        .MP_write   ( MP_write ),
        .latch1_CS  ( latch1_CS ),
        .MP_data_in ( MP_data_in ),
        // Outputs
        .FP_MTR1_N2 ( FP_MTR1_N2_net_0 ),
        .FP_MTR1_N1 ( FP_MTR1_N1_net_0 ),
        .FP_MTR1_P2 ( FP_MTR1_P2_net_0 ),
        .FP_MTR1_P1 ( FP_MTR1_P1_net_0 ),
        .FP_MTR2_P2 ( FP_MTR2_P2_net_0 ),
        .FP_MTR2_N2 ( FP_MTR2_N2_net_0 ),
        .FP_MTR2_P1 ( FP_MTR2_P1_net_0 ),
        .FP_MTR2_N1 ( FP_MTR2_N1_net_0 ),
        .FP_MTR3_N1 ( FP_MTR3_N1_net_0 ),
        .FP_MTR3_N2 ( FP_MTR3_N2_net_0 ),
        .FP_MTR3_P1 ( FP_MTR3_P1_net_0 ),
        .FP_MTR3_P2 ( FP_MTR3_P2_net_0 ) 
        );


endmodule
