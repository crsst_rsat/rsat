//--------------------------------------------------------------------
// Created by Microsemi SmartDesign Sat Feb 17 21:00:07 2018
// Parameters for COREUART
//--------------------------------------------------------------------


parameter BAUD_VAL_FRCTN_EN = 1;
parameter FAMILY = 16;
parameter HDL_license = "U";
parameter RX_FIFO = 0;
parameter RX_LEGACY_MODE = 0;
parameter testbench = "User";
parameter TX_FIFO = 0;
parameter USE_SOFT_FIFO = 0;
