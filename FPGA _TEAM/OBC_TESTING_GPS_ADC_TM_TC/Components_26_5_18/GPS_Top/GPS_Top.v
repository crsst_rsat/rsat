//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Sat Feb 17 21:00:07 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// GPS_Top
module GPS_Top(
    // Inputs
    GPS_CS_n,
    MP_addr,
    MP_data_in,
    MP_re,
    MP_we,
    RX,
    clk,
    clk_2us,
    reset,
    vpp,
    // Outputs
    MP_bit5,
    MP_data_out,
    TX
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         GPS_CS_n;
input  [8:0]  MP_addr;
input  [15:0] MP_data_in;
input         MP_re;
input         MP_we;
input         RX;
input         clk;
input         clk_2us;
input         reset;
input         vpp;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        MP_bit5;
output [15:0] MP_data_out;
output        TX;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          arbitration_logic_0_buffer_re;
wire          arbitration_logic_0_status_MP_re;
wire   [15:0] Buffer_data_0_data_out;
wire          clk;
wire          clk_2us;
wire          clk_divider_0_clk_100us_0;
wire   [3:0]  config_bytes;
wire          config_MP_we;
wire   [7:0]  config_reg_0_config_data_out;
wire   [2:0]  config_reg_0_config_type_0;
wire   [7:0]  COREUART_1_DATA_OUT;
wire          COREUART_1_FRAMING_ERR;
wire          COREUART_1_OVERFLOW;
wire          COREUART_1_PARITY_ERR;
wire          COREUART_1_RXRDY;
wire          COREUART_1_TXRDY;
wire   [7:0]  GPS_controller_0_buf_address;
wire   [15:0] GPS_controller_0_buf_data;
wire          GPS_controller_0_buf_we;
wire   [3:0]  GPS_controller_0_config_addr_FPGA;
wire          GPS_controller_0_config_en_rst;
wire          GPS_controller_0_data_NA;
wire          GPS_controller_0_data_NA_clr;
wire          GPS_controller_0_data_NA_en;
wire          GPS_controller_0_datardy;
wire          GPS_controller_0_fault;
wire          GPS_controller_0_fault_clr;
wire          GPS_controller_0_fault_en;
wire          GPS_controller_0_framing_err;
wire          GPS_controller_0_overflow_err;
wire          GPS_controller_0_parity_err;
wire          GPS_controller_0_PPS_overflow;
wire          GPS_controller_0_presetn;
wire          GPS_controller_0_Status_we;
wire          GPS_controller_0_UART_RE;
wire          GPS_controller_0_UART_WEN;
wire          GPS_CS_n;
wire   [8:0]  MP_addr;
wire          MP_bit5_net_0;
wire   [15:0] MP_data;
wire   [15:0] MP_data_in;
wire          MP_re;
wire          MP_status_re;
wire          MP_we;
wire          reset;
wire          RX;
wire   [2:0]  Status_register_0_config_NMEA_msg;
wire          Status_register_0_MP_GPS_config_re;
wire   [15:0] Status_register_0_status_dataout;
wire          status_we;
wire          TX_net_0;
wire          vpp;
wire          TX_net_1;
wire          MP_bit5_net_1;
wire   [15:0] MP_data_net_0;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire          VCC_net;
wire          GND_net;
wire   [12:0] BAUD_VAL_const_net_0;
wire   [2:0]  BAUD_VAL_FRACTION_const_net_0;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign VCC_net                       = 1'b1;
assign GND_net                       = 1'b0;
assign BAUD_VAL_const_net_0          = 13'h00A1;
assign BAUD_VAL_FRACTION_const_net_0 = 3'h6;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign TX_net_1          = TX_net_0;
assign TX                = TX_net_1;
assign MP_bit5_net_1     = MP_bit5_net_0;
assign MP_bit5           = MP_bit5_net_1;
assign MP_data_net_0     = MP_data;
assign MP_data_out[15:0] = MP_data_net_0;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------GPS_arbitration_logic
GPS_arbitration_logic arbitration_logic_0(
        // Inputs
        .clk          ( clk ),
        .reset        ( reset ),
        .MP_re        ( MP_re ),
        .MP_we        ( MP_we ),
        .GPS_CS_n     ( GPS_CS_n ),
        .MP_addr      ( MP_addr ),
        // Outputs
        .status_we    ( status_we ),
        .buffer_re    ( arbitration_logic_0_buffer_re ),
        .status_MP_re ( arbitration_logic_0_status_MP_re ),
        .config_MP_we ( config_MP_we ) 
        );

//--------GPS_Buffer_data
GPS_Buffer_data Buffer_data_0(
        // Inputs
        .clk        ( clk ),
        .reset      ( reset ),
        .read_en    ( arbitration_logic_0_buffer_re ),
        .write_en   ( GPS_controller_0_buf_we ),
        .address    ( GPS_controller_0_buf_address ),
        .rd_address ( MP_addr ),
        .data_in    ( GPS_controller_0_buf_data ),
        // Outputs
        .data_out   ( Buffer_data_0_data_out ) 
        );

//--------GPS_clk_divider
GPS_clk_divider clk_divider_0(
        // Inputs
        .clk       ( clk_2us ),
        .reset     ( reset ),
        // Outputs
        .clk_100us ( clk_divider_0_clk_100us_0 ) 
        );

//--------GPS_config_reg
GPS_config_reg config_reg_0(
        // Inputs
        .reset            ( reset ),
        .we               ( config_MP_we ),
        .config_data_in   ( MP_data_in ),
        .config_addr_MP   ( MP_addr ),
        .config_addr_FPGA ( GPS_controller_0_config_addr_FPGA ),
        // Outputs
        .config_type      ( config_reg_0_config_type_0 ),
        .config_data_out  ( config_reg_0_config_data_out ) 
        );

//--------GPS_Top_COREUART_1_COREUART   -   Actel:DirectCore:COREUART:5.6.102
GPS_Top_COREUART_1_COREUART #( 
        .BAUD_VAL_FRCTN_EN ( 1 ),
        .FAMILY            ( 16 ),
        .RX_FIFO           ( 0 ),
        .RX_LEGACY_MODE    ( 0 ),
        .TX_FIFO           ( 0 ) )
COREUART_1(
        // Inputs
        .BIT8              ( VCC_net ),
        .CLK               ( clk ),
        .CSN               ( GND_net ),
        .ODD_N_EVEN        ( GND_net ),
        .OEN               ( GPS_controller_0_UART_RE ),
        .PARITY_EN         ( GND_net ),
        .RESET_N           ( GPS_controller_0_presetn ),
        .RX                ( RX ),
        .WEN               ( GPS_controller_0_UART_WEN ),
        .BAUD_VAL          ( BAUD_VAL_const_net_0 ),
        .DATA_IN           ( config_reg_0_config_data_out ),
        .BAUD_VAL_FRACTION ( BAUD_VAL_FRACTION_const_net_0 ),
        // Outputs
        .OVERFLOW          ( COREUART_1_OVERFLOW ),
        .PARITY_ERR        ( COREUART_1_PARITY_ERR ),
        .RXRDY             ( COREUART_1_RXRDY ),
        .TX                ( TX_net_0 ),
        .TXRDY             ( COREUART_1_TXRDY ),
        .FRAMING_ERR       ( COREUART_1_FRAMING_ERR ),
        .DATA_OUT          ( COREUART_1_DATA_OUT ) 
        );

//--------GPS_data_sel
GPS_data_sel data_sel_0(
        // Inputs
        .MP_st_re    ( arbitration_logic_0_status_MP_re ),
        .MP_data_re  ( arbitration_logic_0_buffer_re ),
        .status_out  ( Status_register_0_status_dataout ),
        .GPSdata_out ( Buffer_data_0_data_out ),
        // Outputs
        .MP_data     ( MP_data ) 
        );

//--------GPS_controller
GPS_controller GPS_controller_0(
        // Inputs
        .clk              ( clk ),
        .clk_2us          ( clk_2us ),
        .reset            ( reset ),
        .vpp              ( vpp ),
        .rxrdy            ( COREUART_1_RXRDY ),
        .Txrdy            ( COREUART_1_TXRDY ),
        .MP_status_re     ( MP_status_re ),
        .MP_GPS_config_en ( Status_register_0_MP_GPS_config_re ),
        .framing_err_in   ( COREUART_1_FRAMING_ERR ),
        .parity_err_in    ( COREUART_1_PARITY_ERR ),
        .overflow_err_in  ( COREUART_1_OVERFLOW ),
        .clk_100us        ( clk_divider_0_clk_100us_0 ),
        .pdata            ( COREUART_1_DATA_OUT ),
        .config_NMEA_msg  ( Status_register_0_config_NMEA_msg ),
        .config_bytes     ( config_bytes ),
        .config_type      ( config_reg_0_config_type_0 ),
        // Outputs
        .UART_RE          ( GPS_controller_0_UART_RE ),
        .presetn          ( GPS_controller_0_presetn ),
        .fault_clr        ( GPS_controller_0_fault_clr ),
        .data_NA_clr      ( GPS_controller_0_data_NA_clr ),
        .buf_we           ( GPS_controller_0_buf_we ),
        .data_NA          ( GPS_controller_0_data_NA ),
        .fault            ( GPS_controller_0_fault ),
        .fault_en         ( GPS_controller_0_fault_en ),
        .data_NA_en       ( GPS_controller_0_data_NA_en ),
        .datardy          ( GPS_controller_0_datardy ),
        .framing_err      ( GPS_controller_0_framing_err ),
        .parity_err       ( GPS_controller_0_parity_err ),
        .overflow_err     ( GPS_controller_0_overflow_err ),
        .PPS_overflow     ( GPS_controller_0_PPS_overflow ),
        .UART_WEN         ( GPS_controller_0_UART_WEN ),
        .Status_we        ( GPS_controller_0_Status_we ),
        .config_en_rst    ( GPS_controller_0_config_en_rst ),
        .buf_address      ( GPS_controller_0_buf_address ),
        .buf_data         ( GPS_controller_0_buf_data ),
        .config_addr_FPGA ( GPS_controller_0_config_addr_FPGA ) 
        );

//--------GPS_Status_register
GPS_Status_register Status_register_0(
        // Inputs
        .MP_re            ( arbitration_logic_0_status_MP_re ),
        .MP_we            ( status_we ),
        .reset            ( reset ),
        .clk              ( clk ),
        .framing_err      ( GPS_controller_0_framing_err ),
        .overflow_err     ( GPS_controller_0_overflow_err ),
        .parity_err       ( GPS_controller_0_parity_err ),
        .datardy          ( GPS_controller_0_datardy ),
        .data_NA          ( GPS_controller_0_data_NA ),
        .fault            ( GPS_controller_0_fault ),
        .PPS_overflow     ( GPS_controller_0_PPS_overflow ),
        .fault_en         ( GPS_controller_0_fault_en ),
        .data_NA_en       ( GPS_controller_0_data_NA_en ),
        .FPGA_we          ( GPS_controller_0_Status_we ),
        .config_en_rst    ( GPS_controller_0_config_en_rst ),
        .fault_clr        ( GPS_controller_0_fault_clr ),
        .data_NA_clr      ( GPS_controller_0_data_NA_clr ),
        .MP_data_in       ( MP_data_in ),
        // Outputs
        .MP_bit5          ( MP_bit5_net_0 ),
        .MP_status_re     ( MP_status_re ),
        .MP_GPS_config_re ( Status_register_0_MP_GPS_config_re ),
        .config_bytes     ( config_bytes ),
        .config_NMEA_msg  ( Status_register_0_config_NMEA_msg ),
        .status_dataout   ( Status_register_0_status_dataout ) 
        );


endmodule
