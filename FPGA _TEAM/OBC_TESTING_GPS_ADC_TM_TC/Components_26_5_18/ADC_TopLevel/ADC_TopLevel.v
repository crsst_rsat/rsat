//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Sun Feb 04 12:36:27 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// ADC_TopLevel
module ADC_TopLevel(
    // Inputs
    ADC_CS_n,
    ADC_data_in,
    EOC_n,
    MP_addr_0,
    MP_data_in,
    MP_re,
    MP_write,
    clk,
    reset,
    // Outputs
    CS_n,
    MP_data_out,
    RD_n,
    m1_address,
    m2_address,
    stconv_n
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         ADC_CS_n;
input  [11:0] ADC_data_in;
input         EOC_n;
input  [8:0]  MP_addr_0;
input  [15:0] MP_data_in;
input         MP_re;
input         MP_write;
input         clk;
input         reset;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        CS_n;
output [15:0] MP_data_out;
output        RD_n;
output [3:0]  m1_address;
output [4:0]  m2_address;
output        stconv_n;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          ADC_CS_n;
wire          ADC_CS_n_0;
wire          ADC_RD_n;
wire          arbitration_logic_0_buffer_re;
wire          arbitration_logic_0_status_we;
wire          clk;
wire          clk_divider_0_clk_1us;
wire   [11:0] ADC_data_in;
wire          EOC_n;
wire   [3:0]  m1_address_1;
wire   [4:0]  m2_address_0_0;
wire   [8:0]  MP_addr_0;
wire   [15:0] MP_data_in;
wire   [15:0] MP_data_out_net_0;
wire          MP_re;
wire          MP_write;
wire          reset;
wire          Status_register_0_MP_re;
wire          stconv_n_net_0;
wire   [5:0]  Synch_0_Buf_addr;
wire          Synch_0_buf_write_n;
wire          stconv_n_net_1;
wire          ADC_RD_n_net_0;
wire          ADC_CS_n_0_net_0;
wire   [4:0]  m2_address_0_0_net_0;
wire   [3:0]  m1_address_1_net_0;
wire   [15:0] MP_data_out_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign stconv_n_net_1       = stconv_n_net_0;
assign stconv_n             = stconv_n_net_1;
assign ADC_RD_n_net_0       = ADC_RD_n;
assign RD_n                 = ADC_RD_n_net_0;
assign ADC_CS_n_0_net_0     = ADC_CS_n_0;
assign CS_n                 = ADC_CS_n_0_net_0;
assign m2_address_0_0_net_0 = m2_address_0_0;
assign m2_address[4:0]      = m2_address_0_0_net_0;
assign m1_address_1_net_0   = m1_address_1;
assign m1_address[3:0]      = m1_address_1_net_0;
assign MP_data_out_net_1    = MP_data_out_net_0;
assign MP_data_out[15:0]    = MP_data_out_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------ADC_arbitration_logic
ADC_arbitration_logic arbitration_logic_0(
        // Inputs
        .clk       ( clk ),
        .reset     ( reset ),
        .MP_re     ( MP_re ),
        .MP_we     ( MP_write ),
        .ADC_CS_n  ( ADC_CS_n ),
        .MP_addr   ( MP_addr_0 ),
        // Outputs
        .status_we ( arbitration_logic_0_status_we ),
        .buffer_re ( arbitration_logic_0_buffer_re ) 
        );

//--------ADC_Buffer
ADC_Buffer Buffer_0(
        // Inputs
        .clk        ( clk ),
        .reset      ( reset ),
        .address    ( Synch_0_Buf_addr ),
        .rd_address ( MP_addr_0 ),
        .read_en    ( arbitration_logic_0_buffer_re ),
        .write_en_n ( Synch_0_buf_write_n ),
        .data_in    ( ADC_data_in ),
        // Outputs
        .data_out   ( MP_data_out_net_0 ) 
        );

//--------ADC_clk_divider
ADC_clk_divider clk_divider_0(
        // Inputs
        .clk     ( clk ),
        .reset   ( reset ),
        // Outputs
        .clk_1us ( clk_divider_0_clk_1us ) 
        );

//--------ADC_Status_register
ADC_Status_register Status_register_0(
        // Inputs
        .w_en    ( arbitration_logic_0_status_we ),
        .data_in ( MP_data_in ),
        .reset   ( reset ),
        // Outputs
        .MP_re   ( Status_register_0_MP_re ) 
        );

//--------Synch
Synch Synch_0(
        // Inputs
        .clk         ( clk ),
        .clk_1us     ( clk_divider_0_clk_1us ),
        .reset       ( reset ),
        .MP_re       ( Status_register_0_MP_re ),
        .EOC_n       ( EOC_n ),
        // Outputs
        .stconv_n    ( stconv_n_net_0 ),
        .ADC_RD_n    ( ADC_RD_n ),
        .ADC_CS_n    ( ADC_CS_n_0 ),
        .buf_write_n ( Synch_0_buf_write_n ),
        .m2_address  ( m2_address_0_0 ),
        .m1_address  ( m1_address_1 ),
        .Buf_addr    ( Synch_0_Buf_addr ) 
        );


endmodule
