//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Sun Feb 25 22:21:04 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// Telemetry_Top
module Telemetry_Top(
    // Inputs
    ADD_0,
    CS,
    MP_datain,
    RD,
    WR,
    clock_10KHz,
    clock_40M,
    master_reset,
    // Outputs
    MP_dataout,
    conv_out,
    conv_out1,
    conv_out2,
    encoder_regs,
    ip_data,
    main_out,
    status,
    uncoded_out
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input  [8:0]  ADD_0;
input         CS;
input  [15:0] MP_datain;
input         RD;
input         WR;
input         clock_10KHz;
input         clock_40M;
input         master_reset;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output [15:0] MP_dataout;
output        conv_out;
output        conv_out1;
output        conv_out2;
output [6:0]  encoder_regs;
output [15:0] ip_data;
output        main_out;
output [15:0] status;
output        uncoded_out;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire   [8:0]  ADD_0;
wire          clock_10KHz;
wire          clock_40M;
wire          conv_out_net_0;
wire          conv_out1_net_0;
wire          conv_out2_net_0;
wire          CS;
wire   [6:0]  encoder_regs_net_0;
wire   [15:0] ip_data_net_0;
wire          main_out_net_0;
wire          master_reset;
wire   [15:0] MP_datain;
wire   [15:0] MP_dataout_net_0;
wire          RD;
wire   [15:0] status_net_0;
wire          uncoded_out_net_0;
wire          WR;
wire          conv_out1_net_1;
wire          main_out_net_1;
wire          conv_out_net_1;
wire          uncoded_out_net_1;
wire          conv_out2_net_1;
wire   [15:0] ip_data_net_1;
wire   [6:0]  encoder_regs_net_1;
wire   [15:0] status_net_1;
wire   [15:0] MP_dataout_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign conv_out1_net_1    = conv_out1_net_0;
assign conv_out1          = conv_out1_net_1;
assign main_out_net_1     = main_out_net_0;
assign main_out           = main_out_net_1;
assign conv_out_net_1     = conv_out_net_0;
assign conv_out           = conv_out_net_1;
assign uncoded_out_net_1  = uncoded_out_net_0;
assign uncoded_out        = uncoded_out_net_1;
assign conv_out2_net_1    = conv_out2_net_0;
assign conv_out2          = conv_out2_net_1;
assign ip_data_net_1      = ip_data_net_0;
assign ip_data[15:0]      = ip_data_net_1;
assign encoder_regs_net_1 = encoder_regs_net_0;
assign encoder_regs[6:0]  = encoder_regs_net_1;
assign status_net_1       = status_net_0;
assign status[15:0]       = status_net_1;
assign MP_dataout_net_1   = MP_dataout_net_0;
assign MP_dataout[15:0]   = MP_dataout_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------dual_port
dual_port dual_port_0(
        // Inputs
        .clock_40M    ( clock_40M ),
        .clock_10KHz  ( clock_10KHz ),
        .master_reset ( master_reset ),
        .CS           ( CS ),
        .ADD          ( ADD_0 ),
        .RD           ( RD ),
        .WR           ( WR ),
        .MP_datain    ( MP_datain ),
        // Outputs
        .MP_dataout   ( MP_dataout_net_0 ),
        .ip_data      ( ip_data_net_0 ),
        .status       ( status_net_0 ),
        .main_out     ( main_out_net_0 ),
        .uncoded_out  ( uncoded_out_net_0 ),
        .conv_out1    ( conv_out1_net_0 ),
        .conv_out2    ( conv_out2_net_0 ),
        .encoder_regs ( encoder_regs_net_0 ),
        .conv_out     ( conv_out_net_0 ) 
        );


endmodule
