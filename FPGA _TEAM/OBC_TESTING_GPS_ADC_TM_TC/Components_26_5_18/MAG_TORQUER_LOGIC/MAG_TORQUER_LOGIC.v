//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Wed Feb 21 10:32:36 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// MAG_TORQUER_LOGIC
module MAG_TORQUER_LOGIC(
    // Inputs
    MP_data_in,
    MP_write,
    clk,
    clk_200us,
    latch1_CS,
    reset,
    // Outputs
    FP_MTR1_N1,
    FP_MTR1_N2,
    FP_MTR1_P1,
    FP_MTR1_P2,
    FP_MTR2_N1,
    FP_MTR2_N2,
    FP_MTR2_P1,
    FP_MTR2_P2,
    FP_MTR3_N1,
    FP_MTR3_N2,
    FP_MTR3_P1,
    FP_MTR3_P2
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input  [15:0] MP_data_in;
input         MP_write;
input         clk;
input         clk_200us;
input         latch1_CS;
input         reset;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        FP_MTR1_N1;
output        FP_MTR1_N2;
output        FP_MTR1_P1;
output        FP_MTR1_P2;
output        FP_MTR2_N1;
output        FP_MTR2_N2;
output        FP_MTR2_P1;
output        FP_MTR2_P2;
output        FP_MTR3_N1;
output        FP_MTR3_N2;
output        FP_MTR3_P1;
output        FP_MTR3_P2;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          clk;
wire          clk_200us;
wire          FP_MTR1_N1_net_0;
wire          FP_MTR1_N2_net_0;
wire          FP_MTR1_P1_net_0;
wire          FP_MTR1_P2_net_0;
wire          FP_MTR2_N1_net_0;
wire          FP_MTR2_N2_net_0;
wire          FP_MTR2_P1_net_0;
wire          FP_MTR2_P2_net_0;
wire          FP_MTR3_N1_net_0;
wire          FP_MTR3_N2_net_0;
wire          FP_MTR3_P1_net_0;
wire          FP_MTR3_P2_net_0;
wire          latch1_CS;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR1_N1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR1_N2;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR1_P1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR1_P2;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR2_N1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR2_N2;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR2_P1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR2_P2;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR3_N1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR3_N2;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR3_P1;
wire          latch1_MTR_cntrls_0_FP_CTRL_MTR3_P2;
wire   [15:0] MP_data_in;
wire          MP_write;
wire          reset;
wire          FP_MTR1_N2_net_1;
wire          FP_MTR1_N1_net_1;
wire          FP_MTR1_P2_net_1;
wire          FP_MTR1_P1_net_1;
wire          FP_MTR2_P2_net_1;
wire          FP_MTR2_N2_net_1;
wire          FP_MTR2_P1_net_1;
wire          FP_MTR2_N1_net_1;
wire          FP_MTR3_N1_net_1;
wire          FP_MTR3_N2_net_1;
wire          FP_MTR3_P1_net_1;
wire          FP_MTR3_P2_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign FP_MTR1_N2_net_1 = FP_MTR1_N2_net_0;
assign FP_MTR1_N2       = FP_MTR1_N2_net_1;
assign FP_MTR1_N1_net_1 = FP_MTR1_N1_net_0;
assign FP_MTR1_N1       = FP_MTR1_N1_net_1;
assign FP_MTR1_P2_net_1 = FP_MTR1_P2_net_0;
assign FP_MTR1_P2       = FP_MTR1_P2_net_1;
assign FP_MTR1_P1_net_1 = FP_MTR1_P1_net_0;
assign FP_MTR1_P1       = FP_MTR1_P1_net_1;
assign FP_MTR2_P2_net_1 = FP_MTR2_P2_net_0;
assign FP_MTR2_P2       = FP_MTR2_P2_net_1;
assign FP_MTR2_N2_net_1 = FP_MTR2_N2_net_0;
assign FP_MTR2_N2       = FP_MTR2_N2_net_1;
assign FP_MTR2_P1_net_1 = FP_MTR2_P1_net_0;
assign FP_MTR2_P1       = FP_MTR2_P1_net_1;
assign FP_MTR2_N1_net_1 = FP_MTR2_N1_net_0;
assign FP_MTR2_N1       = FP_MTR2_N1_net_1;
assign FP_MTR3_N1_net_1 = FP_MTR3_N1_net_0;
assign FP_MTR3_N1       = FP_MTR3_N1_net_1;
assign FP_MTR3_N2_net_1 = FP_MTR3_N2_net_0;
assign FP_MTR3_N2       = FP_MTR3_N2_net_1;
assign FP_MTR3_P1_net_1 = FP_MTR3_P1_net_0;
assign FP_MTR3_P1       = FP_MTR3_P1_net_1;
assign FP_MTR3_P2_net_1 = FP_MTR3_P2_net_0;
assign FP_MTR3_P2       = FP_MTR3_P2_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------latch1_MTR_cntrls
latch1_MTR_cntrls latch1_MTR_cntrls_0(
        // Inputs
        .clk             ( clk ),
        .PO_Reset        ( reset ),
        .MP_data_in      ( MP_data_in ),
        .latch1_CS       ( latch1_CS ),
        .MP_write        ( MP_write ),
        // Outputs
        .FP_CTRL_MTR1_P1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_P1 ),
        .FP_CTRL_MTR1_P2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_P2 ),
        .FP_CTRL_MTR1_N1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_N1 ),
        .FP_CTRL_MTR1_N2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_N2 ),
        .FP_CTRL_MTR2_P1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_P1 ),
        .FP_CTRL_MTR2_P2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_P2 ),
        .FP_CTRL_MTR2_N1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_N1 ),
        .FP_CTRL_MTR2_N2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_N2 ),
        .FP_CTRL_MTR3_P1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_P1 ),
        .FP_CTRL_MTR3_P2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_P2 ),
        .FP_CTRL_MTR3_N1 ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_N1 ),
        .FP_CTRL_MTR3_N2 ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_N2 ) 
        );

//--------Mag_torquer_Top
Mag_torquer_Top Mag_torquer_Top_0(
        // Inputs
        .MP_P1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_P1 ),
        .MP_P2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_P2 ),
        .MP_N1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_N1 ),
        .MP_N2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR1_N2 ),
        .clk        ( clk ),
        .clk_200us  ( clk_200us ),
        .reset      ( reset ),
        // Outputs
        .delayed_P1 ( FP_MTR1_P1_net_0 ),
        .delayed_P2 ( FP_MTR1_P2_net_0 ),
        .delayed_N1 ( FP_MTR1_N1_net_0 ),
        .delayed_N2 ( FP_MTR1_N2_net_0 ) 
        );

//--------Mag_torquer_Top
Mag_torquer_Top Mag_torquer_Top_1(
        // Inputs
        .MP_P1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_P1 ),
        .MP_P2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_P2 ),
        .MP_N1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_N1 ),
        .MP_N2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR2_N2 ),
        .clk        ( clk ),
        .clk_200us  ( clk_200us ),
        .reset      ( reset ),
        // Outputs
        .delayed_P1 ( FP_MTR2_P1_net_0 ),
        .delayed_P2 ( FP_MTR2_P2_net_0 ),
        .delayed_N1 ( FP_MTR2_N1_net_0 ),
        .delayed_N2 ( FP_MTR2_N2_net_0 ) 
        );

//--------Mag_torquer_Top
Mag_torquer_Top Mag_torquer_Top_2(
        // Inputs
        .MP_P1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_P1 ),
        .MP_P2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_P2 ),
        .MP_N1      ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_N1 ),
        .MP_N2      ( latch1_MTR_cntrls_0_FP_CTRL_MTR3_N2 ),
        .clk        ( clk ),
        .clk_200us  ( clk_200us ),
        .reset      ( reset ),
        // Outputs
        .delayed_P1 ( FP_MTR3_P1_net_0 ),
        .delayed_P2 ( FP_MTR3_P2_net_0 ),
        .delayed_N1 ( FP_MTR3_N1_net_0 ),
        .delayed_N2 ( FP_MTR3_N2_net_0 ) 
        );


endmodule
