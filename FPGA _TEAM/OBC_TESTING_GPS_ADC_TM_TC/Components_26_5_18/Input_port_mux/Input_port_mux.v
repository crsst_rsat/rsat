//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Feb 26 11:27:19 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// Input_port_mux
module Input_port_mux(
    // Inputs
    DIO1_1,
    DIO1_2,
    FP_ADC_D_0,
    FP_ADC_NEOC,
    FP_GPS1_PPS,
    FP_GPS1_Rx,
    FP_GPS2_PPS,
    FP_GPS2_Rx,
    FP_MTR1_mon1,
    FP_MTR1_mon2,
    FP_MTR2_mon1,
    FP_MTR2_mon2,
    FP_MTR3_mon1,
    FP_MTR3_mon2,
    FP_MUXSEL_IMU2,
    FP_PL_RXD,
    FP_PL_STATUS,
    FP_RW1_Rx,
    FP_RW2_Rx,
    FP_RW3_Rx,
    FP_RW4_Rx,
    FP_WDOG,
    LT_EP1_BRDY,
    LT_EP2_BRDY,
    LT_FP_READ,
    MAJ_TC,
    MIN_TC,
    MP_read,
    MUX1_OUT,
    MUX2_OUT,
    MUX3_OUT,
    MUX4_OUT,
    RX_TC_CLK,
    RX_TC_DATA,
    SA1_deploy_ST,
    SA2_deploy_ST,
    SPI_DOUT_1,
    SPI_DOUT_2,
    TC_MUXSEL,
    TS_SPI_DIO1_3,
    TS_SPI_DOUT_3,
    TS_TC_CLK,
    TS_TC_DATA,
    VOBC_TM,
    in_port1_cs,
    in_port2_cs,
    in_port3_cs,
    in_port4_cs,
    // Outputs
    MP_data,
    MP_data_0,
    MP_data_1,
    MP_data_2
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         DIO1_1;
input         DIO1_2;
input  [9:0]  FP_ADC_D_0;
input         FP_ADC_NEOC;
input         FP_GPS1_PPS;
input         FP_GPS1_Rx;
input         FP_GPS2_PPS;
input         FP_GPS2_Rx;
input         FP_MTR1_mon1;
input         FP_MTR1_mon2;
input         FP_MTR2_mon1;
input         FP_MTR2_mon2;
input         FP_MTR3_mon1;
input         FP_MTR3_mon2;
input         FP_MUXSEL_IMU2;
input         FP_PL_RXD;
input         FP_PL_STATUS;
input         FP_RW1_Rx;
input         FP_RW2_Rx;
input         FP_RW3_Rx;
input         FP_RW4_Rx;
input         FP_WDOG;
input         LT_EP1_BRDY;
input         LT_EP2_BRDY;
input         LT_FP_READ;
input         MAJ_TC;
input         MIN_TC;
input         MP_read;
input         MUX1_OUT;
input         MUX2_OUT;
input         MUX3_OUT;
input         MUX4_OUT;
input         RX_TC_CLK;
input         RX_TC_DATA;
input         SA1_deploy_ST;
input         SA2_deploy_ST;
input         SPI_DOUT_1;
input         SPI_DOUT_2;
input         TC_MUXSEL;
input         TS_SPI_DIO1_3;
input         TS_SPI_DOUT_3;
input         TS_TC_CLK;
input         TS_TC_DATA;
input         VOBC_TM;
input         in_port1_cs;
input         in_port2_cs;
input         in_port3_cs;
input         in_port4_cs;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output [15:0] MP_data;
output [15:0] MP_data_0;
output [15:0] MP_data_1;
output [15:0] MP_data_2;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          DIO1_1;
wire          DIO1_2;
wire   [9:0]  FP_ADC_D_0;
wire          FP_ADC_NEOC;
wire          FP_GPS1_PPS;
wire          FP_GPS1_Rx;
wire          FP_GPS2_PPS;
wire          FP_GPS2_Rx;
wire          FP_MTR1_mon1;
wire          FP_MTR1_mon2;
wire          FP_MTR2_mon1;
wire          FP_MTR2_mon2;
wire          FP_MTR3_mon1;
wire          FP_MTR3_mon2;
wire          FP_MUXSEL_IMU2;
wire          FP_PL_RXD;
wire          FP_PL_STATUS;
wire          FP_RW1_Rx;
wire          FP_RW2_Rx;
wire          FP_RW3_Rx;
wire          FP_RW4_Rx;
wire          FP_WDOG;
wire          in_port1_cs;
wire          in_port2_cs;
wire          in_port3_cs;
wire          in_port4_cs;
wire          LT_EP1_BRDY;
wire          LT_EP2_BRDY;
wire          LT_FP_READ;
wire          MAJ_TC;
wire          MIN_TC;
wire   [15:0] MP_data_net_0;
wire   [15:0] MP_data_0_net_0;
wire   [15:0] MP_data_1_net_0;
wire   [15:0] MP_data_2_net_0;
wire          MP_read;
wire          MUX1_OUT;
wire          MUX2_OUT;
wire          MUX3_OUT;
wire          MUX4_OUT;
wire          RX_TC_CLK;
wire          RX_TC_DATA;
wire          SA1_deploy_ST;
wire          SA2_deploy_ST;
wire          SPI_DOUT_1;
wire          SPI_DOUT_2;
wire          TC_MUXSEL;
wire          TS_SPI_DIO1_3;
wire          TS_SPI_DOUT_3;
wire          TS_TC_CLK;
wire          TS_TC_DATA;
wire          VOBC_TM;
wire   [15:0] MP_data_net_1;
wire   [15:0] MP_data_0_net_1;
wire   [15:0] MP_data_1_net_1;
wire   [15:0] MP_data_2_net_1;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign MP_data_net_1   = MP_data_net_0;
assign MP_data[15:0]   = MP_data_net_1;
assign MP_data_0_net_1 = MP_data_0_net_0;
assign MP_data_0[15:0] = MP_data_0_net_1;
assign MP_data_1_net_1 = MP_data_1_net_0;
assign MP_data_1[15:0] = MP_data_1_net_1;
assign MP_data_2_net_1 = MP_data_2_net_0;
assign MP_data_2[15:0] = MP_data_2_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------inpt_port1
inpt_port1 inpt_port1_0(
        // Inputs
        .in_port1_cs    ( in_port1_cs ),
        .MP_read        ( MP_read ),
        .FP_MTR1_mon1   ( FP_MTR1_mon1 ),
        .FP_MTR1_mon2   ( FP_MTR1_mon2 ),
        .FP_MTR2_mon1   ( FP_MTR2_mon1 ),
        .FP_MTR2_mon2   ( FP_MTR2_mon2 ),
        .FP_MTR3_mon1   ( FP_MTR3_mon1 ),
        .FP_MTR3_mon2   ( FP_MTR3_mon2 ),
        .SPI_DOUT_1     ( SPI_DOUT_1 ),
        .SPI_DOUT_2     ( SPI_DOUT_2 ),
        .TS_SPI_DOUT_3  ( TS_SPI_DOUT_3 ),
        .FP_PL_RXD      ( FP_PL_RXD ),
        .FP_PL_STATUS   ( FP_PL_STATUS ),
        .FP_MUXSEL_IMU2 ( FP_MUXSEL_IMU2 ),
        .DIO1_1         ( DIO1_1 ),
        .DIO1_2         ( DIO1_2 ),
        .TS_SPI_DIO1_3  ( TS_SPI_DIO1_3 ),
        .VOBC_TM        ( VOBC_TM ),
        // Outputs
        .MP_data        ( MP_data_net_0 ) 
        );

//--------inpt_port2
inpt_port2 inpt_port2_0(
        // Inputs
        .in_port2_cs   ( in_port2_cs ),
        .MP_read       ( MP_read ),
        .FP_RW1_Rx     ( FP_RW1_Rx ),
        .FP_RW2_Rx     ( FP_RW2_Rx ),
        .FP_RW3_Rx     ( FP_RW3_Rx ),
        .FP_RW4_Rx     ( FP_RW4_Rx ),
        .FP_GPS1_Rx    ( FP_GPS1_Rx ),
        .FP_GPS2_Rx    ( FP_GPS2_Rx ),
        .FP_GPS1_PPS   ( FP_GPS1_PPS ),
        .FP_GPS2_PPS   ( FP_GPS2_PPS ),
        .SA1_deploy_ST ( SA1_deploy_ST ),
        .SA2_deploy_ST ( SA2_deploy_ST ),
        .RX_TC_CLK     ( RX_TC_CLK ),
        .FP_ADC_NEOC   ( FP_ADC_NEOC ),
        .RX_TC_DATA    ( RX_TC_DATA ),
        .TS_TC_CLK     ( TS_TC_CLK ),
        .TS_TC_DATA    ( TS_TC_DATA ),
        .TC_MUXSEL     ( TC_MUXSEL ),
        // Outputs
        .MP_data       ( MP_data_0_net_0 ) 
        );

//--------inpt_port3
inpt_port3 inpt_port3_0(
        // Inputs
        .in_port3_cs ( in_port3_cs ),
        .MP_read     ( MP_read ),
        .LT_EP1_BRDY ( LT_EP1_BRDY ),
        .LT_EP2_BRDY ( LT_EP2_BRDY ),
        .LT_FP_READ  ( LT_FP_READ ),
        .FP_WDOG     ( FP_WDOG ),
        // Outputs
        .MP_data     ( MP_data_1_net_0 ) 
        );

//--------inpt_port4
inpt_port4 inpt_port4_0(
        // Inputs
        .in_port4_cs ( in_port4_cs ),
        .MP_read     ( MP_read ),
        .MUX1_OUT    ( MUX1_OUT ),
        .MUX2_OUT    ( MUX2_OUT ),
        .MUX3_OUT    ( MUX3_OUT ),
        .MUX4_OUT    ( MUX4_OUT ),
        .MIN_TC      ( MIN_TC ),
        .MAJ_TC      ( MAJ_TC ),
        .FP_ADC_D    ( FP_ADC_D_0 ),
        // Outputs
        .MP_data     ( MP_data_2_net_0 ) 
        );


endmodule
