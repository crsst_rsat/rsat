//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Feb 26 11:27:47 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// top
module top(
    // Inputs
    DIO1_1,
    DIO1_2,
    FP_ADC_D,
    FP_ADC_NEOC,
    FP_GPS1_PPS,
    FP_GPS1_Rx,
    FP_GPS2_PPS,
    FP_GPS2_Rx,
    FP_MTR1_mon1,
    FP_MTR1_mon2,
    FP_MTR2_mon1,
    FP_MTR2_mon2,
    FP_MTR3_mon1,
    FP_MTR3_mon2,
    FP_MUXSEL_IMU2,
    FP_PL_RXD,
    FP_PL_STATUS,
    FP_RW1_Rx,
    FP_RW2_Rx,
    FP_RW3_Rx,
    FP_RW4_Rx,
    FP_WDOG,
    LT_EP1_BRDY,
    LT_EP2_BRDY,
    LT_FP_READ,
    MP_addr_0,
    MP_ios_n,
    MP_read,
    MP_write,
    MUX1_OUT,
    MUX2_OUT,
    MUX3_OUT,
    MUX4_OUT,
    RX_TC_CLK,
    RX_TC_DATA,
    SA1_deploy_ST,
    SA2_deploy_ST,
    SPI_DOUT_1,
    SPI_DOUT_2,
    TC_MUXSEL,
    TS_SPI_DIO1_3,
    TS_SPI_DOUT_3,
    TS_TC_CLK,
    TS_TC_DATA,
    VOBC_TM,
    clk,
    clock_10KHz,
    reset,
    // Outputs
    ANT_DEPLOY,
    ANT_DEPLOY_SDA,
    CS_n,
    FP_MTR1_N1,
    FP_MTR1_N2,
    FP_MTR1_P1,
    FP_MTR1_P2,
    FP_MTR2_N1,
    FP_MTR2_N2,
    FP_MTR2_P1,
    FP_MTR2_P2,
    FP_MTR3_N1,
    FP_MTR3_N2,
    FP_MTR3_P1,
    FP_MTR3_P2,
    FP_NMI,
    FP_PL_EN,
    FP_PL_TXD,
    FP_RW1_TX,
    FP_RW2_TX,
    FP_RW3_TX,
    FP_RW4_TX,
    GC_CLK,
    GPS1_ON_OFF,
    GPS1_Reset,
    GPS1_TX,
    GPS2_ON_OFF,
    GPS2_Reset,
    Heater1_ON_OFF,
    Heater2_ON_OFF,
    Heater3_ON_OFF,
    Heater4_ON_OFF,
    Heater5_ON_OFF,
    Heater6_ON_OFF,
    IMU1_ON_OFF,
    IMU1_Reset,
    IMU2_ON_OFF,
    IMU2_Reset,
    MP_Reset,
    MTR_ON_OFF,
    Payload_ON_OFF,
    RD_n,
    RF_Tx_ON_OFF,
    RW1_ON_OFF,
    RW2_ON_OFF,
    RW3_ON_OFF,
    RW4_ON_OFF,
    SA1_DEPLOY,
    SA2_DEPLOY,
    SEL_0,
    SEL_1,
    SEL_2,
    SENSOR1_ON_OFF,
    SENSOR2_ON_OFF,
    SENSOR3_ON_OFF,
    SENSOR4_ON_OFF,
    SENSOR5_ON_OFF,
    SENSOR6_ON_OFF,
    SPARE1_ON_OFF,
    SPARE2_ON_OFF,
    SPI_DIN_1,
    SPI_DIN_2,
    SPI_NCS_1,
    SPI_NCS_2,
    SPI_SCLK_1,
    SPI_SCLK_2,
    TM_DS_EN,
    TS_SPI_DIN_3,
    TS_SPI_NCS,
    TS_SPI_SCLK,
    X_Tx_ON_OFF,
    coded_out,
    m1_address,
    m2_address,
    stconv_n,
    uncoded_out,
    // Inouts
    MP_DATA
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         DIO1_1;
input         DIO1_2;
input  [11:0] FP_ADC_D;
input         FP_ADC_NEOC;
input         FP_GPS1_PPS;
input         FP_GPS1_Rx;
input         FP_GPS2_PPS;
input         FP_GPS2_Rx;
input         FP_MTR1_mon1;
input         FP_MTR1_mon2;
input         FP_MTR2_mon1;
input         FP_MTR2_mon2;
input         FP_MTR3_mon1;
input         FP_MTR3_mon2;
input         FP_MUXSEL_IMU2;
input         FP_PL_RXD;
input         FP_PL_STATUS;
input         FP_RW1_Rx;
input         FP_RW2_Rx;
input         FP_RW3_Rx;
input         FP_RW4_Rx;
input         FP_WDOG;
input         LT_EP1_BRDY;
input         LT_EP2_BRDY;
input         LT_FP_READ;
input  [15:0] MP_addr_0;
input         MP_ios_n;
input         MP_read;
input         MP_write;
input         MUX1_OUT;
input         MUX2_OUT;
input         MUX3_OUT;
input         MUX4_OUT;
input         RX_TC_CLK;
input         RX_TC_DATA;
input         SA1_deploy_ST;
input         SA2_deploy_ST;
input         SPI_DOUT_1;
input         SPI_DOUT_2;
input         TC_MUXSEL;
input         TS_SPI_DIO1_3;
input         TS_SPI_DOUT_3;
input         TS_TC_CLK;
input         TS_TC_DATA;
input         VOBC_TM;
input         clk;
input         clock_10KHz;
input         reset;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        ANT_DEPLOY;
output        ANT_DEPLOY_SDA;
output        CS_n;
output        FP_MTR1_N1;
output        FP_MTR1_N2;
output        FP_MTR1_P1;
output        FP_MTR1_P2;
output        FP_MTR2_N1;
output        FP_MTR2_N2;
output        FP_MTR2_P1;
output        FP_MTR2_P2;
output        FP_MTR3_N1;
output        FP_MTR3_N2;
output        FP_MTR3_P1;
output        FP_MTR3_P2;
output        FP_NMI;
output        FP_PL_EN;
output        FP_PL_TXD;
output        FP_RW1_TX;
output        FP_RW2_TX;
output        FP_RW3_TX;
output        FP_RW4_TX;
output        GC_CLK;
output        GPS1_ON_OFF;
output        GPS1_Reset;
output        GPS1_TX;
output        GPS2_ON_OFF;
output        GPS2_Reset;
output        Heater1_ON_OFF;
output        Heater2_ON_OFF;
output        Heater3_ON_OFF;
output        Heater4_ON_OFF;
output        Heater5_ON_OFF;
output        Heater6_ON_OFF;
output        IMU1_ON_OFF;
output        IMU1_Reset;
output        IMU2_ON_OFF;
output        IMU2_Reset;
output        MP_Reset;
output        MTR_ON_OFF;
output        Payload_ON_OFF;
output        RD_n;
output        RF_Tx_ON_OFF;
output        RW1_ON_OFF;
output        RW2_ON_OFF;
output        RW3_ON_OFF;
output        RW4_ON_OFF;
output        SA1_DEPLOY;
output        SA2_DEPLOY;
output        SEL_0;
output        SEL_1;
output        SEL_2;
output        SENSOR1_ON_OFF;
output        SENSOR2_ON_OFF;
output        SENSOR3_ON_OFF;
output        SENSOR4_ON_OFF;
output        SENSOR5_ON_OFF;
output        SENSOR6_ON_OFF;
output        SPARE1_ON_OFF;
output        SPARE2_ON_OFF;
output        SPI_DIN_1;
output        SPI_DIN_2;
output        SPI_NCS_1;
output        SPI_NCS_2;
output        SPI_SCLK_1;
output        SPI_SCLK_2;
output        TM_DS_EN;
output        TS_SPI_DIN_3;
output        TS_SPI_NCS;
output        TS_SPI_SCLK;
output        X_Tx_ON_OFF;
output        coded_out;
output [3:0]  m1_address;
output [4:0]  m2_address;
output        stconv_n;
output        uncoded_out;
//--------------------------------------------------------------------
// Inout
//--------------------------------------------------------------------
inout  [15:0] MP_DATA;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire   [15:0] ADC_TopLevel_0_MP_data_out;
wire          ANT_DEPLOY_net_0;
wire          ANT_DEPLOY_SDA_net_0;
wire          BUFF_0_Y;
wire          BUFF_2_Y_1;
wire          clk;
wire          clk_div_2us_0_clk_2us;
wire          clock_10KHz;
wire          coded_out_net_0;
wire          CS_n_net_0;
wire          DIO1_1;
wire          DIO1_2;
wire   [11:0] FP_ADC_D;
wire          FP_ADC_NEOC;
wire          FP_GPS1_Rx;
wire          FP_GPS2_PPS;
wire          FP_GPS2_Rx;
wire          FP_MTR1_mon1;
wire          FP_MTR1_mon2;
wire          FP_MTR1_N1_net_0;
wire          FP_MTR1_N2_net_0;
wire          FP_MTR1_P1_net_0;
wire          FP_MTR1_P2_net_0;
wire          FP_MTR2_mon1;
wire          FP_MTR2_mon2;
wire          FP_MTR2_N1_net_0;
wire          FP_MTR2_N2_net_0;
wire          FP_MTR2_P1_net_0;
wire          FP_MTR2_P2_net_0;
wire          FP_MTR3_mon1;
wire          FP_MTR3_mon2;
wire          FP_MTR3_N1_net_0;
wire          FP_MTR3_N2_net_0;
wire          FP_MTR3_P1_net_0;
wire          FP_MTR3_P2_net_0;
wire          FP_MUXSEL_IMU2;
wire          FP_NMI_net_0;
wire          FP_PL_EN_net_0;
wire          FP_PL_RXD;
wire          FP_PL_STATUS;
wire          FP_PL_TXD_net_0;
wire          FP_RW1_Rx;
wire          FP_RW1_TX_net_0;
wire          FP_RW2_Rx;
wire          FP_RW2_TX_net_0;
wire          FP_RW3_Rx;
wire          FP_RW3_TX_net_0;
wire          FP_RW4_Rx;
wire          FP_RW4_TX_net_0;
wire          FP_WDOG;
wire          GC_CLK_net_0;
wire          GPS1_ON_OFF_net_0;
wire          GPS1_Reset_net_0;
wire          GPS1_TX_net_0;
wire          GPS2_ON_OFF_net_0;
wire          GPS2_Reset_net_0;
wire   [15:0] GPS_Top_0_MP_data_out;
wire          Heater1_ON_OFF_net_0;
wire          Heater2_ON_OFF_net_0;
wire          Heater3_ON_OFF_net_0;
wire          Heater4_ON_OFF_net_0;
wire          Heater5_ON_OFF_net_0;
wire          Heater6_ON_OFF_net_0;
wire          IMU1_ON_OFF_net_0;
wire          IMU1_Reset_net_0;
wire          IMU2_ON_OFF_net_0;
wire          IMU2_Reset_net_0;
wire          IO_Test_TopLevel_0_adc_cs;
wire          IO_Test_TopLevel_0_gps1_cs;
wire          IO_Test_TopLevel_0_MAJ_RST;
wire          IO_Test_TopLevel_0_MIN_RST;
wire   [15:0] IO_Test_TopLevel_0_MP_data;
wire   [15:0] IO_Test_TopLevel_0_MP_data_0;
wire   [15:0] IO_Test_TopLevel_0_MP_data_1;
wire   [15:0] IO_Test_TopLevel_0_MP_data_2;
wire          IO_Test_TopLevel_0_telecommand_cs;
wire          IO_Test_TopLevel_0_telemetry_cs;
wire          LT_EP1_BRDY;
wire          LT_EP2_BRDY;
wire          LT_FP_READ;
wire   [3:0]  m1_address_net_0;
wire   [4:0]  m2_address_net_0;
wire          maj_TC;
wire          Min_cycle_TC_0_clk_16ms;
wire          min_TC;
wire   [15:9] MP_addr_0_slice_0;
wire   [8:0]  MP_addr_0_slice_1;
wire   [15:0] MP_DATA;
wire   [15:0] MP_data_buffer_0_MP_dataout;
wire   [15:0] MP_data_buffer_1_MP_dataout;
wire   [15:0] MP_data_buffer_2_MP_dataout;
wire          MP_ios_n;
wire          MP_read;
wire          MP_Reset_net_0;
wire          ms_clk_0_clk_1ms;
wire          MTR_ON_OFF_net_0;
wire          MUX1_OUT;
wire          MUX2_OUT;
wire          MUX3_OUT;
wire          MUX4_OUT;
wire   [15:0] mux_input_0_mp_data_out;
wire          Payload_ON_OFF_net_0;
wire          RD_n_net_0;
wire   [15:0] receiver_0_data_16;
wire          reset;
wire          RF_Tx_ON_OFF_net_0;
wire          RW1_ON_OFF_net_0;
wire          MP_write;
wire          RW3_ON_OFF_net_0;
wire          RW4_ON_OFF_net_0;
wire          RX_TC_CLK;
wire          RX_TC_DATA;
wire          SA1_DEPLOY_net_0;
wire          SA1_deploy_ST;
wire          SA2_DEPLOY_net_0;
wire          SA2_deploy_ST;
wire          SEL_0_net_0;
wire          SEL_1_net_0;
wire          SEL_2_net_0;
wire          SENSOR1_ON_OFF_net_0;
wire          SENSOR2_ON_OFF_net_0;
wire          SENSOR3_ON_OFF_net_0;
wire          SENSOR4_ON_OFF_net_0;
wire          SENSOR5_ON_OFF_net_0;
wire          SENSOR6_ON_OFF_net_0;
wire          FP_GPS1_PPS;
wire          SPARE2_ON_OFF_net_0;
wire          SPI_DIN_1_net_0;
wire          SPI_DIN_2_net_0;
wire          SPI_DOUT_1;
wire          SPI_DOUT_2;
wire          SPI_NCS_1_net_0;
wire          SPI_NCS_2_net_0;
wire          SPI_SCLK_1_net_0;
wire          SPI_SCLK_2_net_0;
wire          stconv_n_net_0;
wire          TC_MUXSEL;
wire   [15:0] Telemetry_Top_0_MP_dataout;
wire          TM_DS_EN_net_0;
wire   [15:0] Transreceiver_0_MP_data_OPlatch;
wire          TS_SPI_DIN_3_net_0;
wire          TS_SPI_DIO1_3;
wire          TS_SPI_DOUT_3;
wire          TS_SPI_NCS_net_0;
wire          TS_SPI_SCLK_net_0;
wire          TS_TC_CLK;
wire          TS_TC_DATA;
wire          uncoded_out_net_0;
wire          VOBC_TM;
wire          X_Tx_ON_OFF_net_0;
wire          Heater6_ON_OFF_net_1;
wire          SENSOR6_ON_OFF_net_1;
wire          SENSOR3_ON_OFF_net_1;
wire          FP_RW2_TX_net_1;
wire          FP_RW4_TX_net_1;
wire          SENSOR5_ON_OFF_net_1;
wire          Heater1_ON_OFF_net_1;
wire          TS_SPI_DIN_3_net_1;
wire          SPI_SCLK_2_net_1;
wire          SEL_2_net_1;
wire          SPI_SCLK_1_net_1;
wire          SPI_NCS_2_net_1;
wire          FP_PL_TXD_net_1;
wire          SENSOR1_ON_OFF_net_1;
wire          SEL_0_net_1;
wire          SENSOR2_ON_OFF_net_1;
wire          GC_CLK_net_1;
wire          SPI_NCS_1_net_1;
wire          ANT_DEPLOY_SDA_net_1;
wire          SPI_DIN_2_net_1;
wire          SPI_DIN_1_net_1;
wire          SENSOR4_ON_OFF_net_1;
wire          SEL_1_net_1;
wire          FP_MTR1_N1_net_1;
wire          FP_MTR3_P1_net_1;
wire          X_Tx_ON_OFF_net_1;
wire          IMU1_Reset_net_1;
wire          FP_MTR3_N2_net_1;
wire          FP_MTR3_N1_net_1;
wire          FP_MTR1_P1_net_1;
wire          FP_MTR3_P2_net_1;
wire          Heater2_ON_OFF_net_1;
wire          ANT_DEPLOY_net_1;
wire          RF_Tx_ON_OFF_net_1;
wire          FP_MTR2_P1_net_1;
wire          SA2_DEPLOY_net_1;
wire          FP_MTR2_N2_net_1;
wire          FP_MTR1_N2_net_1;
wire          Heater5_ON_OFF_net_1;
wire          MTR_ON_OFF_net_1;
wire          FP_MTR2_P2_net_1;
wire          MP_Reset_net_1;
wire          GPS2_ON_OFF_net_1;
wire          GPS1_Reset_net_1;
wire          RW2_ON_OFF_net_0;
wire          IMU2_Reset_net_1;
wire          FP_MTR1_P2_net_1;
wire          Heater4_ON_OFF_net_1;
wire          FP_MTR2_N1_net_1;
wire          Payload_ON_OFF_net_1;
wire          IMU2_ON_OFF_net_1;
wire          SPARE2_ON_OFF_net_1;
wire          GPS1_ON_OFF_net_1;
wire          IMU1_ON_OFF_net_1;
wire          FP_NMI_net_1;
wire          Heater3_ON_OFF_net_1;
wire          GPS2_Reset_net_1;
wire          RW3_ON_OFF_net_1;
wire          RW1_ON_OFF_net_1;
wire          SPARE1_ON_OFF_net_0;
wire          FP_RW3_TX_net_1;
wire          TM_DS_EN_net_1;
wire          FP_PL_EN_net_1;
wire          RW4_ON_OFF_net_1;
wire          FP_RW1_TX_net_1;
wire          TS_SPI_SCLK_net_1;
wire          TS_SPI_NCS_net_1;
wire          SA1_DEPLOY_net_1;
wire          stconv_n_net_1;
wire          CS_n_net_1;
wire          RD_n_net_1;
wire          GPS1_TX_net_1;
wire          coded_out_net_1;
wire          uncoded_out_net_1;
wire   [3:0]  m1_address_net_1;
wire   [4:0]  m2_address_net_1;
wire   [15:0] MP_addr_0;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire          GND_net;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign GND_net = 1'b0;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign Heater6_ON_OFF_net_1 = Heater6_ON_OFF_net_0;
assign Heater6_ON_OFF       = Heater6_ON_OFF_net_1;
assign SENSOR6_ON_OFF_net_1 = SENSOR6_ON_OFF_net_0;
assign SENSOR6_ON_OFF       = SENSOR6_ON_OFF_net_1;
assign SENSOR3_ON_OFF_net_1 = SENSOR3_ON_OFF_net_0;
assign SENSOR3_ON_OFF       = SENSOR3_ON_OFF_net_1;
assign FP_RW2_TX_net_1      = FP_RW2_TX_net_0;
assign FP_RW2_TX            = FP_RW2_TX_net_1;
assign FP_RW4_TX_net_1      = FP_RW4_TX_net_0;
assign FP_RW4_TX            = FP_RW4_TX_net_1;
assign SENSOR5_ON_OFF_net_1 = SENSOR5_ON_OFF_net_0;
assign SENSOR5_ON_OFF       = SENSOR5_ON_OFF_net_1;
assign Heater1_ON_OFF_net_1 = Heater1_ON_OFF_net_0;
assign Heater1_ON_OFF       = Heater1_ON_OFF_net_1;
assign TS_SPI_DIN_3_net_1   = TS_SPI_DIN_3_net_0;
assign TS_SPI_DIN_3         = TS_SPI_DIN_3_net_1;
assign SPI_SCLK_2_net_1     = SPI_SCLK_2_net_0;
assign SPI_SCLK_2           = SPI_SCLK_2_net_1;
assign SEL_2_net_1          = SEL_2_net_0;
assign SEL_2                = SEL_2_net_1;
assign SPI_SCLK_1_net_1     = SPI_SCLK_1_net_0;
assign SPI_SCLK_1           = SPI_SCLK_1_net_1;
assign SPI_NCS_2_net_1      = SPI_NCS_2_net_0;
assign SPI_NCS_2            = SPI_NCS_2_net_1;
assign FP_PL_TXD_net_1      = FP_PL_TXD_net_0;
assign FP_PL_TXD            = FP_PL_TXD_net_1;
assign SENSOR1_ON_OFF_net_1 = SENSOR1_ON_OFF_net_0;
assign SENSOR1_ON_OFF       = SENSOR1_ON_OFF_net_1;
assign SEL_0_net_1          = SEL_0_net_0;
assign SEL_0                = SEL_0_net_1;
assign SENSOR2_ON_OFF_net_1 = SENSOR2_ON_OFF_net_0;
assign SENSOR2_ON_OFF       = SENSOR2_ON_OFF_net_1;
assign GC_CLK_net_1         = GC_CLK_net_0;
assign GC_CLK               = GC_CLK_net_1;
assign SPI_NCS_1_net_1      = SPI_NCS_1_net_0;
assign SPI_NCS_1            = SPI_NCS_1_net_1;
assign ANT_DEPLOY_SDA_net_1 = ANT_DEPLOY_SDA_net_0;
assign ANT_DEPLOY_SDA       = ANT_DEPLOY_SDA_net_1;
assign SPI_DIN_2_net_1      = SPI_DIN_2_net_0;
assign SPI_DIN_2            = SPI_DIN_2_net_1;
assign SPI_DIN_1_net_1      = SPI_DIN_1_net_0;
assign SPI_DIN_1            = SPI_DIN_1_net_1;
assign SENSOR4_ON_OFF_net_1 = SENSOR4_ON_OFF_net_0;
assign SENSOR4_ON_OFF       = SENSOR4_ON_OFF_net_1;
assign SEL_1_net_1          = SEL_1_net_0;
assign SEL_1                = SEL_1_net_1;
assign FP_MTR1_N1_net_1     = FP_MTR1_N1_net_0;
assign FP_MTR1_N1           = FP_MTR1_N1_net_1;
assign FP_MTR3_P1_net_1     = FP_MTR3_P1_net_0;
assign FP_MTR3_P1           = FP_MTR3_P1_net_1;
assign X_Tx_ON_OFF_net_1    = X_Tx_ON_OFF_net_0;
assign X_Tx_ON_OFF          = X_Tx_ON_OFF_net_1;
assign IMU1_Reset_net_1     = IMU1_Reset_net_0;
assign IMU1_Reset           = IMU1_Reset_net_1;
assign FP_MTR3_N2_net_1     = FP_MTR3_N2_net_0;
assign FP_MTR3_N2           = FP_MTR3_N2_net_1;
assign FP_MTR3_N1_net_1     = FP_MTR3_N1_net_0;
assign FP_MTR3_N1           = FP_MTR3_N1_net_1;
assign FP_MTR1_P1_net_1     = FP_MTR1_P1_net_0;
assign FP_MTR1_P1           = FP_MTR1_P1_net_1;
assign FP_MTR3_P2_net_1     = FP_MTR3_P2_net_0;
assign FP_MTR3_P2           = FP_MTR3_P2_net_1;
assign Heater2_ON_OFF_net_1 = Heater2_ON_OFF_net_0;
assign Heater2_ON_OFF       = Heater2_ON_OFF_net_1;
assign ANT_DEPLOY_net_1     = ANT_DEPLOY_net_0;
assign ANT_DEPLOY           = ANT_DEPLOY_net_1;
assign RF_Tx_ON_OFF_net_1   = RF_Tx_ON_OFF_net_0;
assign RF_Tx_ON_OFF         = RF_Tx_ON_OFF_net_1;
assign FP_MTR2_P1_net_1     = FP_MTR2_P1_net_0;
assign FP_MTR2_P1           = FP_MTR2_P1_net_1;
assign SA2_DEPLOY_net_1     = SA2_DEPLOY_net_0;
assign SA2_DEPLOY           = SA2_DEPLOY_net_1;
assign FP_MTR2_N2_net_1     = FP_MTR2_N2_net_0;
assign FP_MTR2_N2           = FP_MTR2_N2_net_1;
assign FP_MTR1_N2_net_1     = FP_MTR1_N2_net_0;
assign FP_MTR1_N2           = FP_MTR1_N2_net_1;
assign Heater5_ON_OFF_net_1 = Heater5_ON_OFF_net_0;
assign Heater5_ON_OFF       = Heater5_ON_OFF_net_1;
assign MTR_ON_OFF_net_1     = MTR_ON_OFF_net_0;
assign MTR_ON_OFF           = MTR_ON_OFF_net_1;
assign FP_MTR2_P2_net_1     = FP_MTR2_P2_net_0;
assign FP_MTR2_P2           = FP_MTR2_P2_net_1;
assign MP_Reset_net_1       = MP_Reset_net_0;
assign MP_Reset             = MP_Reset_net_1;
assign GPS2_ON_OFF_net_1    = GPS2_ON_OFF_net_0;
assign GPS2_ON_OFF          = GPS2_ON_OFF_net_1;
assign GPS1_Reset_net_1     = GPS1_Reset_net_0;
assign GPS1_Reset           = GPS1_Reset_net_1;
assign RW2_ON_OFF_net_0     = MP_write;
assign RW2_ON_OFF           = RW2_ON_OFF_net_0;
assign IMU2_Reset_net_1     = IMU2_Reset_net_0;
assign IMU2_Reset           = IMU2_Reset_net_1;
assign FP_MTR1_P2_net_1     = FP_MTR1_P2_net_0;
assign FP_MTR1_P2           = FP_MTR1_P2_net_1;
assign Heater4_ON_OFF_net_1 = Heater4_ON_OFF_net_0;
assign Heater4_ON_OFF       = Heater4_ON_OFF_net_1;
assign FP_MTR2_N1_net_1     = FP_MTR2_N1_net_0;
assign FP_MTR2_N1           = FP_MTR2_N1_net_1;
assign Payload_ON_OFF_net_1 = Payload_ON_OFF_net_0;
assign Payload_ON_OFF       = Payload_ON_OFF_net_1;
assign IMU2_ON_OFF_net_1    = IMU2_ON_OFF_net_0;
assign IMU2_ON_OFF          = IMU2_ON_OFF_net_1;
assign SPARE2_ON_OFF_net_1  = SPARE2_ON_OFF_net_0;
assign SPARE2_ON_OFF        = SPARE2_ON_OFF_net_1;
assign GPS1_ON_OFF_net_1    = GPS1_ON_OFF_net_0;
assign GPS1_ON_OFF          = GPS1_ON_OFF_net_1;
assign IMU1_ON_OFF_net_1    = IMU1_ON_OFF_net_0;
assign IMU1_ON_OFF          = IMU1_ON_OFF_net_1;
assign FP_NMI_net_1         = FP_NMI_net_0;
assign FP_NMI               = FP_NMI_net_1;
assign Heater3_ON_OFF_net_1 = Heater3_ON_OFF_net_0;
assign Heater3_ON_OFF       = Heater3_ON_OFF_net_1;
assign GPS2_Reset_net_1     = GPS2_Reset_net_0;
assign GPS2_Reset           = GPS2_Reset_net_1;
assign RW3_ON_OFF_net_1     = RW3_ON_OFF_net_0;
assign RW3_ON_OFF           = RW3_ON_OFF_net_1;
assign RW1_ON_OFF_net_1     = RW1_ON_OFF_net_0;
assign RW1_ON_OFF           = RW1_ON_OFF_net_1;
assign SPARE1_ON_OFF_net_0  = FP_GPS1_PPS;
assign SPARE1_ON_OFF        = SPARE1_ON_OFF_net_0;
assign FP_RW3_TX_net_1      = FP_RW3_TX_net_0;
assign FP_RW3_TX            = FP_RW3_TX_net_1;
assign TM_DS_EN_net_1       = TM_DS_EN_net_0;
assign TM_DS_EN             = TM_DS_EN_net_1;
assign FP_PL_EN_net_1       = FP_PL_EN_net_0;
assign FP_PL_EN             = FP_PL_EN_net_1;
assign RW4_ON_OFF_net_1     = RW4_ON_OFF_net_0;
assign RW4_ON_OFF           = RW4_ON_OFF_net_1;
assign FP_RW1_TX_net_1      = FP_RW1_TX_net_0;
assign FP_RW1_TX            = FP_RW1_TX_net_1;
assign TS_SPI_SCLK_net_1    = TS_SPI_SCLK_net_0;
assign TS_SPI_SCLK          = TS_SPI_SCLK_net_1;
assign TS_SPI_NCS_net_1     = TS_SPI_NCS_net_0;
assign TS_SPI_NCS           = TS_SPI_NCS_net_1;
assign SA1_DEPLOY_net_1     = SA1_DEPLOY_net_0;
assign SA1_DEPLOY           = SA1_DEPLOY_net_1;
assign stconv_n_net_1       = stconv_n_net_0;
assign stconv_n             = stconv_n_net_1;
assign CS_n_net_1           = CS_n_net_0;
assign CS_n                 = CS_n_net_1;
assign RD_n_net_1           = RD_n_net_0;
assign RD_n                 = RD_n_net_1;
assign GPS1_TX_net_1        = GPS1_TX_net_0;
assign GPS1_TX              = GPS1_TX_net_1;
assign coded_out_net_1      = coded_out_net_0;
assign coded_out            = coded_out_net_1;
assign uncoded_out_net_1    = uncoded_out_net_0;
assign uncoded_out          = uncoded_out_net_1;
assign m1_address_net_1     = m1_address_net_0;
assign m1_address[3:0]      = m1_address_net_1;
assign m2_address_net_1     = m2_address_net_0;
assign m2_address[4:0]      = m2_address_net_1;
//--------------------------------------------------------------------
// Slices assignments
//--------------------------------------------------------------------
assign MP_addr_0_slice_0 = MP_addr_0[15:9];
assign MP_addr_0_slice_1 = MP_addr_0[8:0];
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------ADC_TopLevel
ADC_TopLevel ADC_TopLevel_0(
        // Inputs
        .reset       ( BUFF_2_Y_1 ),
        .clk         ( clk ),
        .EOC_n       ( FP_ADC_NEOC ),
        .MP_write    ( MP_write ),
        .MP_re       ( MP_read ),
        .ADC_CS_n    ( IO_Test_TopLevel_0_adc_cs ),
        .ADC_data_in ( FP_ADC_D ),
        .MP_addr_0   ( MP_addr_0_slice_1 ),
        .MP_data_in  ( MP_data_buffer_1_MP_dataout ),
        // Outputs
        .stconv_n    ( stconv_n_net_0 ),
        .RD_n        ( RD_n_net_0 ),
        .CS_n        ( CS_n_net_0 ),
        .m2_address  ( m2_address_net_0 ),
        .m1_address  ( m1_address_net_0 ),
        .MP_data_out ( ADC_TopLevel_0_MP_data_out ) 
        );

//--------BUFF
BUFF BUFF_0(
        // Inputs
        .A ( RX_TC_CLK ),
        // Outputs
        .Y ( BUFF_0_Y ) 
        );

//--------BUFF
BUFF BUFF_2(
        // Inputs
        .A ( reset ),
        // Outputs
        .Y ( BUFF_2_Y_1 ) 
        );

//--------clk_div_2us
clk_div_2us clk_div_2us_0(
        // Inputs
        .clk     ( clk ),
        .reset   ( BUFF_2_Y_1 ),
        // Outputs
        .clk_2us ( clk_div_2us_0_clk_2us ) 
        );

//--------GPS_Top
GPS_Top GPS_Top_0(
        // Inputs
        .reset       ( BUFF_2_Y_1 ),
        .clk         ( clk ),
        .GPS_CS_n    ( IO_Test_TopLevel_0_gps1_cs ),
        .MP_we       ( MP_write ),
        .MP_re       ( MP_read ),
        .vpp         ( FP_GPS1_PPS ),
        .RX          ( FP_GPS1_Rx ),
        .clk_2us     ( clk_div_2us_0_clk_2us ),
        .MP_data_in  ( MP_data_buffer_0_MP_dataout ),
        .MP_addr     ( MP_addr_0_slice_1 ),
        // Outputs
        .TX          ( GPS1_TX_net_0 ),
        .MP_bit5     (  ),
        .MP_data_out ( GPS_Top_0_MP_data_out ) 
        );

//--------IO_Test_TopLevel
IO_Test_TopLevel IO_Test_TopLevel_0(
        // Inputs
        .MP_ios_n       ( MP_ios_n ),
        .reset          ( BUFF_2_Y_1 ),
        .clk            ( clk ),
        .MP_write       ( MP_write ),
        .MP_read        ( MP_read ),
        .FP_MTR1_mon2   ( FP_MTR1_mon2 ),
        .DIO1_1         ( DIO1_1 ),
        .MUX3_OUT       ( MUX3_OUT ),
        .TS_SPI_DOUT_3  ( TS_SPI_DOUT_3 ),
        .FP_MTR2_mon1   ( FP_MTR2_mon1 ),
        .FP_MTR3_mon1   ( FP_MTR3_mon1 ),
        .SPI_DOUT_2     ( SPI_DOUT_2 ),
        .FP_MTR1_mon1   ( FP_MTR1_mon1 ),
        .FP_MTR3_mon2   ( FP_MTR3_mon2 ),
        .DIO1_2         ( DIO1_2 ),
        .SPI_DOUT_1     ( SPI_DOUT_1 ),
        .MUX2_OUT       ( MUX2_OUT ),
        .VOBC_TM        ( VOBC_TM ),
        .TS_SPI_DIO1_3  ( TS_SPI_DIO1_3 ),
        .FP_MTR2_mon2   ( FP_MTR2_mon2 ),
        .MUX4_OUT       ( MUX4_OUT ),
        .FP_PL_RXD      ( FP_PL_RXD ),
        .FP_MUXSEL_IMU2 ( FP_MUXSEL_IMU2 ),
        .FP_PL_STATUS   ( FP_PL_STATUS ),
        .FP_ADC_NEOC    ( FP_ADC_NEOC ),
        .LT_EP2_BRDY    ( LT_EP2_BRDY ),
        .RX_TC_DATA     ( RX_TC_DATA ),
        .LT_FP_READ     ( LT_FP_READ ),
        .FP_RW1_Rx      ( FP_RW1_Rx ),
        .LT_EP1_BRDY    ( LT_EP1_BRDY ),
        .MUX1_OUT       ( MUX1_OUT ),
        .FP_WDOG        ( FP_WDOG ),
        .FP_GPS2_PPS    ( FP_GPS2_PPS ),
        .FP_GPS1_Rx     ( FP_GPS1_Rx ),
        .SA2_deploy_ST  ( SA2_deploy_ST ),
        .FP_RW3_Rx      ( FP_RW3_Rx ),
        .FP_RW4_Rx      ( FP_RW4_Rx ),
        .RX_TC_CLK      ( GND_net ),
        .SA1_deploy_ST  ( SA1_deploy_ST ),
        .TS_TC_DATA     ( TS_TC_DATA ),
        .TS_TC_CLK      ( TS_TC_CLK ),
        .TC_MUXSEL      ( TC_MUXSEL ),
        .FP_GPS2_Rx     ( FP_GPS2_Rx ),
        .FP_GPS1_PPS    ( FP_GPS1_PPS ),
        .FP_RW2_Rx      ( FP_RW2_Rx ),
        .FP_ADC_D       ( FP_ADC_D ),
        .MP_addr        ( MP_addr_0_slice_0 ),
        .MP_data_in     ( MP_data_buffer_2_MP_dataout ),
        .MIN_TC         ( min_TC ),
        .MAJ_TC         ( maj_TC ),
        // Outputs
        .SENSOR2_ON_OFF ( SENSOR2_ON_OFF_net_0 ),
        .SPI_NCS_2      ( SPI_NCS_2_net_0 ),
        .SENSOR5_ON_OFF ( SENSOR5_ON_OFF_net_0 ),
        .SPI_DIN_1      ( SPI_DIN_1_net_0 ),
        .SEL_0          ( SEL_0_net_0 ),
        .SPI_SCLK_1     ( SPI_SCLK_1_net_0 ),
        .SEL_1          ( SEL_1_net_0 ),
        .SENSOR3_ON_OFF ( SENSOR3_ON_OFF_net_0 ),
        .SENSOR4_ON_OFF ( SENSOR4_ON_OFF_net_0 ),
        .ANT_DEPLOY_SDA ( ANT_DEPLOY_SDA_net_0 ),
        .SPI_SCLK_2     ( SPI_SCLK_2_net_0 ),
        .SPI_DIN_2      ( SPI_DIN_2_net_0 ),
        .SENSOR1_ON_OFF ( SENSOR1_ON_OFF_net_0 ),
        .SEL_2          ( SEL_2_net_0 ),
        .SENSOR6_ON_OFF ( SENSOR6_ON_OFF_net_0 ),
        .SPI_NCS_1      ( SPI_NCS_1_net_0 ),
        .FP_PL_TXD      ( FP_PL_TXD_net_0 ),
        .GC_CLK         ( GC_CLK_net_0 ),
        .Heater1_ON_OFF (  ),
        .FP_RW4_TX      ( FP_RW4_TX_net_0 ),
        .Heater6_ON_OFF (  ),
        .FP_RW2_TX      ( FP_RW2_TX_net_0 ),
        .TS_SPI_DIN_3   ( TS_SPI_DIN_3_net_0 ),
        .SA1_DEPLOY     ( SA1_DEPLOY_net_0 ),
        .FP_RW3_TX      ( FP_RW3_TX_net_0 ),
        .FP_NMI         (  ),
        .RW1_ON_OFF     (  ),
        .TS_SPI_NCS     ( TS_SPI_NCS_net_0 ),
        .FP_RW1_TX      ( FP_RW1_TX_net_0 ),
        .TS_SPI_SCLK    ( TS_SPI_SCLK_net_0 ),
        .FP_PL_EN       ( FP_PL_EN_net_0 ),
        .RW4_ON_OFF     ( RW4_ON_OFF_net_0 ),
        .RW3_ON_OFF     (  ),
        .IMU1_ON_OFF    ( IMU1_ON_OFF_net_0 ),
        .TM_DS_EN       ( TM_DS_EN_net_0 ),
        .GPS2_Reset     ( GPS2_Reset_net_0 ),
        .Heater3_ON_OFF (  ),
        .SPARE1_ON_OFF  (  ),
        .GPS1_ON_OFF    ( GPS1_ON_OFF_net_0 ),
        .Heater5_ON_OFF (  ),
        .RW2_ON_OFF     (  ),
        .MTR_ON_OFF     ( MTR_ON_OFF_net_0 ),
        .Payload_ON_OFF ( Payload_ON_OFF_net_0 ),
        .ANT_DEPLOY     ( ANT_DEPLOY_net_0 ),
        .Heater2_ON_OFF (  ),
        .SA2_DEPLOY     ( SA2_DEPLOY_net_0 ),
        .Heater4_ON_OFF (  ),
        .IMU1_Reset     ( IMU1_Reset_net_0 ),
        .SPARE2_ON_OFF  ( SPARE2_ON_OFF_net_0 ),
        .IMU2_ON_OFF    ( IMU2_ON_OFF_net_0 ),
        .FP_MTR1_N2     ( FP_MTR1_N2_net_0 ),
        .FP_MTR3_N2     ( FP_MTR3_N2_net_0 ),
        .FP_MTR1_N1     ( FP_MTR1_N1_net_0 ),
        .FP_MTR1_P2     ( FP_MTR1_P2_net_0 ),
        .FP_MTR2_N1     ( FP_MTR2_N1_net_0 ),
        .FP_MTR3_P1     ( FP_MTR3_P1_net_0 ),
        .FP_MTR3_P2     ( FP_MTR3_P2_net_0 ),
        .FP_MTR1_P1     ( FP_MTR1_P1_net_0 ),
        .GPS1_Reset     ( GPS1_Reset_net_0 ),
        .GPS2_ON_OFF    ( GPS2_ON_OFF_net_0 ),
        .FP_MTR3_N1     ( FP_MTR3_N1_net_0 ),
        .RF_Tx_ON_OFF   ( RF_Tx_ON_OFF_net_0 ),
        .FP_MTR2_P2     ( FP_MTR2_P2_net_0 ),
        .IMU2_Reset     ( IMU2_Reset_net_0 ),
        .X_Tx_ON_OFF    ( X_Tx_ON_OFF_net_0 ),
        .FP_MTR2_N2     ( FP_MTR2_N2_net_0 ),
        .FP_MTR2_P1     ( FP_MTR2_P1_net_0 ),
        .MP_Reset       ( MP_Reset_net_0 ),
        .adc_cs         ( IO_Test_TopLevel_0_adc_cs ),
        .gps1_cs        ( IO_Test_TopLevel_0_gps1_cs ),
        .telecommand_cs ( IO_Test_TopLevel_0_telecommand_cs ),
        .telemetry_cs   ( IO_Test_TopLevel_0_telemetry_cs ),
        .MP_data        ( IO_Test_TopLevel_0_MP_data ),
        .MP_data_2      ( IO_Test_TopLevel_0_MP_data_2 ),
        .MP_data_1      ( IO_Test_TopLevel_0_MP_data_1 ),
        .MP_data_0      ( IO_Test_TopLevel_0_MP_data_0 ),
        .MAJ_RST        ( IO_Test_TopLevel_0_MAJ_RST ),
        .MIN_RST        ( IO_Test_TopLevel_0_MIN_RST ) 
        );

//--------Maj_Cycle_TC
Maj_Cycle_TC Maj_Cycle_TC_0(
        // Inputs
        .clk_16ms   ( Min_cycle_TC_0_clk_16ms ),
        .POR        ( BUFF_2_Y_1 ),
        .maj_TC_rst ( IO_Test_TopLevel_0_MAJ_RST ),
        // Outputs
        .maj_TC     ( maj_TC ),
        .FP_NMI     ( FP_NMI_net_0 ) 
        );

//--------Min_cycle_TC
Min_cycle_TC Min_cycle_TC_0(
        // Inputs
        .clk_1ms    ( ms_clk_0_clk_1ms ),
        .POR        ( BUFF_2_Y_1 ),
        .min_TC_rst ( IO_Test_TopLevel_0_MIN_RST ),
        // Outputs
        .min_TC     ( min_TC ),
        .clk_16ms   ( Min_cycle_TC_0_clk_16ms ) 
        );

//--------MP_data_buffer
MP_data_buffer MP_data_buffer_0(
        // Inputs
        .MP_datain  ( Transreceiver_0_MP_data_OPlatch ),
        // Outputs
        .MP_dataout ( MP_data_buffer_0_MP_dataout ) 
        );

//--------MP_data_buffer
MP_data_buffer MP_data_buffer_1(
        // Inputs
        .MP_datain  ( Transreceiver_0_MP_data_OPlatch ),
        // Outputs
        .MP_dataout ( MP_data_buffer_1_MP_dataout ) 
        );

//--------MP_data_buffer
MP_data_buffer MP_data_buffer_2(
        // Inputs
        .MP_datain  ( MP_data_buffer_1_MP_dataout ),
        // Outputs
        .MP_dataout ( MP_data_buffer_2_MP_dataout ) 
        );

//--------ms_clk
ms_clk ms_clk_0(
        // Inputs
        .clk_2us ( clk_div_2us_0_clk_2us ),
        .reset   ( BUFF_2_Y_1 ),
        // Outputs
        .clk_1ms ( ms_clk_0_clk_1ms ) 
        );

//--------mux_input
mux_input mux_input_0(
        // Inputs
        .mp_read     ( MP_read ),
        .mp_iosn     ( MP_ios_n ),
        .mp_addr     ( MP_addr_0_slice_0 ),
        .data1       ( IO_Test_TopLevel_0_MP_data ),
        .data2       ( IO_Test_TopLevel_0_MP_data_0 ),
        .data3       ( IO_Test_TopLevel_0_MP_data_1 ),
        .data4       ( IO_Test_TopLevel_0_MP_data_2 ),
        .data_ADC    ( ADC_TopLevel_0_MP_data_out ),
        .data_GPS    ( GPS_Top_0_MP_data_out ),
        .data_TC     ( receiver_0_data_16 ),
        .data_TM     ( Telemetry_Top_0_MP_dataout ),
        // Outputs
        .mp_data_out ( mux_input_0_mp_data_out ) 
        );

//--------receiver
receiver receiver_0(
        // Inputs
        .clock3125       ( BUFF_0_Y ),
        .clock40         ( clk ),
        .master_reset    ( BUFF_2_Y_1 ),
        .CS              ( IO_Test_TopLevel_0_telecommand_cs ),
        .data            ( RX_TC_DATA ),
        .RD              ( MP_read ),
        .WR              ( MP_write ),
        .MP_data         ( Transreceiver_0_MP_data_OPlatch ),
        .MP_ADD          ( MP_addr_0_slice_1 ),
        // Outputs
        .randomizer_out  (  ),
        .BCH_data        ( Heater1_ON_OFF_net_0 ),
        .randomizer_data (  ),
        .BCH_enbl        ( Heater4_ON_OFF_net_0 ),
        .randomizer_enbl ( Heater5_ON_OFF_net_0 ),
        .AP              ( Heater3_ON_OFF_net_0 ),
        .busy            ( Heater2_ON_OFF_net_0 ),
        .SPC_RD          ( RW1_ON_OFF_net_0 ),
        .SR_RD           ( Heater6_ON_OFF_net_0 ),
        .SR_WR           ( RW3_ON_OFF_net_0 ),
        .data_16         ( receiver_0_data_16 ),
        .status          (  ),
        .SPC_data        (  ) 
        );

//--------Telemetry_Top
Telemetry_Top Telemetry_Top_0(
        // Inputs
        .CS           ( IO_Test_TopLevel_0_telemetry_cs ),
        .clock_10KHz  ( clock_10KHz ),
        .master_reset ( BUFF_2_Y_1 ),
        .RD           ( MP_read ),
        .WR           ( MP_write ),
        .clock_40M    ( clk ),
        .ADD_0        ( MP_addr_0_slice_1 ),
        .MP_datain    ( Transreceiver_0_MP_data_OPlatch ),
        // Outputs
        .conv_out1    (  ),
        .main_out     ( coded_out_net_0 ),
        .conv_out     (  ),
        .uncoded_out  ( uncoded_out_net_0 ),
        .conv_out2    (  ),
        .ip_data      (  ),
        .encoder_regs (  ),
        .status       (  ),
        .MP_dataout   ( Telemetry_Top_0_MP_dataout ) 
        );

//--------Transreceiver
Transreceiver Transreceiver_0(
        // Inputs
        .IOS_n           ( MP_ios_n ),
        .MP_OE_n         ( MP_read ),
        .MP_write_n      ( MP_write ),
        .MP_data_IPlatch ( mux_input_0_mp_data_out ),
        // Outputs
        .MP_data_OPlatch ( Transreceiver_0_MP_data_OPlatch ),
        // Inouts
        .MPdata_in       ( MP_DATA ) 
        );


endmodule
