//////////////////////////////////////////////////////////////////////
// Created by SmartDesign Mon Feb 26 11:27:17 2018
// Version: v11.8 11.8.0.26
//////////////////////////////////////////////////////////////////////

`timescale 1ns / 100ps

// IO_Test_TopLevel
module IO_Test_TopLevel(
    // Inputs
    DIO1_1,
    DIO1_2,
    FP_ADC_D,
    FP_ADC_NEOC,
    FP_GPS1_PPS,
    FP_GPS1_Rx,
    FP_GPS2_PPS,
    FP_GPS2_Rx,
    FP_MTR1_mon1,
    FP_MTR1_mon2,
    FP_MTR2_mon1,
    FP_MTR2_mon2,
    FP_MTR3_mon1,
    FP_MTR3_mon2,
    FP_MUXSEL_IMU2,
    FP_PL_RXD,
    FP_PL_STATUS,
    FP_RW1_Rx,
    FP_RW2_Rx,
    FP_RW3_Rx,
    FP_RW4_Rx,
    FP_WDOG,
    LT_EP1_BRDY,
    LT_EP2_BRDY,
    LT_FP_READ,
    MAJ_TC,
    MIN_TC,
    MP_addr,
    MP_data_in,
    MP_ios_n,
    MP_read,
    MP_write,
    MUX1_OUT,
    MUX2_OUT,
    MUX3_OUT,
    MUX4_OUT,
    RX_TC_CLK,
    RX_TC_DATA,
    SA1_deploy_ST,
    SA2_deploy_ST,
    SPI_DOUT_1,
    SPI_DOUT_2,
    TC_MUXSEL,
    TS_SPI_DIO1_3,
    TS_SPI_DOUT_3,
    TS_TC_CLK,
    TS_TC_DATA,
    VOBC_TM,
    clk,
    reset,
    // Outputs
    ANT_DEPLOY,
    ANT_DEPLOY_SDA,
    FP_MTR1_N1,
    FP_MTR1_N2,
    FP_MTR1_P1,
    FP_MTR1_P2,
    FP_MTR2_N1,
    FP_MTR2_N2,
    FP_MTR2_P1,
    FP_MTR2_P2,
    FP_MTR3_N1,
    FP_MTR3_N2,
    FP_MTR3_P1,
    FP_MTR3_P2,
    FP_NMI,
    FP_PL_EN,
    FP_PL_TXD,
    FP_RW1_TX,
    FP_RW2_TX,
    FP_RW3_TX,
    FP_RW4_TX,
    GC_CLK,
    GPS1_ON_OFF,
    GPS1_Reset,
    GPS2_ON_OFF,
    GPS2_Reset,
    Heater1_ON_OFF,
    Heater2_ON_OFF,
    Heater3_ON_OFF,
    Heater4_ON_OFF,
    Heater5_ON_OFF,
    Heater6_ON_OFF,
    IMU1_ON_OFF,
    IMU1_Reset,
    IMU2_ON_OFF,
    IMU2_Reset,
    MAJ_RST,
    MIN_RST,
    MP_Reset,
    MP_data,
    MP_data_0,
    MP_data_1,
    MP_data_2,
    MTR_ON_OFF,
    Payload_ON_OFF,
    RF_Tx_ON_OFF,
    RW1_ON_OFF,
    RW2_ON_OFF,
    RW3_ON_OFF,
    RW4_ON_OFF,
    SA1_DEPLOY,
    SA2_DEPLOY,
    SEL_0,
    SEL_1,
    SEL_2,
    SENSOR1_ON_OFF,
    SENSOR2_ON_OFF,
    SENSOR3_ON_OFF,
    SENSOR4_ON_OFF,
    SENSOR5_ON_OFF,
    SENSOR6_ON_OFF,
    SPARE1_ON_OFF,
    SPARE2_ON_OFF,
    SPI_DIN_1,
    SPI_DIN_2,
    SPI_NCS_1,
    SPI_NCS_2,
    SPI_SCLK_1,
    SPI_SCLK_2,
    TM_DS_EN,
    TS_SPI_DIN_3,
    TS_SPI_NCS,
    TS_SPI_SCLK,
    X_Tx_ON_OFF,
    adc_cs,
    gps1_cs,
    telecommand_cs,
    telemetry_cs
);

//--------------------------------------------------------------------
// Input
//--------------------------------------------------------------------
input         DIO1_1;
input         DIO1_2;
input  [11:0] FP_ADC_D;
input         FP_ADC_NEOC;
input         FP_GPS1_PPS;
input         FP_GPS1_Rx;
input         FP_GPS2_PPS;
input         FP_GPS2_Rx;
input         FP_MTR1_mon1;
input         FP_MTR1_mon2;
input         FP_MTR2_mon1;
input         FP_MTR2_mon2;
input         FP_MTR3_mon1;
input         FP_MTR3_mon2;
input         FP_MUXSEL_IMU2;
input         FP_PL_RXD;
input         FP_PL_STATUS;
input         FP_RW1_Rx;
input         FP_RW2_Rx;
input         FP_RW3_Rx;
input         FP_RW4_Rx;
input         FP_WDOG;
input         LT_EP1_BRDY;
input         LT_EP2_BRDY;
input         LT_FP_READ;
input         MAJ_TC;
input         MIN_TC;
input  [6:0]  MP_addr;
input  [15:0] MP_data_in;
input         MP_ios_n;
input         MP_read;
input         MP_write;
input         MUX1_OUT;
input         MUX2_OUT;
input         MUX3_OUT;
input         MUX4_OUT;
input         RX_TC_CLK;
input         RX_TC_DATA;
input         SA1_deploy_ST;
input         SA2_deploy_ST;
input         SPI_DOUT_1;
input         SPI_DOUT_2;
input         TC_MUXSEL;
input         TS_SPI_DIO1_3;
input         TS_SPI_DOUT_3;
input         TS_TC_CLK;
input         TS_TC_DATA;
input         VOBC_TM;
input         clk;
input         reset;
//--------------------------------------------------------------------
// Output
//--------------------------------------------------------------------
output        ANT_DEPLOY;
output        ANT_DEPLOY_SDA;
output        FP_MTR1_N1;
output        FP_MTR1_N2;
output        FP_MTR1_P1;
output        FP_MTR1_P2;
output        FP_MTR2_N1;
output        FP_MTR2_N2;
output        FP_MTR2_P1;
output        FP_MTR2_P2;
output        FP_MTR3_N1;
output        FP_MTR3_N2;
output        FP_MTR3_P1;
output        FP_MTR3_P2;
output        FP_NMI;
output        FP_PL_EN;
output        FP_PL_TXD;
output        FP_RW1_TX;
output        FP_RW2_TX;
output        FP_RW3_TX;
output        FP_RW4_TX;
output        GC_CLK;
output        GPS1_ON_OFF;
output        GPS1_Reset;
output        GPS2_ON_OFF;
output        GPS2_Reset;
output        Heater1_ON_OFF;
output        Heater2_ON_OFF;
output        Heater3_ON_OFF;
output        Heater4_ON_OFF;
output        Heater5_ON_OFF;
output        Heater6_ON_OFF;
output        IMU1_ON_OFF;
output        IMU1_Reset;
output        IMU2_ON_OFF;
output        IMU2_Reset;
output        MAJ_RST;
output        MIN_RST;
output        MP_Reset;
output [15:0] MP_data;
output [15:0] MP_data_0;
output [15:0] MP_data_1;
output [15:0] MP_data_2;
output        MTR_ON_OFF;
output        Payload_ON_OFF;
output        RF_Tx_ON_OFF;
output        RW1_ON_OFF;
output        RW2_ON_OFF;
output        RW3_ON_OFF;
output        RW4_ON_OFF;
output        SA1_DEPLOY;
output        SA2_DEPLOY;
output        SEL_0;
output        SEL_1;
output        SEL_2;
output        SENSOR1_ON_OFF;
output        SENSOR2_ON_OFF;
output        SENSOR3_ON_OFF;
output        SENSOR4_ON_OFF;
output        SENSOR5_ON_OFF;
output        SENSOR6_ON_OFF;
output        SPARE1_ON_OFF;
output        SPARE2_ON_OFF;
output        SPI_DIN_1;
output        SPI_DIN_2;
output        SPI_NCS_1;
output        SPI_NCS_2;
output        SPI_SCLK_1;
output        SPI_SCLK_2;
output        TM_DS_EN;
output        TS_SPI_DIN_3;
output        TS_SPI_NCS;
output        TS_SPI_SCLK;
output        X_Tx_ON_OFF;
output        adc_cs;
output        gps1_cs;
output        telecommand_cs;
output        telemetry_cs;
//--------------------------------------------------------------------
// Nets
//--------------------------------------------------------------------
wire          adc_cs_net_0;
wire          adrs_decoder_0_ip_latch1_cs;
wire          adrs_decoder_0_ip_latch2_cs;
wire          adrs_decoder_0_ip_latch3_cs;
wire          adrs_decoder_0_ip_latch4_cs;
wire          adrs_decoder_0_op_latch1_cs;
wire          adrs_decoder_0_op_latch2_cs;
wire          adrs_decoder_0_op_latch3_cs;
wire          adrs_decoder_0_op_latch4_cs;
wire          adrs_decoder_0_op_latch5_cs;
wire          adrs_decoder_0_op_latch6_cs;
wire          ANT_DEPLOY_net_0;
wire          ANT_DEPLOY_SDA_net_0;
wire          clk;
wire          clk_200us_0_clk_200;
wire          DIO1_1;
wire          DIO1_2;
wire   [11:0] FP_ADC_D;
wire          FP_ADC_NEOC;
wire          FP_GPS1_PPS;
wire          FP_GPS1_Rx;
wire          FP_GPS2_PPS;
wire          FP_GPS2_Rx;
wire          FP_MTR1_mon1;
wire          FP_MTR1_mon2;
wire          FP_MTR1_N1_net_0;
wire          FP_MTR1_N2_net_0;
wire          FP_MTR1_P1_net_0;
wire          FP_MTR1_P2_net_0;
wire          FP_MTR2_mon1;
wire          FP_MTR2_mon2;
wire          FP_MTR2_N1_net_0;
wire          FP_MTR2_N2_net_0;
wire          FP_MTR2_P1_net_0;
wire          FP_MTR2_P2_net_0;
wire          FP_MTR3_mon1;
wire          FP_MTR3_mon2;
wire          FP_MTR3_N1_net_0;
wire          FP_MTR3_N2_net_0;
wire          FP_MTR3_P1_net_0;
wire          FP_MTR3_P2_net_0;
wire          FP_MUXSEL_IMU2;
wire          FP_NMI_net_0;
wire          FP_PL_EN_net_0;
wire          FP_PL_RXD;
wire          FP_PL_STATUS;
wire          FP_PL_TXD_net_0;
wire          FP_RW1_Rx;
wire          FP_RW1_TX_net_0;
wire          FP_RW2_Rx;
wire          FP_RW2_TX_net_0;
wire          FP_RW3_Rx;
wire          FP_RW3_TX_net_0;
wire          FP_RW4_Rx;
wire          FP_RW4_TX_net_0;
wire          FP_WDOG;
wire          GC_CLK_net_0;
wire          gps1_cs_net_0;
wire          GPS1_ON_OFF_net_0;
wire          GPS1_Reset_net_0;
wire          GPS2_ON_OFF_net_0;
wire          GPS2_Reset_net_0;
wire          Heater1_ON_OFF_net_0;
wire          Heater2_ON_OFF_net_0;
wire          Heater3_ON_OFF_net_0;
wire          Heater4_ON_OFF_net_0;
wire          Heater5_ON_OFF_net_0;
wire          Heater6_ON_OFF_net_0;
wire          IMU1_ON_OFF_net_0;
wire          IMU1_Reset_net_0;
wire          IMU2_ON_OFF_net_0;
wire          IMU2_Reset_net_0;
wire          LT_EP1_BRDY;
wire          LT_EP2_BRDY;
wire          LT_FP_READ;
wire          MAJ_RST_net_0;
wire          MAJ_TC;
wire          MIN_RST_net_0;
wire          MIN_TC;
wire   [6:0]  MP_addr;
wire   [15:0] MP_data_net_0;
wire   [15:0] MP_data_0_net_0;
wire   [15:0] MP_data_1_net_0;
wire   [15:0] MP_data_2_net_0;
wire   [15:0] MP_data_in;
wire          MP_ios_n;
wire          MP_read;
wire          MP_Reset_net_0;
wire          MP_write;
wire          MTR_ON_OFF_net_0;
wire          MUX1_OUT;
wire          MUX2_OUT;
wire          MUX3_OUT;
wire          MUX4_OUT;
wire          Payload_ON_OFF_net_0;
wire          reset;
wire          RF_Tx_ON_OFF_net_0;
wire          RW1_ON_OFF_net_0;
wire          RW2_ON_OFF_net_0;
wire          RW3_ON_OFF_net_0;
wire          RW4_ON_OFF_net_0;
wire          RX_TC_CLK;
wire          RX_TC_DATA;
wire          SA1_DEPLOY_net_0;
wire          SA1_deploy_ST;
wire          SA2_DEPLOY_net_0;
wire          SA2_deploy_ST;
wire          SEL_0_net_0;
wire          SEL_1_net_0;
wire          SEL_2_net_0;
wire          SENSOR1_ON_OFF_net_0;
wire          SENSOR2_ON_OFF_net_0;
wire          SENSOR3_ON_OFF_net_0;
wire          SENSOR4_ON_OFF_net_0;
wire          SENSOR5_ON_OFF_net_0;
wire          SENSOR6_ON_OFF_net_0;
wire          SPARE1_ON_OFF_net_0;
wire          SPARE2_ON_OFF_net_0;
wire          SPI_DIN_1_net_0;
wire          SPI_DIN_2_net_0;
wire          SPI_DOUT_1;
wire          SPI_DOUT_2;
wire          SPI_NCS_1_net_0;
wire          SPI_NCS_2_net_0;
wire          SPI_SCLK_1_net_0;
wire          SPI_SCLK_2_net_0;
wire          TC_MUXSEL;
wire          telecommand_cs_net_0;
wire          telemetry_cs_net_0;
wire          TM_DS_EN_net_0;
wire          TS_SPI_DIN_3_net_0;
wire          TS_SPI_DIO1_3;
wire          TS_SPI_DOUT_3;
wire          TS_SPI_NCS_net_0;
wire          TS_SPI_SCLK_net_0;
wire          TS_TC_CLK;
wire          TS_TC_DATA;
wire          VOBC_TM;
wire          X_Tx_ON_OFF_net_0;
wire          SENSOR2_ON_OFF_net_1;
wire          SPI_NCS_2_net_1;
wire          SENSOR5_ON_OFF_net_1;
wire          SPI_DIN_1_net_1;
wire          SEL_0_net_1;
wire          SPI_SCLK_1_net_1;
wire          SEL_1_net_1;
wire          SENSOR3_ON_OFF_net_1;
wire          SENSOR4_ON_OFF_net_1;
wire          ANT_DEPLOY_SDA_net_1;
wire          SPI_SCLK_2_net_1;
wire          SPI_DIN_2_net_1;
wire          SENSOR1_ON_OFF_net_1;
wire          SEL_2_net_1;
wire          SENSOR6_ON_OFF_net_1;
wire          SPI_NCS_1_net_1;
wire          FP_PL_TXD_net_1;
wire          GC_CLK_net_1;
wire          Heater1_ON_OFF_net_1;
wire          FP_RW4_TX_net_1;
wire          Heater6_ON_OFF_net_1;
wire          FP_RW2_TX_net_1;
wire          TS_SPI_DIN_3_net_1;
wire          SA1_DEPLOY_net_1;
wire          FP_RW3_TX_net_1;
wire          FP_NMI_net_1;
wire          RW1_ON_OFF_net_1;
wire          TS_SPI_NCS_net_1;
wire          FP_RW1_TX_net_1;
wire          TS_SPI_SCLK_net_1;
wire          FP_PL_EN_net_1;
wire          RW4_ON_OFF_net_1;
wire          RW3_ON_OFF_net_1;
wire          IMU1_ON_OFF_net_1;
wire          TM_DS_EN_net_1;
wire          GPS2_Reset_net_1;
wire          Heater3_ON_OFF_net_1;
wire          SPARE1_ON_OFF_net_1;
wire          GPS1_ON_OFF_net_1;
wire          Heater5_ON_OFF_net_1;
wire          RW2_ON_OFF_net_1;
wire          MTR_ON_OFF_net_1;
wire          Payload_ON_OFF_net_1;
wire          ANT_DEPLOY_net_1;
wire          Heater2_ON_OFF_net_1;
wire          SA2_DEPLOY_net_1;
wire          Heater4_ON_OFF_net_1;
wire          IMU1_Reset_net_1;
wire          SPARE2_ON_OFF_net_1;
wire          IMU2_ON_OFF_net_1;
wire          FP_MTR1_N2_net_1;
wire          FP_MTR3_N2_net_1;
wire          FP_MTR1_N1_net_1;
wire          FP_MTR1_P2_net_1;
wire          FP_MTR2_N1_net_1;
wire          FP_MTR3_P1_net_1;
wire          FP_MTR3_P2_net_1;
wire          FP_MTR1_P1_net_1;
wire          GPS1_Reset_net_1;
wire          GPS2_ON_OFF_net_1;
wire          FP_MTR3_N1_net_1;
wire          RF_Tx_ON_OFF_net_1;
wire          FP_MTR2_P2_net_1;
wire          IMU2_Reset_net_1;
wire          X_Tx_ON_OFF_net_1;
wire          FP_MTR2_N2_net_1;
wire          FP_MTR2_P1_net_1;
wire          MP_Reset_net_1;
wire          adc_cs_net_1;
wire          gps1_cs_net_1;
wire          telecommand_cs_net_1;
wire          telemetry_cs_net_1;
wire          MAJ_RST_net_1;
wire          MIN_RST_net_1;
wire   [15:0] MP_data_net_1;
wire   [15:0] MP_data_2_net_1;
wire   [15:0] MP_data_1_net_1;
wire   [15:0] MP_data_0_net_1;
//--------------------------------------------------------------------
// TiedOff Nets
//--------------------------------------------------------------------
wire   [9:0]  FP_ADC_D_0_const_net_0;
//--------------------------------------------------------------------
// Constant assignments
//--------------------------------------------------------------------
assign FP_ADC_D_0_const_net_0 = 10'h000;
//--------------------------------------------------------------------
// Top level output port assignments
//--------------------------------------------------------------------
assign SENSOR2_ON_OFF_net_1 = SENSOR2_ON_OFF_net_0;
assign SENSOR2_ON_OFF       = SENSOR2_ON_OFF_net_1;
assign SPI_NCS_2_net_1      = SPI_NCS_2_net_0;
assign SPI_NCS_2            = SPI_NCS_2_net_1;
assign SENSOR5_ON_OFF_net_1 = SENSOR5_ON_OFF_net_0;
assign SENSOR5_ON_OFF       = SENSOR5_ON_OFF_net_1;
assign SPI_DIN_1_net_1      = SPI_DIN_1_net_0;
assign SPI_DIN_1            = SPI_DIN_1_net_1;
assign SEL_0_net_1          = SEL_0_net_0;
assign SEL_0                = SEL_0_net_1;
assign SPI_SCLK_1_net_1     = SPI_SCLK_1_net_0;
assign SPI_SCLK_1           = SPI_SCLK_1_net_1;
assign SEL_1_net_1          = SEL_1_net_0;
assign SEL_1                = SEL_1_net_1;
assign SENSOR3_ON_OFF_net_1 = SENSOR3_ON_OFF_net_0;
assign SENSOR3_ON_OFF       = SENSOR3_ON_OFF_net_1;
assign SENSOR4_ON_OFF_net_1 = SENSOR4_ON_OFF_net_0;
assign SENSOR4_ON_OFF       = SENSOR4_ON_OFF_net_1;
assign ANT_DEPLOY_SDA_net_1 = ANT_DEPLOY_SDA_net_0;
assign ANT_DEPLOY_SDA       = ANT_DEPLOY_SDA_net_1;
assign SPI_SCLK_2_net_1     = SPI_SCLK_2_net_0;
assign SPI_SCLK_2           = SPI_SCLK_2_net_1;
assign SPI_DIN_2_net_1      = SPI_DIN_2_net_0;
assign SPI_DIN_2            = SPI_DIN_2_net_1;
assign SENSOR1_ON_OFF_net_1 = SENSOR1_ON_OFF_net_0;
assign SENSOR1_ON_OFF       = SENSOR1_ON_OFF_net_1;
assign SEL_2_net_1          = SEL_2_net_0;
assign SEL_2                = SEL_2_net_1;
assign SENSOR6_ON_OFF_net_1 = SENSOR6_ON_OFF_net_0;
assign SENSOR6_ON_OFF       = SENSOR6_ON_OFF_net_1;
assign SPI_NCS_1_net_1      = SPI_NCS_1_net_0;
assign SPI_NCS_1            = SPI_NCS_1_net_1;
assign FP_PL_TXD_net_1      = FP_PL_TXD_net_0;
assign FP_PL_TXD            = FP_PL_TXD_net_1;
assign GC_CLK_net_1         = GC_CLK_net_0;
assign GC_CLK               = GC_CLK_net_1;
assign Heater1_ON_OFF_net_1 = Heater1_ON_OFF_net_0;
assign Heater1_ON_OFF       = Heater1_ON_OFF_net_1;
assign FP_RW4_TX_net_1      = FP_RW4_TX_net_0;
assign FP_RW4_TX            = FP_RW4_TX_net_1;
assign Heater6_ON_OFF_net_1 = Heater6_ON_OFF_net_0;
assign Heater6_ON_OFF       = Heater6_ON_OFF_net_1;
assign FP_RW2_TX_net_1      = FP_RW2_TX_net_0;
assign FP_RW2_TX            = FP_RW2_TX_net_1;
assign TS_SPI_DIN_3_net_1   = TS_SPI_DIN_3_net_0;
assign TS_SPI_DIN_3         = TS_SPI_DIN_3_net_1;
assign SA1_DEPLOY_net_1     = SA1_DEPLOY_net_0;
assign SA1_DEPLOY           = SA1_DEPLOY_net_1;
assign FP_RW3_TX_net_1      = FP_RW3_TX_net_0;
assign FP_RW3_TX            = FP_RW3_TX_net_1;
assign FP_NMI_net_1         = FP_NMI_net_0;
assign FP_NMI               = FP_NMI_net_1;
assign RW1_ON_OFF_net_1     = RW1_ON_OFF_net_0;
assign RW1_ON_OFF           = RW1_ON_OFF_net_1;
assign TS_SPI_NCS_net_1     = TS_SPI_NCS_net_0;
assign TS_SPI_NCS           = TS_SPI_NCS_net_1;
assign FP_RW1_TX_net_1      = FP_RW1_TX_net_0;
assign FP_RW1_TX            = FP_RW1_TX_net_1;
assign TS_SPI_SCLK_net_1    = TS_SPI_SCLK_net_0;
assign TS_SPI_SCLK          = TS_SPI_SCLK_net_1;
assign FP_PL_EN_net_1       = FP_PL_EN_net_0;
assign FP_PL_EN             = FP_PL_EN_net_1;
assign RW4_ON_OFF_net_1     = RW4_ON_OFF_net_0;
assign RW4_ON_OFF           = RW4_ON_OFF_net_1;
assign RW3_ON_OFF_net_1     = RW3_ON_OFF_net_0;
assign RW3_ON_OFF           = RW3_ON_OFF_net_1;
assign IMU1_ON_OFF_net_1    = IMU1_ON_OFF_net_0;
assign IMU1_ON_OFF          = IMU1_ON_OFF_net_1;
assign TM_DS_EN_net_1       = TM_DS_EN_net_0;
assign TM_DS_EN             = TM_DS_EN_net_1;
assign GPS2_Reset_net_1     = GPS2_Reset_net_0;
assign GPS2_Reset           = GPS2_Reset_net_1;
assign Heater3_ON_OFF_net_1 = Heater3_ON_OFF_net_0;
assign Heater3_ON_OFF       = Heater3_ON_OFF_net_1;
assign SPARE1_ON_OFF_net_1  = SPARE1_ON_OFF_net_0;
assign SPARE1_ON_OFF        = SPARE1_ON_OFF_net_1;
assign GPS1_ON_OFF_net_1    = GPS1_ON_OFF_net_0;
assign GPS1_ON_OFF          = GPS1_ON_OFF_net_1;
assign Heater5_ON_OFF_net_1 = Heater5_ON_OFF_net_0;
assign Heater5_ON_OFF       = Heater5_ON_OFF_net_1;
assign RW2_ON_OFF_net_1     = RW2_ON_OFF_net_0;
assign RW2_ON_OFF           = RW2_ON_OFF_net_1;
assign MTR_ON_OFF_net_1     = MTR_ON_OFF_net_0;
assign MTR_ON_OFF           = MTR_ON_OFF_net_1;
assign Payload_ON_OFF_net_1 = Payload_ON_OFF_net_0;
assign Payload_ON_OFF       = Payload_ON_OFF_net_1;
assign ANT_DEPLOY_net_1     = ANT_DEPLOY_net_0;
assign ANT_DEPLOY           = ANT_DEPLOY_net_1;
assign Heater2_ON_OFF_net_1 = Heater2_ON_OFF_net_0;
assign Heater2_ON_OFF       = Heater2_ON_OFF_net_1;
assign SA2_DEPLOY_net_1     = SA2_DEPLOY_net_0;
assign SA2_DEPLOY           = SA2_DEPLOY_net_1;
assign Heater4_ON_OFF_net_1 = Heater4_ON_OFF_net_0;
assign Heater4_ON_OFF       = Heater4_ON_OFF_net_1;
assign IMU1_Reset_net_1     = IMU1_Reset_net_0;
assign IMU1_Reset           = IMU1_Reset_net_1;
assign SPARE2_ON_OFF_net_1  = SPARE2_ON_OFF_net_0;
assign SPARE2_ON_OFF        = SPARE2_ON_OFF_net_1;
assign IMU2_ON_OFF_net_1    = IMU2_ON_OFF_net_0;
assign IMU2_ON_OFF          = IMU2_ON_OFF_net_1;
assign FP_MTR1_N2_net_1     = FP_MTR1_N2_net_0;
assign FP_MTR1_N2           = FP_MTR1_N2_net_1;
assign FP_MTR3_N2_net_1     = FP_MTR3_N2_net_0;
assign FP_MTR3_N2           = FP_MTR3_N2_net_1;
assign FP_MTR1_N1_net_1     = FP_MTR1_N1_net_0;
assign FP_MTR1_N1           = FP_MTR1_N1_net_1;
assign FP_MTR1_P2_net_1     = FP_MTR1_P2_net_0;
assign FP_MTR1_P2           = FP_MTR1_P2_net_1;
assign FP_MTR2_N1_net_1     = FP_MTR2_N1_net_0;
assign FP_MTR2_N1           = FP_MTR2_N1_net_1;
assign FP_MTR3_P1_net_1     = FP_MTR3_P1_net_0;
assign FP_MTR3_P1           = FP_MTR3_P1_net_1;
assign FP_MTR3_P2_net_1     = FP_MTR3_P2_net_0;
assign FP_MTR3_P2           = FP_MTR3_P2_net_1;
assign FP_MTR1_P1_net_1     = FP_MTR1_P1_net_0;
assign FP_MTR1_P1           = FP_MTR1_P1_net_1;
assign GPS1_Reset_net_1     = GPS1_Reset_net_0;
assign GPS1_Reset           = GPS1_Reset_net_1;
assign GPS2_ON_OFF_net_1    = GPS2_ON_OFF_net_0;
assign GPS2_ON_OFF          = GPS2_ON_OFF_net_1;
assign FP_MTR3_N1_net_1     = FP_MTR3_N1_net_0;
assign FP_MTR3_N1           = FP_MTR3_N1_net_1;
assign RF_Tx_ON_OFF_net_1   = RF_Tx_ON_OFF_net_0;
assign RF_Tx_ON_OFF         = RF_Tx_ON_OFF_net_1;
assign FP_MTR2_P2_net_1     = FP_MTR2_P2_net_0;
assign FP_MTR2_P2           = FP_MTR2_P2_net_1;
assign IMU2_Reset_net_1     = IMU2_Reset_net_0;
assign IMU2_Reset           = IMU2_Reset_net_1;
assign X_Tx_ON_OFF_net_1    = X_Tx_ON_OFF_net_0;
assign X_Tx_ON_OFF          = X_Tx_ON_OFF_net_1;
assign FP_MTR2_N2_net_1     = FP_MTR2_N2_net_0;
assign FP_MTR2_N2           = FP_MTR2_N2_net_1;
assign FP_MTR2_P1_net_1     = FP_MTR2_P1_net_0;
assign FP_MTR2_P1           = FP_MTR2_P1_net_1;
assign MP_Reset_net_1       = MP_Reset_net_0;
assign MP_Reset             = MP_Reset_net_1;
assign adc_cs_net_1         = adc_cs_net_0;
assign adc_cs               = adc_cs_net_1;
assign gps1_cs_net_1        = gps1_cs_net_0;
assign gps1_cs              = gps1_cs_net_1;
assign telecommand_cs_net_1 = telecommand_cs_net_0;
assign telecommand_cs       = telecommand_cs_net_1;
assign telemetry_cs_net_1   = telemetry_cs_net_0;
assign telemetry_cs         = telemetry_cs_net_1;
assign MAJ_RST_net_1        = MAJ_RST_net_0;
assign MAJ_RST              = MAJ_RST_net_1;
assign MIN_RST_net_1        = MIN_RST_net_0;
assign MIN_RST              = MIN_RST_net_1;
assign MP_data_net_1        = MP_data_net_0;
assign MP_data[15:0]        = MP_data_net_1;
assign MP_data_2_net_1      = MP_data_2_net_0;
assign MP_data_2[15:0]      = MP_data_2_net_1;
assign MP_data_1_net_1      = MP_data_1_net_0;
assign MP_data_1[15:0]      = MP_data_1_net_1;
assign MP_data_0_net_1      = MP_data_0_net_0;
assign MP_data_0[15:0]      = MP_data_0_net_1;
//--------------------------------------------------------------------
// Component instances
//--------------------------------------------------------------------
//--------adrs_decoder
adrs_decoder adrs_decoder_0(
        // Inputs
        .reset          ( reset ),
        .MP_ios_n       ( MP_ios_n ),
        .MP_addr        ( MP_addr ),
        // Outputs
        .adc_cs         ( adc_cs_net_0 ),
        .gps1_cs        ( gps1_cs_net_0 ),
        .gps2_cs        (  ),
        .imu1_cs        (  ),
        .imu2_cs        (  ),
        .heater_cs      (  ),
        .torquer_cs     (  ),
        .RW_cs          (  ),
        .telemetry_cs   ( telemetry_cs_net_0 ),
        .telecommand_cs ( telecommand_cs_net_0 ),
        .minor_cyc_cs   (  ),
        .payload_cs     (  ),
        .op_latch1_cs   ( adrs_decoder_0_op_latch1_cs ),
        .op_latch2_cs   ( adrs_decoder_0_op_latch2_cs ),
        .op_latch3_cs   ( adrs_decoder_0_op_latch3_cs ),
        .op_latch4_cs   ( adrs_decoder_0_op_latch4_cs ),
        .op_latch5_cs   ( adrs_decoder_0_op_latch5_cs ),
        .ip_latch1_cs   ( adrs_decoder_0_ip_latch1_cs ),
        .ip_latch2_cs   ( adrs_decoder_0_ip_latch2_cs ),
        .ip_latch3_cs   ( adrs_decoder_0_ip_latch3_cs ),
        .ip_latch4_cs   ( adrs_decoder_0_ip_latch4_cs ),
        .op_latch6_cs   ( adrs_decoder_0_op_latch6_cs ) 
        );

//--------clk_200us
clk_200us clk_200us_0(
        // Inputs
        .clk      ( clk ),
        .reset    ( reset ),
        // Outputs
        .clk_200  ( clk_200us_0_clk_200 ),
        .MP_Reset ( MP_Reset_net_0 ) 
        );

//--------Input_port_mux
Input_port_mux Input_port_mux_0(
        // Inputs
        .in_port1_cs    ( adrs_decoder_0_ip_latch1_cs ),
        .MP_read        ( MP_read ),
        .in_port3_cs    ( adrs_decoder_0_ip_latch3_cs ),
        .in_port4_cs    ( adrs_decoder_0_ip_latch4_cs ),
        .FP_MTR1_mon2   ( FP_MTR1_mon2 ),
        .DIO1_2         ( DIO1_2 ),
        .FP_PL_RXD      ( FP_PL_RXD ),
        .TS_SPI_DOUT_3  ( TS_SPI_DOUT_3 ),
        .FP_MUXSEL_IMU2 ( FP_MUXSEL_IMU2 ),
        .FP_MTR2_mon1   ( FP_MTR2_mon1 ),
        .SPI_DOUT_1     ( SPI_DOUT_1 ),
        .SPI_DOUT_2     ( SPI_DOUT_2 ),
        .FP_MTR1_mon1   ( FP_MTR1_mon1 ),
        .FP_PL_STATUS   ( FP_PL_STATUS ),
        .TS_SPI_DIO1_3  ( TS_SPI_DIO1_3 ),
        .DIO1_1         ( DIO1_1 ),
        .FP_MTR3_mon2   ( FP_MTR3_mon2 ),
        .VOBC_TM        ( VOBC_TM ),
        .FP_MTR2_mon2   ( FP_MTR2_mon2 ),
        .FP_MTR3_mon1   ( FP_MTR3_mon1 ),
        .in_port2_cs    ( adrs_decoder_0_ip_latch2_cs ),
        .MUX4_OUT       ( MUX4_OUT ),
        .MUX2_OUT       ( MUX2_OUT ),
        .MUX3_OUT       ( MUX3_OUT ),
        .MUX1_OUT       ( MUX1_OUT ),
        .LT_EP2_BRDY    ( LT_EP2_BRDY ),
        .LT_EP1_BRDY    ( LT_EP1_BRDY ),
        .FP_WDOG        ( FP_WDOG ),
        .LT_FP_READ     ( LT_FP_READ ),
        .FP_RW1_Rx      ( FP_RW1_Rx ),
        .RX_TC_DATA     ( RX_TC_DATA ),
        .FP_ADC_NEOC    ( FP_ADC_NEOC ),
        .FP_GPS1_Rx     ( FP_GPS1_Rx ),
        .FP_GPS2_PPS    ( FP_GPS2_PPS ),
        .FP_RW3_Rx      ( FP_RW3_Rx ),
        .TC_MUXSEL      ( TC_MUXSEL ),
        .TS_TC_CLK      ( TS_TC_CLK ),
        .FP_RW4_Rx      ( FP_RW4_Rx ),
        .RX_TC_CLK      ( RX_TC_CLK ),
        .FP_RW2_Rx      ( FP_RW2_Rx ),
        .TS_TC_DATA     ( TS_TC_DATA ),
        .SA2_deploy_ST  ( SA2_deploy_ST ),
        .FP_GPS1_PPS    ( FP_GPS1_PPS ),
        .FP_GPS2_Rx     ( FP_GPS2_Rx ),
        .SA1_deploy_ST  ( SA1_deploy_ST ),
        .MAJ_TC         ( MAJ_TC ),
        .MIN_TC         ( MIN_TC ),
        .FP_ADC_D_0     ( FP_ADC_D_0_const_net_0 ),
        // Outputs
        .MP_data        ( MP_data_net_0 ),
        .MP_data_0      ( MP_data_0_net_0 ),
        .MP_data_1      ( MP_data_1_net_0 ),
        .MP_data_2      ( MP_data_2_net_0 ) 
        );

//--------OP_LATCHES
OP_LATCHES OP_LATCHES_0(
        // Inputs
        .MP_write       ( MP_write ),
        .PO_Reset       ( reset ),
        .latch2_CS      ( adrs_decoder_0_op_latch2_cs ),
        .latch3_CS      ( adrs_decoder_0_op_latch3_cs ),
        .latch4_CS      ( adrs_decoder_0_op_latch4_cs ),
        .latch5_CS      ( adrs_decoder_0_op_latch5_cs ),
        .clk            ( clk ),
        .clk_200us      ( clk_200us_0_clk_200 ),
        .latch1_CS      ( adrs_decoder_0_op_latch1_cs ),
        .latch6_CS      ( adrs_decoder_0_op_latch6_cs ),
        .MP_data_in     ( MP_data_in ),
        // Outputs
        .ANT_DEPLOY_SDA ( ANT_DEPLOY_SDA_net_0 ),
        .SPI_NCS_1      ( SPI_NCS_1_net_0 ),
        .SENSOR1_ON_OFF ( SENSOR1_ON_OFF_net_0 ),
        .SPI_DIN_1      ( SPI_DIN_1_net_0 ),
        .SEL_1          ( SEL_1_net_0 ),
        .SPI_SCLK_1     ( SPI_SCLK_1_net_0 ),
        .SENSOR4_ON_OFF ( SENSOR4_ON_OFF_net_0 ),
        .SENSOR3_ON_OFF ( SENSOR3_ON_OFF_net_0 ),
        .SPI_NCS_2      ( SPI_NCS_2_net_0 ),
        .SEL_0          ( SEL_0_net_0 ),
        .SPI_DIN_2      ( SPI_DIN_2_net_0 ),
        .SPI_SCLK_2     ( SPI_SCLK_2_net_0 ),
        .SENSOR6_ON_OFF ( SENSOR6_ON_OFF_net_0 ),
        .SENSOR5_ON_OFF ( SENSOR5_ON_OFF_net_0 ),
        .SEL_2          ( SEL_2_net_0 ),
        .SENSOR2_ON_OFF ( SENSOR2_ON_OFF_net_0 ),
        .FP_PL_EN       ( FP_PL_EN_net_0 ),
        .FP_PL_TXD      ( FP_PL_TXD_net_0 ),
        .FP_RW2_TX      ( FP_RW2_TX_net_0 ),
        .FP_RW3_TX      ( FP_RW3_TX_net_0 ),
        .GC_CLK         ( GC_CLK_net_0 ),
        .FP_RW1_TX      ( FP_RW1_TX_net_0 ),
        .FP_NMI         ( FP_NMI_net_0 ),
        .FP_RW4_TX      ( FP_RW4_TX_net_0 ),
        .TS_SPI_DIN_3   ( TS_SPI_DIN_3_net_0 ),
        .TS_SPI_NCS     ( TS_SPI_NCS_net_0 ),
        .TS_SPI_SCLK    ( TS_SPI_SCLK_net_0 ),
        .Heater1_ON_OFF ( Heater1_ON_OFF_net_0 ),
        .Heater6_ON_OFF ( Heater6_ON_OFF_net_0 ),
        .SA1_DEPLOY     ( SA1_DEPLOY_net_0 ),
        .RW1_ON_OFF     ( RW1_ON_OFF_net_0 ),
        .RW2_ON_OFF     ( RW2_ON_OFF_net_0 ),
        .Heater2_ON_OFF ( Heater2_ON_OFF_net_0 ),
        .ANT_DEPLOY     ( ANT_DEPLOY_net_0 ),
        .Heater3_ON_OFF ( Heater3_ON_OFF_net_0 ),
        .Heater5_ON_OFF ( Heater5_ON_OFF_net_0 ),
        .SA2_DEPLOY     ( SA2_DEPLOY_net_0 ),
        .RW3_ON_OFF     ( RW3_ON_OFF_net_0 ),
        .Heater4_ON_OFF ( Heater4_ON_OFF_net_0 ),
        .RW4_ON_OFF     ( RW4_ON_OFF_net_0 ),
        .TM_DS_EN       ( TM_DS_EN_net_0 ),
        .GPS1_ON_OFF    ( GPS1_ON_OFF_net_0 ),
        .GPS2_Reset     ( GPS2_Reset_net_0 ),
        .SPARE2_ON_OFF  ( SPARE2_ON_OFF_net_0 ),
        .IMU1_Reset     ( IMU1_Reset_net_0 ),
        .MTR_ON_OFF     ( MTR_ON_OFF_net_0 ),
        .IMU1_ON_OFF    ( IMU1_ON_OFF_net_0 ),
        .SPARE1_ON_OFF  ( SPARE1_ON_OFF_net_0 ),
        .Payload_ON_OFF ( Payload_ON_OFF_net_0 ),
        .GPS2_ON_OFF    ( GPS2_ON_OFF_net_0 ),
        .IMU2_Reset     ( IMU2_Reset_net_0 ),
        .RF_Tx_ON_OFF   ( RF_Tx_ON_OFF_net_0 ),
        .GPS1_Reset     ( GPS1_Reset_net_0 ),
        .X_Tx_ON_OFF    ( X_Tx_ON_OFF_net_0 ),
        .IMU2_ON_OFF    ( IMU2_ON_OFF_net_0 ),
        .FP_MTR1_N1     ( FP_MTR1_N1_net_0 ),
        .FP_MTR1_P2     ( FP_MTR1_P2_net_0 ),
        .FP_MTR2_P1     ( FP_MTR2_P1_net_0 ),
        .FP_MTR1_P1     ( FP_MTR1_P1_net_0 ),
        .FP_MTR3_N1     ( FP_MTR3_N1_net_0 ),
        .FP_MTR2_N1     ( FP_MTR2_N1_net_0 ),
        .FP_MTR2_N2     ( FP_MTR2_N2_net_0 ),
        .FP_MTR3_N2     ( FP_MTR3_N2_net_0 ),
        .FP_MTR3_P2     ( FP_MTR3_P2_net_0 ),
        .FP_MTR2_P2     ( FP_MTR2_P2_net_0 ),
        .FP_MTR1_N2     ( FP_MTR1_N2_net_0 ),
        .FP_MTR3_P1     ( FP_MTR3_P1_net_0 ),
        .MAJ_RST        ( MAJ_RST_net_0 ),
        .MIN_RST        ( MIN_RST_net_0 ) 
        );


endmodule
